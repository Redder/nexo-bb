package es.hga.nexo.presentation.view.pendingorderdetails.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.hga.nexo.R;
import es.hga.nexo.domain.business.PendingOrder;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by alex on 10/05/2017.
 */

public class PendingOrderDetailsAdapter extends RecyclerView.Adapter<PendingOrderDetailsAdapter.ViewHolder>{

    private static final String TAG = "PendingOrderDetAdapter";

    private List<PendingOrder.PendingOrderLine> mPendingOrderLines;

    public PendingOrderDetailsAdapter(List<PendingOrder.PendingOrderLine> pendingOrderLines) {
        mPendingOrderLines = pendingOrderLines;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pendingorderdetails_rv_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PendingOrder.PendingOrderLine pendingOrderLine = mPendingOrderLines.get(position);

        holder.lineaConcepto.setText(pendingOrderLine.getConcepto());
        holder.lineaEstado.setText(pendingOrderLine.getEstado());
        holder.lineaTotal.setText(pendingOrderLine.getTotalLinea() + "");
        holder.lineaCC.setText(pendingOrderLine.getCentrocoste());


        //Log.i(TAG, "concepto: " + pendingOrderLine.getConcepto());

    }


    @Override
    public int getItemCount() {
        return mPendingOrderLines.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tv_pendingorderdetails_rv_item_concepto)
        protected TextView lineaConcepto;
        @BindView(R.id.tv_pendingorderdetails_rv_item_estado)
        protected TextView lineaEstado;
        @BindView(R.id.tv_pendingorderdetails_rv_item_totallinea)
        protected TextView lineaTotal;
        @BindView(R.id.tv_pendingorderdetails_rv_item_centrocoste)
        protected TextView lineaCC;
        @BindView(R.id.ll_pendingorderdetails_rv_item_container)
        protected LinearLayout lineaContainer;


        ViewHolder(View view){
            super(view);

            ButterKnife.bind(this, view);
        }

    }

    public void setPendingOrderLines(List<PendingOrder.PendingOrderLine> pendingOrderLines) {
        mPendingOrderLines = pendingOrderLines;
        notifyDataSetChanged();
    }
}

