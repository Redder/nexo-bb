package es.hga.nexo.presentation.view.activity;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.iid.FirebaseInstanceId;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.hga.nexo.MyApp;
import es.hga.nexo.R;
import es.hga.nexo.domain.business.Login;
import es.hga.nexo.presentation.base.BaseActivity;
import es.hga.nexo.presentation.base.BaseFragment;
import es.hga.nexo.presentation.view.dashboard.DashboardFragment;
import es.hga.nexo.presentation.view.login.LoginFragment;
import es.hga.nexo.presentation.view.pendingorderdetails.PendingOrderDetailsFragment;
import es.hga.nexo.presentation.view.pendingorders.PendingOrdersFragment;
import es.hga.nexo.presentation.view.presence.PresenceFragment;
import es.hga.nexo.presentation.view.production.ProductionFragment;
import es.hga.nexo.presentation.view.settings.SettingsFragment;

import static es.hga.nexo.MyApp.getContext;

public class MainActivity extends BaseActivity implements MainActivityView, NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "MainActivity";

    private boolean isLoggedIn = false;

    @BindView(R.id.nav_view)
    NavigationView navView;

    boolean fragmentTransaction = false;
    BaseFragment fragment = null;

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.fab_app_bar_main)
    FloatingActionButton mFab;

    //@Inject
    //CloseSessionUseCase mCloseSessionUseCase;

    @Inject
    MainActivityPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        
        /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */

        //DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Log.i(TAG, "onCreate");



        /////////////
        //FIREBASE
        /*
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG,"Token: " + token);
        if (token != null && token.length() > 0){
            mSetPushTokenUseCase.execute(new SetPushTokenObserver());
        }
        */
        /////////////

        /////////////////////////////////////
        ////////FROM NOTIFICATION////////////
        String openFragment = getIntent().getStringExtra("openFragment");

        Log.d(TAG,"onCreate: " + openFragment);

        BaseFragment fragment;

        if (openFragment != null && openFragment.length() > 0){

            switch (openFragment){

                case "pendingorderlist":
                    fragment = PendingOrdersFragment.newInstance();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.mainContainer, fragment)
                            .addToBackStack(null)
                            .commit();
                    break;

                case "pendingordersdetails":

                    int pendingOrderId = Integer.parseInt(getIntent().getStringExtra("pendingOrderId"));

                    int id = 0;

                    fragment = PendingOrderDetailsFragment.newInstance(pendingOrderId);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.mainContainer, fragment)
                            .addToBackStack(null)
                            .commit();
                    break;

                default:
                    fragment = new LoginFragment();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.mainContainer, fragment)
                            .addToBackStack(null)
                            .commit();
                    break;

            }



        }else{

            //LOGIN FRAGMENT

            fragment = new LoginFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.mainContainer, fragment)
                    .addToBackStack(null)
                    .commit();
        }

        /////////////////////////////////////

        this.initializeInjector();

        mPresenter.setView(this);
    }


    @Override
    public void onBackPressed() {
        Log.d(TAG,"onBackPressed");
        //DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        /*
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {

            if (isLoggedIn()){
                getSupportFragmentManager().popBackStack();
            }
        }
        */
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            fragment = new SettingsFragment();

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.mainContainer, fragment)
                    .addToBackStack(null)
                    .commit();

            item.setChecked(true);
            //getSupportActionBar().setTitle(item.getTitle());
            getSupportActionBar().setTitle(fragment.getFragmentTitle());

            mDrawerLayout.closeDrawers();

            return true;
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_closesession) {

            fragment = new LoginFragment();

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.mainContainer, fragment)
                    .addToBackStack(null)
                    .commit();

            item.setChecked(true);
            //getSupportActionBar().setTitle(item.getTitle());
            getSupportActionBar().setTitle(fragment.getFragmentTitle());

            mDrawerLayout.closeDrawers();

            closeSession();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG,"onResume out");
        if (isLoggedIn()){
            Log.d(TAG,"onResume in");
            mPresenter.checkTokenWS();
        }
    }

    @Override
    public void showErrorCheckTokenWS(){

        isLoggedIn = false;

        BaseFragment fragment = LoginFragment.newInstance();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.mainContainer, fragment)
                //.addToBackStack(null)
                .commit();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_dashboard) {
            fragment = new DashboardFragment();
            fragmentTransaction = true;
        }

        if (id == R.id.nav_pendingorders) {
            fragment = new PendingOrdersFragment();
            fragmentTransaction = true;
        }

        if (id == R.id.nav_presence) {
            fragment = new PresenceFragment();
            fragmentTransaction = true;
        }

        if (id == R.id.nav_production) {
            fragment = new ProductionFragment();
            fragmentTransaction = true;
        }

        /*
        if (id == R.id.nav_dashboard) {
            fragment = new DashboardFragment();
            fragmentTransaction = true;
        } else if (id == R.id.nav_pendingorders) {

            //View view = findViewById(android.R.id.content);
            //Snackbar.make(view,"Replace with your own action", Snackbar.LENGTH_LONG)
            //        .setAction("Action", null).show();

            fragment = new PendingOrdersFragment();
            fragmentTransaction = true;
        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }
        */

        //DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.closeDrawer(GravityCompat.START);

        //cambio de fragment
        if(fragmentTransaction) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.mainContainer, fragment)
                    .addToBackStack(null)
                    .commit();

            //item.setChecked(true);

            //getSupportActionBar().setTitle(fragment.getFragmentTitle());
        }

        mDrawerLayout.closeDrawers();
        return false;//RETURN FLASE PARA QUE NO HAGA HIGHLIGHT DEL ITEM
    }

    private void initializeInjector() {
        ((MyApp) getApplication()).getApplicationComponent().inject(this);
    }

    public void setDrawerEnabled(boolean enabled) {
        int lockMode = enabled ? DrawerLayout.LOCK_MODE_UNLOCKED :
                DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
        mDrawerLayout.setDrawerLockMode(lockMode);

        //mToggle.setDrawerIndicatorEnabled(enabled);
        /*
        if (enabled){
            //Drawable d = getResources().getDrawable(R.drawable.ic_navigationdrawer_menu);
            //d.setColorFilter(0x090150, PorterDuff.Mode.MULTIPLY);
            mToolbar.setNavigationIcon(R.drawable.ic_navigationdrawer_menu);
        }else{
            mToolbar.setNavigationIcon(null);
        }
        */
    }

    private void clearBackStack() {

        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
            manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        /*
        while (getSupportFragmentManager().getBackStackEntryCount() > 0){
            getSupportFragmentManager().popBackStackImmediate();
        }
        */
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void openSession(){
        isLoggedIn = true;
    }

    public void closeSession(){

        mPresenter.closeSession();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    FirebaseInstanceId.getInstance().deleteInstanceId();
                } catch (Exception e) {
                    Log.d(TAG,"closeSession Exception: " + e.toString());
                }
            }
        }).start();

        isLoggedIn = false;

    }

    public void configureMenus(Login login){

        //MenuItem item = (MenuItem)findViewById(R.id.nav_pendingorders);
        //item.setVisible(false);

        Login.Permission permission;
        Menu nav_Menu = navView.getMenu();

        //ListPendingOrders
        permission = login.getPermissionByFormCode(133);
        if (permission == null || !permission.isVer()){
            nav_Menu.findItem(R.id.nav_pendingorders).setVisible(false);
        }

    }


    /*
    public void checkTokenWS() {
        mPresenter.closeSession();
    }
    */

    public void configFab(boolean enabled, int icon) {

        if (enabled){
            mFab.setVisibility(View.VISIBLE);
        }else{
            mFab.setVisibility(View.GONE);
        }

        if (icon != 0){
            mFab.setImageDrawable(ContextCompat.getDrawable(getContext(), icon));
        }


    }

    public FloatingActionButton getFab(){
        return mFab;
    }


    public boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }


    }

}
