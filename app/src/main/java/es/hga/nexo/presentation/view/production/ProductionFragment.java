package es.hga.nexo.presentation.view.production;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.hga.nexo.MyApp;
import es.hga.nexo.R;
import es.hga.nexo.dagger.components.ApplicationComponent;
import es.hga.nexo.domain.business.Employee;
import es.hga.nexo.presentation.base.BaseFragment;
import es.hga.nexo.presentation.view.activity.MainActivity;

public class ProductionFragment extends BaseFragment implements ProductionView{

    private static final String TAG = "ProductionFragment";
    private static final String mFragmentTitle = "Producción";

    @BindView(R.id.rl_production_form)
    RelativeLayout mRlForm;

    @BindView(R.id.btn_production_scan)
    Button mBtnScan;

    @BindView(R.id.et_production_card)
    EditText mEtCard;

    @BindView(R.id.tv_production_employee_name)
    TextView mTvEmpName;

    @BindView(R.id.et_production_chip)
    EditText mEtChip;

    @BindView(R.id.et_production_machine)
    EditText mEtMachine;

    @BindView(R.id.et_production_item)
    EditText mEtItem;

    @BindView(R.id.btn_production_save_card)
    Button mBtnCheckCard;


    @Inject
    ProductionPresenter mPresenter;

    private boolean resumeFromBarcode = false;


    public ProductionFragment() {

    }

    public static ProductionFragment newInstance() {
        Bundle args = new Bundle();

        ProductionFragment fragment = new ProductionFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectComponent(MyApp.get(getActivity()).getApplicationComponent());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        ((MainActivity)getActivity()).setDrawerEnabled(true);
        ((MainActivity)getActivity()).configFab(false,0);

        if (view != null) {
            ButterKnife.bind(this, view);
            Log.i(TAG, "onCreateView");
            ((MainActivity)getActivity()).configFab(false,0);
            //mAbilityScoresRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
            //mStatScoresRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        }

        mBtnScan.setOnClickListener(v -> {
            //Log.i(TAG,"btn accept click");
            scanFromFragment();
        });

        mBtnCheckCard.setOnClickListener(v -> {
            checkCard(mEtCard.getText() + "");
        });

        /////////////////////////////////


        //////////////////////////////

        return view;
    }



    public void scanFromFragment() {
        IntentIntegrator.forSupportFragment(this).initiateScan();

        /*
                IntentIntegrator integrator = new IntentIntegrator(this);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
                integrator.setPrompt("Scan a barcode");
                integrator.setCameraId(0);  // Use a specific camera of the device
                integrator.setBeepEnabled(false);
                integrator.setBarcodeImageEnabled(true);
                integrator.initiateScan();
                 */
    }

    private void checkCard(String card) {
        mPresenter.checkCard(card);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Toast.makeText(getActivity(), "Lectura cancelada", Toast.LENGTH_LONG).show();
            } else {
                mEtCard.setText(result.getContents());
                resumeFromBarcode = true;

            }

        }
    }


    @Override
    public String getFragmentTitle() {
        return mFragmentTitle;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_production;
    }

    @Override
    protected void injectComponent(ApplicationComponent component) {
        component.inject(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mPresenter.setView(this);
        if (savedInstanceState == null) {
//            Log.i(TAG, "onViewCreated: no saveinstanceState");
            mPresenter.initialize();
        }


    }

    @Override
    public void renderProduction() {
        //mDashboard = dashboard;
        setActionBarTitle();
        //setNamePlayerCharacter();
        //setAbilityAdapter();
        //setStatAdapter();
    }

    @Override
    public void showCheckCard(Employee employee) {

        mTvEmpName.setText(employee.getNombre() + employee.getApellido1() + employee.getApellido2() + "");

    }

   


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {

        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG)
                //.setAction("Reintentar", null)
                .show();

    }

    @Override
    public Context context() {
        return getContext();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume() called");
        grantPermissions();
        mPresenter.resume();
        if (resumeFromBarcode){
            resumeFromBarcode = false;


        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause() called");

        mPresenter.pause();
    }

    @Override
    public void onDestroy() {
        mPresenter.destroy();
        super.onDestroy();
        Log.d(TAG, "onDestroy() called");
    }

    private void grantPermissions(){

        //  PEDIR PERMISOS, NECESARIO EN API > 23
        if (ActivityCompat.checkSelfPermission(getContext(),
                android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{
                            android.Manifest.permission.CAMERA
                    },1); //ESTE 1 NO SE PARA QUE ES
        } else {
            Log.e(TAG, "PERMISSIONS GRANTED");
        }

    }


}
