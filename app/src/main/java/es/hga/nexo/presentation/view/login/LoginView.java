package es.hga.nexo.presentation.view.login;

import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.Login;
import es.hga.nexo.presentation.view.LoadDataView;

public interface LoginView extends LoadDataView {

    void showLoginResponse(DuxWsResponse duxWsResponse);

    void loadDashboardFragment();

    void renderLogin(Login login);

    void setLoggedIn();

    void configureMenus(Login login);

//    void showChangeAbilityDialog(Ability ability);

//    void showChangeStatDialog(Stat stat);

}
