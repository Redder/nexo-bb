package es.hga.nexo.presentation.view.pendingorders;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.hga.nexo.MyApp;
import es.hga.nexo.R;
import es.hga.nexo.dagger.components.ApplicationComponent;
import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.PendingOrder;
import es.hga.nexo.presentation.base.BaseFragment;
import es.hga.nexo.presentation.view.activity.MainActivity;
import es.hga.nexo.presentation.view.pendingorderdetails.PendingOrderDetailsFragment;
import es.hga.nexo.presentation.view.pendingorders.adapter.PendingOrdersAdapter;


public class PendingOrdersFragment extends BaseFragment implements PendingOrdersView {

    private static final String TAG = "PendingOrdersFragment";
    private static final String mFragmentTitle = "Pedidos pendientes";

    @BindView(R.id.rv_pendingorders_list)
    RecyclerView mPendingOrdersRV;

    @BindView(R.id.ll_pendingorders_emptylist)
    LinearLayout mLlEmptyList;

    @Inject
    PendingOrdersPresenter mPresenter;

    PendingOrdersAdapter mPendingOrdersAdapter;

    PendingOrder mPendingOrder;

    public PendingOrdersFragment() {
    }

    public static PendingOrdersFragment newInstance() {
        Bundle args = new Bundle();

        PendingOrdersFragment fragment = new PendingOrdersFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectComponent(MyApp.get(getActivity()).getApplicationComponent());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        ((MainActivity)getActivity()).configFab(true,R.drawable.ic_refresh);
        if (view != null) {
            ButterKnife.bind(this, view);
            Log.i(TAG, "onCreateView");
            mPendingOrdersRV.setLayoutManager(new LinearLayoutManager(getContext()));
            mPendingOrdersRV.setNestedScrollingEnabled(false);
            //mAbilityScoresRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
            //mStatScoresRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        }

        ((MainActivity)getActivity()).getFab().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.reloadPendingOrdersList();
            }
        });

        return view;
    }

    @Override
    public String getFragmentTitle() {
        return mFragmentTitle;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_pendingorders;
    }

    @Override
    protected void injectComponent(ApplicationComponent component) {
        component.inject(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((MainActivity)getActivity()).setDrawerEnabled(true);
        mPresenter.setView(this);
        if (savedInstanceState == null) {
//            Log.i(TAG, "onViewCreated: no saveinstanceState");
            mPresenter.initialize();
        }
    }

    @Override
    public void renderPendingOrders(@NonNull List<PendingOrder> pendingOrderList) {
        Log.i(TAG, "renderPendingOrders");
        setActionBarTitle();
        setPendingOrdersAdapter(pendingOrderList);

        if (pendingOrderList.isEmpty()){
            mLlEmptyList.setVisibility(View.VISIBLE);
            mPendingOrdersRV.setVisibility(View.GONE);
        }else{
            mLlEmptyList.setVisibility(View.GONE);
            mPendingOrdersRV.setVisibility(View.VISIBLE);
        }

        //setNamePlayerCharacter();
        //setAbilityAdapter();
        //setStatAdapter();
    }

    private void setPendingOrdersAdapter(List<PendingOrder> pendingOrderList) {

        Log.d(TAG, "pendingOrderList size: " + pendingOrderList.size());

        if (mPendingOrdersAdapter == null) {
            mPendingOrdersAdapter = new PendingOrdersAdapter(pendingOrderList);
            //mPendingOrdersRV.setAdapter(mPendingOrdersAdapter);
            mPendingOrdersAdapter.getDetailsClick().subscribe(this::loadDetailsFragment);
            mPendingOrdersAdapter.getAcceptClick().subscribe(this::acceptPendingOrder);
            mPendingOrdersAdapter.getRejectClick().subscribe(this::rejectPendingOrder);
            //mStatusScoresRecycler.setAdapter(mStatusAdapter);
            //mPendingOrdersAdapter.getDetailsClick().subscribe(this::showAlert);
            //mPendingOrdersAdapter.getRemoveStatusClick().subscribe(mPresenter::removeStatus);
            //mPendingOrdersAdapter.getUpdateStatusClick().subscribe(mPresenter::updateStatus);
            Log.i(TAG, "setPendingOrdersAdapter: null");

        } else {
            Log.i(TAG, "setPendingOrdersAdapter: not null");
            Log.i(TAG, "" + mPendingOrdersRV.getAdapter());
            mPendingOrdersAdapter.setPendingOrdersList(pendingOrderList);
        }

        mPendingOrdersRV.setAdapter(mPendingOrdersAdapter);
    }

    private void loadDetailsFragment(int id) {
        Log.i(TAG,"pendingOrder ID: " + id);
        BaseFragment fragment = PendingOrderDetailsFragment.newInstance(id);
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.mainContainer, fragment)
                //.addToBackStack(null)
                .commit();

        ((MainActivity)getActivity()).getSupportActionBar().setTitle(fragment.getFragmentTitle());
    }

    private void acceptPendingOrder(int id) {

        new MaterialDialog.Builder(getContext())
                .title("Aceptar pedido")
                .content("¿Desea aceptar el pedido?")
                .positiveText("Ok")
                .negativeText("Cancelar")
                .cancelable(true)
                .autoDismiss(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        mPresenter.acceptPendingOrder(id);
                        dialog.dismiss();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();

        //mPresenter.acceptPendingOrder(id);
    }

    private void rejectPendingOrder(Integer id) {

        new MaterialDialog.Builder(getContext())
                .title("Rechazar pedido")
                .cancelable(true)
                .autoDismiss(false)
                .input("Motivo","", true, (dialog, input) -> {

                    mPresenter.rejectPendingOrder(id,input.toString());
                    dialog.dismiss();

                }).show();
    }

    @Override
    public void showErrorGetPendingOrders(@NonNull String text) {
        Log.i(TAG, "showErrorGetPendingOrders");

        Snackbar.make(getView(), text, Snackbar.LENGTH_LONG)
                .setAction("Reintentar", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Log.i("Snackbar", "Pulsada acción snackbar!");
                        mPresenter.initialize();
                    }
                }).show();
    }

    @Override
    public void showErrorPendingOrderRejected(@NonNull String text) {
        Log.i(TAG, "showErrorPendingOrderRejected");

        Snackbar.make(getView(), text, Snackbar.LENGTH_LONG)
                .setAction("Reintentar", null)
                .show();
    }

    @Override
    public void showErrorPendingOrderAccepted(@NonNull String text) {
        Log.i(TAG, "showErrorPendingOrderAccepted");

        Snackbar.make(getView(), text, Snackbar.LENGTH_LONG)
                .setAction("Reintentar", null)
                .show();
    }

    @Override
    public void showPendingOrderAccepted(@NonNull DuxWsResponse duxWsResponse) {
        Log.i(TAG, "showPendingOrderAccepted");

        Snackbar.make(getView(), duxWsResponse.getTexto(), Snackbar.LENGTH_LONG)
                .setAction("Acción", null).show();

    }

    @Override
    public void showPendingOrderRejected(@NonNull DuxWsResponse duxWsResponse) {
        Log.i(TAG, "showPendingOrderRejected");

        Snackbar.make(getView(), duxWsResponse.getTexto(), Snackbar.LENGTH_LONG)
                .setAction("Acción", null).show();

    }

    /*
    private void setStatAdapter() {
        if (mStatAdapter == null) {
            mStatAdapter = new StatAdapter(mPlayerCharacter.getStats());
            mStatScoresRecycler.setAdapter(mStatAdapter);
            mStatAdapter.getPositionClicks()
                    .subscribe(this::showStatAmountDialog, Throwable::printStackTrace);
        } else {
            mStatAdapter.setStats(mPlayerCharacter.getStats());
        }
    }

    private void setAbilityAdapter() {
        if (mAbilityAdapter == null) {
            mAbilityAdapter = new AbilityAdapter(mPlayerCharacter.getAbilities());
            mAbilityScoresRecycler.setAdapter(mAbilityAdapter);
            mAbilityAdapter.getPositionClicks()
                    .subscribe(this::showChangeAbilityDialog, Throwable::printStackTrace);
        } else {
            mAbilityAdapter.setAbilitiesScores(mPlayerCharacter.getAbilities());
        }
    }

    */


    //private void setNamePlayerCharacter(){
    //    mNameTextView.setText(mPlayerCharacter.getName());
    //}

    /*
    @OnClick(R.id.dashboard_text)
    public void clickDashboardText(){
        new MaterialDialog.Builder(getContext())
                .title(R.string.pc_name)
                .cancelable(true)
                .autoDismiss(false)
                .input(getString(R.string.pc_name), mPlayerCharacter.getName(), false, (dialog, input) -> {
                    if (input.length() > 0) {
                        mPlayerCharacter.setName(input.toString());
                        mPresenter.reloadPlayerCharacter(mPlayerCharacter);
                        dialog.dismiss();
                    }
                }).show();
    }
    */

    //@OnClick(R.id.fab)
    //public void clickFab(){

    //  showNewStatDialog()
    //};




    @Override
    public void showLoading() {
        super.showLoading();
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public Context context() {
        return getContext();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume() called");
        mPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause() called");
        mPresenter.pause();
    }

    @Override
    public void onDestroy() {
        mPresenter.destroy();
        super.onDestroy();
        Log.d(TAG, "onDestroy() called");
    }

    @Override
    public void onBackPressedFragment() {
        Log.d(TAG,"onBackPressed");
    }
}
