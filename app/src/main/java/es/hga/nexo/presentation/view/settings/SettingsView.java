package es.hga.nexo.presentation.view.settings;

import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.Settings;
import es.hga.nexo.presentation.view.LoadDataView;

public interface SettingsView extends LoadDataView {

    //void showLoginResponse(DuxWsResponse duxWsResponse);

    void renderSettings(Settings settings);
    void showSavedSettings();

//    void showChangeAbilityDialog(Ability ability);

//    void showChangeStatDialog(Stat stat);

}
