package es.hga.nexo.presentation.view.pendingorderdetails;

//import es.hga.nexo.domain.business.PlayerCharacter;

import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.PendingOrder;
import es.hga.nexo.presentation.view.LoadDataView;

public interface PendingOrderDetailsView extends LoadDataView {

    void renderPendingOrderDetails(PendingOrder pendingOrder);
    void showPendingOrderAccepted(DuxWsResponse duxWsResponse);
    void showPendingOrderRejected(DuxWsResponse duxWsResponse);
    void renderNotFoundPendingOrderDetails();
    void showErrorGetPendingOrderDetails(String text);
    void showErrorPendingOrderDetailsAccepted(String text);
    void showErrorPendingOrderDetailsRejected(String text);
    void loadPendingOrdersListFragment();

}
