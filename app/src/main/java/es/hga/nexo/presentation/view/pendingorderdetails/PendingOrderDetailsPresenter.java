package es.hga.nexo.presentation.view.pendingorderdetails;

import android.util.Log;

import java.io.IOException;
import java.net.IDN;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.PendingOrder;
import es.hga.nexo.domain.exceptions.ConnectionError;
import es.hga.nexo.domain.exceptions.IdNotFound;
import es.hga.nexo.domain.interactor.base.DefaultMaybeObserver;
import es.hga.nexo.domain.interactor.base.DefaultSingleObserver;
import es.hga.nexo.domain.interactor.model.RejectPendingOrderParam;
import es.hga.nexo.domain.interactor.pendingorders.AcceptPendingOrderUseCase;
import es.hga.nexo.domain.interactor.pendingorders.GetPendingOrderByIdUseCase;
import es.hga.nexo.domain.interactor.pendingorders.RejectPendingOrderUseCase;
import es.hga.nexo.presentation.base.Presenter;
import io.reactivex.Single;


class PendingOrderDetailsPresenter implements Presenter<PendingOrderDetailsView> {

    //private static final String TAG = "PODetailsPresenter";
    private static final String TAG = "PendingOrderDetailsPres";

    private final GetPendingOrderByIdUseCase mGetPendingOrderByIdUseCase;
    private final AcceptPendingOrderUseCase mAcceptPendingOrderUseCase;
    private final RejectPendingOrderUseCase mRejectPendingOrderUseCase;

    private PendingOrderDetailsView mPendingOrderDetailsView;

    //private GetPlayerCharacterUseCase mGetPlayerCharacterUseCase;
    //private UpdatePlayerCharacterUseCase mUpdatePlayerCharacterUseCase;

    private boolean initializing = false;

    int mIdPendingOrder;


    @Inject
    public PendingOrderDetailsPresenter(GetPendingOrderByIdUseCase getPendingOrderByIdUseCase,
                                        AcceptPendingOrderUseCase acceptPendingOrderUseCase,
                                        RejectPendingOrderUseCase rejectPendingOrderUseCase) {
        mGetPendingOrderByIdUseCase = getPendingOrderByIdUseCase;
        mAcceptPendingOrderUseCase = acceptPendingOrderUseCase;
        mRejectPendingOrderUseCase = rejectPendingOrderUseCase;
    }

    @Override
    public void resume() {
        if(!initializing){
            initialize();
        }
    }

    @Override
    public void pause() {
        Log.d(TAG, "pause() called");
    }

    @Override
    public void destroy() {
        mPendingOrderDetailsView = null;
        mGetPendingOrderByIdUseCase.dispose();
        //mUpdatePlayerCharacterUseCase.dispose();
    }

    @Override
    public void setView(PendingOrderDetailsView view) {
        mPendingOrderDetailsView = view;
    }

    @Override
    public void initialize() {
        Log.d(TAG, "initialize() called");
        initializing = true;
        //loadPendingOrder();
    }

    public void loadPendingOrder(int id){
        hideViewRetry();
        showViewLoading();
        getPendingOrder(id);
    }

    private void showPendingOrderInView(PendingOrder pendingOrder) {
        mPendingOrderDetailsView.renderPendingOrderDetails(pendingOrder);
    }

    private void showNotFoundPendingOrderInView() {
        mPendingOrderDetailsView.renderNotFoundPendingOrderDetails();
    }

    private void showPendingOrderAccepted(DuxWsResponse duxWsResponse) {
        mPendingOrderDetailsView.showPendingOrderAccepted(duxWsResponse);
    }

    private void showPendingOrderRejected(DuxWsResponse duxWsResponse) {
        mPendingOrderDetailsView.showPendingOrderRejected(duxWsResponse);
    }

    private void getPendingOrder(int id) {
        mGetPendingOrderByIdUseCase.execute(new PendingOrderObserver(),id);
    }

    public void acceptPendingOrder(int id) {
        Log.i(TAG,"acceptPendingOrder");
        showViewLoading();
        mAcceptPendingOrderUseCase.execute(new AcceptPendingOrderObserver(),id);
    }

    public void rejectPendingOrder(int id, String reason) {
        Log.i(TAG,"acceptPendingOrder");
        showViewLoading();
        RejectPendingOrderParam params = new RejectPendingOrderParam();
        params.setId(id);
        params.setReason(reason);
        mRejectPendingOrderUseCase.execute(new RejectPendingOrderObserver(),params);
    }

    private void showErrorGetPendingOrderDetails(String text) {
        mPendingOrderDetailsView.showErrorGetPendingOrderDetails(text);
    }

    private void showErrorAcceptPendingOrder(String text) {
        mPendingOrderDetailsView.showErrorGetPendingOrderDetails(text);
    }

    private void showErrorPendingOrderDetailsRejected(String text) {
        mPendingOrderDetailsView.showErrorPendingOrderDetailsRejected(text);
    }


    private void showErrorPendingOrderDetailsAccepted(String text) {
        mPendingOrderDetailsView.showErrorPendingOrderDetailsAccepted(text);
    }

    private void loadPendingOrdersList(){
        mPendingOrderDetailsView.loadPendingOrdersListFragment();
    }

    private final class PendingOrderObserver extends DefaultMaybeObserver<PendingOrder> {

        @Override public void onError(Throwable e) {
            Log.d(TAG, "PendingOrderObserver onERROR: " + e.toString());
            hideViewLoading();
            //showViewRetry();

            showErrorGetPendingOrderDetails(e.getMessage());
            //showNotFoundPendingOrderInView();

            initializing = false;
        }

        @Override public void onComplete() {
            hideViewLoading();
            initializing = false;
            showNotFoundPendingOrderInView();
        }

        @Override public void onSuccess(PendingOrder pendingOrder) {
            hideViewLoading();
            //Log.d(TAG, "pendingOrder id: " + pendingOrder.getId());
            showPendingOrderInView(pendingOrder);
            initializing = false;
        }

    }

    private final class AcceptPendingOrderObserver extends DefaultSingleObserver<DuxWsResponse> {

        @Override public void onError(Throwable e) {
            Log.d(TAG, "AcceptPendingOrderObserver onERROR: " + e.toString());
            hideViewLoading();
            showViewRetry();
            initializing = false;
            //showNotFoundPendingOrderInView();
            showErrorPendingOrderDetailsAccepted(e.getMessage());

        }

        @Override public void onSuccess(DuxWsResponse duxWsResponse) {
            hideViewLoading();
            //Log.d(TAG, "pendingOrder id: " + pendingOrder.getId());
            if (duxWsResponse.isOP_OK()){
                loadPendingOrdersList();
            }
            showPendingOrderAccepted(duxWsResponse);
            initializing = false;
        }
    }

    private final class RejectPendingOrderObserver extends DefaultSingleObserver<DuxWsResponse> {

        @Override public void onError(Throwable e) {
            Log.d(TAG, "RejectPendingOrderObserver onERROR: " + e.toString());
            hideViewLoading();
            showViewRetry();
            initializing = false;
            showErrorPendingOrderDetailsRejected(e.getMessage());
        }

        @Override public void onSuccess(DuxWsResponse duxWsResponse) {
            hideViewLoading();
            //Log.d(TAG, "pendingOrder id: " + pendingOrder.getId());
            showPendingOrderRejected(duxWsResponse);

            hideViewLoading();
            //Log.d(TAG, "pendingOrder id: " + pendingOrder.getId());
            if (duxWsResponse.isOP_OK()){
                loadPendingOrdersList();
            }

            showPendingOrderRejected(duxWsResponse);
            initializing = false;
        }
    }

    /*
    private final class GetPlayerCharacterObserver extends DefaultObserver<PlayerCharacter>{

        @Override
        public void onNext(PlayerCharacter playerCharacter) {
            Log.d(TAG, "onNext() called with: playerCharacter = [" + playerCharacter + "]");
            showPlayerCharacter(playerCharacter);
            initializing = false;
        }

        @Override
        public void onComplete() {
            hideViewLoading();
        }

        @Override
        public void onError(Throwable exception) {
            Log.d(TAG, "onError() called with: exception = [" + exception + "]");
            hideViewLoading();
            showViewRetry();
        }

    }
    */

    /*
    private final class UpdatePlayerCharacterObserver extends DefaultMaybeObserver<PlayerCharacter> {

        @Override
        public void onSuccess(PlayerCharacter playerCharacter) {
            Log.d(TAG, "onNext() called with: playerCharacter = [" + playerCharacter + "]");
            hideViewLoading();

            PlayerCharacter uiPC = new PlayerCharacter(playerCharacter);

            showPlayerCharacter(uiPC);

            initializing = false;
        }

        @Override
        public void onComplete() {
            hideViewLoading();
        }

        @Override
        public void onError(Throwable exception) {
            Log.d(TAG, "onError() called with: exception = [" + exception + "]");
            hideViewLoading();
            showViewRetry();
        }

    }
    */

    private void showViewLoading() {
        if (mPendingOrderDetailsView != null) {
            mPendingOrderDetailsView.showLoading();
        };
    }

    private void hideViewLoading() {
        if (mPendingOrderDetailsView != null){
            mPendingOrderDetailsView.hideLoading();
        }
    }

    private void showViewRetry() {
        mPendingOrderDetailsView.showRetry();
    }

    private void hideViewRetry() {
        mPendingOrderDetailsView.hideRetry();
    }
}
