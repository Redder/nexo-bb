package es.hga.nexo.presentation.view.presence;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

import javax.inject.Inject;

import es.hga.nexo.domain.business.Dashboard;
import es.hga.nexo.domain.business.NxLocation;
import es.hga.nexo.domain.business.Presence;
import es.hga.nexo.domain.interactor.Location.GetCurrentLocationUseCase;
import es.hga.nexo.domain.interactor.Location.SetLocationUseCase;
import es.hga.nexo.domain.interactor.base.DefaultCompletableObserver;
import es.hga.nexo.domain.interactor.base.DefaultSingleObserver;
import es.hga.nexo.domain.interactor.model.PresenceLocationParam;
import es.hga.nexo.domain.interactor.presence.SavePresenceUseCase;
import es.hga.nexo.presentation.base.Presenter;
import es.hga.nexo.presentation.helpers.LocationHelper;

class PresencePresenter implements Presenter<PresenceView> {

    private static final String TAG = "PresencePresenter";

    private PresenceView mPresenceView;

    private LocationHelper mLocationHelper;
    private LocationListener mLocationListenerNetwork;
    private LocationListener mLocationListenerGPS;

    private final SavePresenceUseCase mSavePresenceUseCase;
    private final SetLocationUseCase mSetLocationUseCase;
    private final GetCurrentLocationUseCase mGetCurrentLocationUseCase;

    private boolean initializing = false;

    private String card = "";

    @Inject
    PresencePresenter(LocationHelper locationHelper,
                      SavePresenceUseCase savePresenceUseCase,
                      SetLocationUseCase setLocationUseCase,
                      GetCurrentLocationUseCase getCurrentLocationUseCase) {
        mLocationHelper = locationHelper;
        mSavePresenceUseCase = savePresenceUseCase;
        mSetLocationUseCase = setLocationUseCase;
        mGetCurrentLocationUseCase = getCurrentLocationUseCase;
    }


    @Override
    public void resume() {
        Log.d(TAG, "resume: ");
        if(!initializing){
            Log.d(TAG, "resume: initialize");
            initialize();
        }
    }

    @Override
    public void pause() {

        Log.d(TAG,"pause");

        if (mLocationListenerNetwork != null) {
            Log.d(TAG, "pause: removing mLocationListenerNetwork");
            mLocationHelper.removeListener(mLocationListenerNetwork);
            mLocationListenerNetwork = null;
        }

        if (mLocationListenerGPS != null) {
            Log.d(TAG, "pause: removing mLocationListenerGPS");
            mLocationHelper.removeListener(mLocationListenerGPS);
            mLocationListenerGPS = null;
        }

    }

    public void savePresence(String card){
        Log.i(TAG,"savePresence");

        showViewLoading();

        this.card = card;

        mGetCurrentLocationUseCase.execute(new GetLocationToSavePresenceObserver());
    }

    @Override
    public void destroy() {
        mPresenceView = null;
    }

    @Override
    public void setView(PresenceView view) {
        mPresenceView = view;
    }

    @Override
    public void initialize() {
        Log.d(TAG, "initialize() called");
        initializing = true;
        loadPresence();

        createListenersGPS();

        initializing = false;

    }

    private void loadPresence(){
        hideViewRetry();
        showViewLoading();
        getPresence();
    }

    //private void showDashboard(Dashboard dashboard) {
    //    mPresenceView.renderPresence();
    //}

    private void getPresence() {
        mPresenceView.renderPresence();
        //mGetPlayerCharacterUseCase.execute(new GetPlayerCharacterObserver());
    }

    private void createListenersGPS(){
        //GPS
        // Define a listener that responds to location updates
        if (mLocationListenerNetwork == null) {
            mLocationListenerNetwork = new LocationListener() {
                public void onLocationChanged(Location location) {
                    // Called when a new location is found by the network location provider.
                    //makeUseOfNewLocation(location);
                    Log.d(TAG,"onLocationChanged Network: " + location);

                    //mPresenceView.showLocation(location);
                    mPresenceView.updatePosition(location);

                    NxLocation nxLocation = new NxLocation();
                    nxLocation.setLocation(location);

                    mSetLocationUseCase.execute(new SetLocationObserver(),nxLocation);

                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                    Log.d(TAG,"onStatusChanged Network: " + provider + " - status: " + status);
                }

                public void onProviderEnabled(String provider) {
                    Log.d(TAG,"onProviderEnabled Network: " + provider);
                }

                public void onProviderDisabled(String provider) {
                    Log.d(TAG,"onProviderDisabled: " + provider);
                }
            };

            mLocationHelper.setLocationListenerNetwork(mLocationListenerNetwork);
        }

        if (mLocationListenerGPS == null) {
            mLocationListenerGPS = new LocationListener() {
                public void onLocationChanged(Location location) {
                    // Called when a new location is found by the network location provider.
                    //makeUseOfNewLocation(location);
                    Log.d(TAG,"onLocationChanged GPS: " + location);

                    mPresenceView.updatePosition(location);

                    NxLocation nxLocation = new NxLocation();
                    nxLocation.setLocation(location);

                    mSetLocationUseCase.execute(new SetLocationObserver(),nxLocation);

                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                    Log.d(TAG,"onStatusChanged GPS: " + provider + " - status: " + status);
                }

                public void onProviderEnabled(String provider) {
                    Log.d(TAG,"onProviderEnabled GPS: " + provider);
                    mPresenceView.toggleIconGPS(true);
                }

                public void onProviderDisabled(String provider) {
                    Log.d(TAG,"onProviderDisabled GPS: " + provider);
                    mPresenceView.toggleIconGPS(false);
                }
            };

            mLocationHelper.setLocationListenerGPS(mLocationListenerGPS);
        }
    }

    private void showErrorSavePresence(String text) {
        mPresenceView.showErrorSavePresence(text);
    }

    private void showSavePresence(Presence presence) {
        mPresenceView.showSavePresence(presence);
    }


    private final class SavePresenceObserver extends DefaultSingleObserver<Presence> {

        @Override public void onError(Throwable e) {
            Log.d(TAG, "SavePresenceObserver onERROR: " + e.toString());
            hideViewLoading();
            showViewRetry();
            initializing = false;
            showErrorSavePresence(e.getMessage());
        }

        @Override public void onSuccess(Presence presence) {
            Log.d(TAG, "SavePresenceObserver onSuccess: ");
            hideViewLoading();

            showSavePresence(presence);

            initializing = false;
        }
    }

    private final class SetLocationObserver extends DefaultCompletableObserver{

        @Override public void onError(Throwable e) {
            Log.d(TAG, "SetLocationObserver onERROR: " + e.toString());
            hideViewLoading();
            showViewRetry();
            initializing = false;
            showErrorSavePresence(e.getMessage());
        }

        @Override public void onComplete() {
            Log.d(TAG, "SetLocationObserver onComplete: ");
            hideViewLoading();

            //showSavePresence(presence);

            initializing = false;
        }
    }

    private final class GetLocationToSavePresenceObserver extends DefaultSingleObserver<NxLocation>{

        @Override public void onError(Throwable e) {
            Log.d(TAG, "GetLocationToSavePresenceObserver onERROR: " + e.toString());
            hideViewLoading();
            showViewRetry();
            initializing = false;
            showErrorSavePresence("Error obteniendo la posición.");
        }

        @Override public void onSuccess(NxLocation nxLocation) {
            Log.d(TAG, "GetLocationToSavePresenceObserver onSuccess: ");
            hideViewLoading();

            initializing = false;

            PresenceLocationParam presenceLocationParam = new PresenceLocationParam();

            presenceLocationParam.setCard(card);
            presenceLocationParam.setNxLocation(nxLocation);

            mSavePresenceUseCase.execute(new SavePresenceObserver(), presenceLocationParam);

        }
    }


    private void showViewLoading() {
        mPresenceView.showLoading();
    }

    private void hideViewLoading() {
        mPresenceView.hideLoading();
    }

    private void showViewRetry() {
        mPresenceView.showRetry();
    }

    private void hideViewRetry() {
        mPresenceView.hideRetry();
    }
}
