package es.hga.nexo.presentation.view.dashboard;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

import javax.inject.Inject;

//import es.hga.nexo.domain.business.PlayerCharacter;
//import es.hga.nexo.domain.interactor.base.DefaultMaybeObserver;
//import es.hga.nexo.domain.interactor.base.DefaultObserver;
//import es.hga.nexo.domain.interactor.playercharacter.GetPlayerCharacterUseCase;
//import es.hga.nexo.domain.interactor.playercharacter.UpdatePlayerCharacterUseCase;
import es.hga.nexo.domain.business.Dashboard;
import es.hga.nexo.presentation.base.Presenter;
import es.hga.nexo.presentation.helpers.LocationHelper;

class DashboardPresenter implements Presenter<DashboardView> {

    private static final String TAG = "DashboardPresenter";

    private DashboardView mDashboardView;

    //private GetPlayerCharacterUseCase mGetPlayerCharacterUseCase;
    //private UpdatePlayerCharacterUseCase mUpdatePlayerCharacterUseCase;

    private LocationHelper mLocationHelper;
    private LocationListener mLocationListenerNetwork;
    private LocationListener mLocationListenerGPS;

    private boolean initializing = false;


    @Inject
    DashboardPresenter(LocationHelper locationHelper) {
        mLocationHelper = locationHelper;
    }


    @Override
    public void resume() {
        if(!initializing){
            initialize();
        }
    }

    @Override
    public void pause() {

        Log.d(TAG,"pause");
        if (mLocationListenerNetwork != null) {
            mLocationHelper.removeListener(mLocationListenerNetwork);
        }

        if (mLocationListenerGPS != null) {
            mLocationHelper.removeListener(mLocationListenerGPS);
        }

    }

    @Override
    public void destroy() {
        mDashboardView = null;
        //mGetPlayerCharacterUseCase.dispose();
        //mUpdatePlayerCharacterUseCase.dispose();
    }

    @Override
    public void setView(DashboardView view) {
        mDashboardView = view;
    }

    @Override
    public void initialize() {
        Log.d(TAG, "initialize() called");
        initializing = true;
        loadDashboard();

        //GPS
        // Define a listener that responds to location updates
        mLocationListenerNetwork = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                //makeUseOfNewLocation(location);
                Log.d(TAG,"onLocationChanged: " + location);
                mDashboardView.showLocation(location);
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
                Log.d(TAG,"onStatusChanged: " + provider + " - status: " + status);
            }

            public void onProviderEnabled(String provider) {
                Log.d(TAG,"onProviderEnabled: " + provider);
            }

            public void onProviderDisabled(String provider) {
                Log.d(TAG,"onProviderDisabled: " + provider);
            }
        };

        mLocationListenerGPS = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                //makeUseOfNewLocation(location);
                Log.d(TAG,"onLocationChanged: " + location);
                mDashboardView.showLocation(location);
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
                Log.d(TAG,"onStatusChanged: " + provider + " - status: " + status);
            }

            public void onProviderEnabled(String provider) {
                Log.d(TAG,"onProviderEnabled: " + provider);
            }

            public void onProviderDisabled(String provider) {
                Log.d(TAG,"onProviderDisabled: " + provider);
            }
        };

        mLocationHelper.setLocationListenerNetwork(mLocationListenerNetwork);
        mLocationHelper.setLocationListenerGPS(mLocationListenerGPS);
    }

    private void loadDashboard(){
        hideViewRetry();
        showViewLoading();
        getDashboard();
    }

    private void showDashboard(Dashboard dashboard) {
        mDashboardView.renderDashboard(dashboard);
    }

    private void getDashboard() {
        mDashboardView.renderDashboard(new Dashboard());
        //mGetPlayerCharacterUseCase.execute(new GetPlayerCharacterObserver());
    }

    void updateDashboard(Dashboard dashboard){
        //mUpdatePlayerCharacterUseCase.execute(new UpdatePlayerCharacterObserver(), playerCharacter);
    }

    private void showViewLoading() {
        mDashboardView.showLoading();
    }

    private void hideViewLoading() {
        mDashboardView.hideLoading();
    }

    private void showViewRetry() {
        mDashboardView.showRetry();
    }

    private void hideViewRetry() {
        mDashboardView.hideRetry();
    }
}
