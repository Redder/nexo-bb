package es.hga.nexo.presentation.helpers;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import javax.inject.Inject;

import es.hga.nexo.MyApp;


public class LocationHelper {

    private static final String TAG = "LocationHelper";
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 7171;

    private LocationManager locationManager;

    Context mAppContext;

    @Inject
    public LocationHelper(Context appContext){
        Log.d(TAG,"Constructor");
        mAppContext = appContext;
        locationManager = (LocationManager)mAppContext.getSystemService(Context.LOCATION_SERVICE);
    }

    public LocationManager getLocationManager(){
        return locationManager;
    }

    public void setLocationListenerNetwork(LocationListener locationListener){
        try{

            //https://stackoverflow.com/questions/39114970/checkpermissions-error-for-locationmanager-in-android-studio

            if (ContextCompat.checkSelfPermission(mAppContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) { // fine permission is granted
                // Todo
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
            }else{
                ActivityCompat.requestPermissions((Activity)mAppContext, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_REQUEST_CODE);
            }


        }catch (Exception e){
            Log.d(TAG,"ERROR LocationHelper.setLocationListener: " + e.toString());
        }


    }

    public void setLocationListenerGPS(LocationListener locationListener){
        try{

            //https://stackoverflow.com/questions/39114970/checkpermissions-error-for-locationmanager-in-android-studio

            if (ContextCompat.checkSelfPermission(mAppContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) { // fine permission is granted
                // Todo
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            }else{
                ActivityCompat.requestPermissions((Activity)mAppContext, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_REQUEST_CODE);
            }


        }catch (Exception e){
            Log.d(TAG,"ERROR LocationHelper.setLocationListener: " + e.toString());
        }


    }

    public void removeListener(LocationListener locationListener){
        // Remove the listener you previously added
        Log.d(TAG, "removeListener: ");
        locationManager.removeUpdates(locationListener);
    }

}
