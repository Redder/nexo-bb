package es.hga.nexo.presentation.view.production;

import es.hga.nexo.domain.business.Employee;
import es.hga.nexo.presentation.view.LoadDataView;

public interface ProductionView extends LoadDataView {

    void renderProduction();

    void showCheckCard(Employee employee);


}
