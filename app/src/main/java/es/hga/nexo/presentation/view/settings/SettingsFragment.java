package es.hga.nexo.presentation.view.settings;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.hga.nexo.MyApp;
import es.hga.nexo.R;
import es.hga.nexo.dagger.components.ApplicationComponent;
import es.hga.nexo.domain.business.Settings;
import es.hga.nexo.presentation.base.BaseFragment;
import es.hga.nexo.presentation.view.activity.MainActivity;
import es.hga.nexo.presentation.view.login.LoginFragment;
import es.hga.nexo.presentation.view.pendingorders.PendingOrdersFragment;


public class SettingsFragment extends BaseFragment implements SettingsView {

    private static final String TAG = "SettingsFragment";
    private static final String mFragmentTitle = "Settings";

    @BindView(R.id.et_settings_urlserver)
    EditText mUrlServer;

    @BindView(R.id.et_settings_company)
    EditText mCompany;

    @BindView(R.id.btn_settings_save)
    Button mBtnSave;

    @Inject
    SettingsPresenter mPresenter;

    Settings mSettings;

    public SettingsFragment() {
    }

    public static SettingsFragment newInstance() {
        Bundle args = new Bundle();

        SettingsFragment fragment = new SettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectComponent(MyApp.get(getActivity()).getApplicationComponent());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view != null) {
            ButterKnife.bind(this, view);
            Log.i(TAG, "onCreateView");
            ((MainActivity)getActivity()).configFab(false,0);
            //mAbilityScoresRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
            //mStatScoresRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        }
        return view;
    }

    @Override
    public String getFragmentTitle() {
        return mFragmentTitle;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_settings;
    }

    @Override
    protected void injectComponent(ApplicationComponent component) {
        component.inject(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((MainActivity)getActivity()).setDrawerEnabled(false);

        mPresenter.setView(this);
        if (savedInstanceState == null) {
//            Log.i(TAG, "onViewCreated: no saveinstanceState");
            mPresenter.initialize();
        }

    }

    @Override
    public void renderSettings(Settings settings){

        mUrlServer.setText(settings.getUrlServer());
        mCompany.setText(settings.getCompany());

        mBtnSave.setOnClickListener(v -> {
            //Log.i(TAG,"btn accept click");

            if (mUrlServer.getText().length() > 0){
                mSettings = new Settings();
                mSettings.setUrlServer(mUrlServer.getText() + "");
                mSettings.setCompany(mCompany.getText() + "");
                mPresenter.saveSettings(mSettings);
            }

        });

        //Si esta logueado no puede cambiar estos valores
        mUrlServer.setEnabled(!((MainActivity)getActivity()).isLoggedIn());
        mCompany.setEnabled(!((MainActivity)getActivity()).isLoggedIn());
        mBtnSave.setEnabled(!((MainActivity)getActivity()).isLoggedIn());


    }

    @Override
    public void showSavedSettings(){

        Snackbar.make(getView(), "Guardado correcto", Snackbar.LENGTH_LONG)
                .setAction("Acción", null).show();

    }



    //@OnClick(R.id.fab)
    //public void clickFab(){

    //  showNewStatDialog()
    //};




    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public Context context() {
        return getContext();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume() called");
        mPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause() called");
        mPresenter.pause();
    }

    @Override
    public void onDestroy() {
        mPresenter.destroy();
        super.onDestroy();
        Log.d(TAG, "onDestroy() called");
    }

    @Override
    public void onBackPressedFragment() {
        Log.d(TAG,"onBackPressed");

        if (((MainActivity)getActivity()).isLoggedIn()){
            BaseFragment fragment = PendingOrdersFragment.newInstance();
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.mainContainer, fragment)
                    //.addToBackStack(null)
                    .commit();
        }else{
            BaseFragment fragment = LoginFragment.newInstance();
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.mainContainer, fragment)
                    //.addToBackStack(null)
                    .commit();
        }

    }
}
