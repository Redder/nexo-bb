package es.hga.nexo.presentation.view.dashboard;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.hga.nexo.MyApp;
import es.hga.nexo.R;
import es.hga.nexo.dagger.components.ApplicationComponent;
import es.hga.nexo.domain.business.Dashboard;
import es.hga.nexo.presentation.base.BaseFragment;
import es.hga.nexo.presentation.view.activity.MainActivity;


public class DashboardFragment extends BaseFragment implements DashboardView {

    private static final String TAG = "DashboardFragment";
    private static final String mFragmentTitle = "Dashboard";

    @BindView(R.id.dashboard_latitude)
    TextView mLatitude;

    @BindView(R.id.dashboard_longitude)
    TextView mLongitude;

    @BindView(R.id.dashboard_accuarcy)
    TextView mAccuarcy;

    @BindView(R.id.dashboard_altitude)
    TextView mAltitude;

    @BindView(R.id.dashboard_provider)
    TextView mProvider;

    @Inject
    DashboardPresenter mPresenter;

    Dashboard mDashboard;

    //GPS
    LocationManager locationManager;

    public DashboardFragment() {

    }

    public static DashboardFragment newInstance() {
        Bundle args = new Bundle();

        DashboardFragment fragment = new DashboardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectComponent(MyApp.get(getActivity()).getApplicationComponent());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        ((MainActivity)getActivity()).setDrawerEnabled(true);
        ((MainActivity)getActivity()).configFab(false,0);

        locationManager = (LocationManager)getContext().getSystemService(getContext().LOCATION_SERVICE);

        if (view != null) {
            ButterKnife.bind(this, view);
            Log.i(TAG, "onCreateView");
            ((MainActivity)getActivity()).configFab(false,0);
            //mAbilityScoresRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
            //mStatScoresRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        }
        return view;
    }

    @Override
    public String getFragmentTitle() {
        return mFragmentTitle;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_dashboard;
    }

    @Override
    protected void injectComponent(ApplicationComponent component) {
        component.inject(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mPresenter.setView(this);
        if (savedInstanceState == null) {
//            Log.i(TAG, "onViewCreated: no saveinstanceState");
            mPresenter.initialize();
        }
    }

    public void showLocation(Location location){
        mLatitude.setText(location.getLatitude() + "");
        mLongitude.setText(location.getLongitude() + "");
        mAccuarcy.setText(location.getAccuracy() + "");
        mAltitude.setText(location.getAltitude() + "");
        mProvider.setText(location.getProvider() + "");
    }

    @Override
    public void renderDashboard(@NonNull Dashboard dashboard) {
        mDashboard = dashboard;
        setActionBarTitle();
        //setNamePlayerCharacter();
        //setAbilityAdapter();
        //setStatAdapter();
    }

    /*
    private void setStatAdapter() {
        if (mStatAdapter == null) {
            mStatAdapter = new StatAdapter(mPlayerCharacter.getStats());
            mStatScoresRecycler.setAdapter(mStatAdapter);
            mStatAdapter.getPositionClicks()
                    .subscribe(this::showStatAmountDialog, Throwable::printStackTrace);
        } else {
            mStatAdapter.setStats(mPlayerCharacter.getStats());
        }
    }

    private void setAbilityAdapter() {
        if (mAbilityAdapter == null) {
            mAbilityAdapter = new AbilityAdapter(mPlayerCharacter.getAbilities());
            mAbilityScoresRecycler.setAdapter(mAbilityAdapter);
            mAbilityAdapter.getPositionClicks()
                    .subscribe(this::showChangeAbilityDialog, Throwable::printStackTrace);
        } else {
            mAbilityAdapter.setAbilitiesScores(mPlayerCharacter.getAbilities());
        }
    }

    */


    //private void setNamePlayerCharacter(){
    //    mNameTextView.setText(mPlayerCharacter.getName());
    //}

    /*
    @OnClick(R.id.dashboard_text)
    public void clickDashboardText(){
        new MaterialDialog.Builder(getContext())
                .title(R.string.pc_name)
                .cancelable(true)
                .autoDismiss(false)
                .input(getString(R.string.pc_name), mPlayerCharacter.getName(), false, (dialog, input) -> {
                    if (input.length() > 0) {
                        mPlayerCharacter.setName(input.toString());
                        mPresenter.reloadPlayerCharacter(mPlayerCharacter);
                        dialog.dismiss();
                    }
                }).show();
    }
    */

    //@OnClick(R.id.fab)
    //public void clickFab(){

    //  showNewStatDialog()
    //};




    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public Context context() {
        return getContext();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume() called");
        mPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause() called");
        mPresenter.pause();
    }

    @Override
    public void onDestroy() {
        mPresenter.destroy();
        super.onDestroy();
        Log.d(TAG, "onDestroy() called");
    }
}
