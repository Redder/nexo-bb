package es.hga.nexo.presentation.view.production;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

import javax.inject.Inject;

import es.hga.nexo.domain.business.Employee;
import es.hga.nexo.domain.business.NxLocation;
import es.hga.nexo.domain.business.Presence;
import es.hga.nexo.domain.interactor.Location.GetCurrentLocationUseCase;
import es.hga.nexo.domain.interactor.Location.SetLocationUseCase;
import es.hga.nexo.domain.interactor.base.DefaultCompletableObserver;
import es.hga.nexo.domain.interactor.base.DefaultSingleObserver;
import es.hga.nexo.domain.interactor.model.PresenceLocationParam;
import es.hga.nexo.domain.interactor.presence.SavePresenceUseCase;
import es.hga.nexo.domain.interactor.production.CheckProductionCardUseCase;
import es.hga.nexo.presentation.base.Presenter;
import es.hga.nexo.presentation.helpers.LocationHelper;
import es.hga.nexo.presentation.view.presence.PresenceView;

class ProductionPresenter implements Presenter<ProductionView> {

    private static final String TAG = "PresencePresenter";

    private ProductionView mProductionView;

    private final CheckProductionCardUseCase mCheckProductionCardUseCase;

    private boolean initializing = false;

    @Inject
    ProductionPresenter(CheckProductionCardUseCase checkProductionCardUseCase) {
        mCheckProductionCardUseCase = checkProductionCardUseCase;
    }


    @Override
    public void resume() {
        Log.d(TAG, "resume: ");
        if(!initializing){
            Log.d(TAG, "resume: initialize");
            initialize();
        }
    }

    @Override
    public void pause() {

        Log.d(TAG,"pause");

    }


    @Override
    public void destroy() {
        mProductionView = null;
    }

    @Override
    public void setView(ProductionView view) {
        mProductionView = view;
    }

    @Override
    public void initialize() {
        Log.d(TAG, "initialize() called");
        initializing = true;
        loadProduction();

        initializing = false;

    }

    private void loadProduction(){
        hideViewRetry();
        showViewLoading();
        getProduction();
    }

    public void checkCard(String card){
        Log.i(TAG,"checkCard");

        showViewLoading();

        mCheckProductionCardUseCase.execute(new CheckProductionCardObserver(),card);
    }

    //private void showDashboard(Dashboard dashboard) {
    //    mPresenceView.renderProduction();
    //}

    private void getProduction() {
        mProductionView.renderProduction();
        //mGetPlayerCharacterUseCase.execute(new GetPlayerCharacterObserver());
    }

    private void showErrorProduction(String text) {
        mProductionView.showError(text);
    }

    private void showCheckCard(Employee employee) {
        mProductionView.showCheckCard(employee);
    }

    private final class CheckProductionCardObserver extends DefaultSingleObserver<Employee>{

        @Override public void onError(Throwable e) {
            Log.d(TAG, "CheckProductionCardObserver onERROR: " + e.toString());
            hideViewLoading();
            showViewRetry();
            initializing = false;
            showErrorProduction(e.toString());
        }

        @Override public void onSuccess(Employee employee) {
            Log.d(TAG, "CheckProductionCardObserver onSuccess: ");
            hideViewLoading();

            initializing = false;

            showCheckCard(employee);
        }
    }


    private void showViewLoading() {
        mProductionView.showLoading();
    }

    private void hideViewLoading() {
        mProductionView.hideLoading();
    }

    private void showViewRetry() {
        mProductionView.showRetry();
    }

    private void hideViewRetry() {
        mProductionView.hideRetry();
    }
}
