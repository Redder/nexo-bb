package es.hga.nexo.presentation.view.pendingorders;

import android.util.Log;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.PendingOrder;
import es.hga.nexo.domain.interactor.base.DefaultSingleObserver;
import es.hga.nexo.domain.interactor.model.RejectPendingOrderParam;
import es.hga.nexo.domain.interactor.pendingorders.AcceptPendingOrderUseCase;
import es.hga.nexo.domain.interactor.pendingorders.GetPendingOrdersListUseCase;
import es.hga.nexo.domain.interactor.pendingorders.RejectPendingOrderUseCase;
import es.hga.nexo.domain.interactor.pendingorders.ReloadPendingOrdersListUseCase;
import es.hga.nexo.presentation.base.Presenter;

import es.hga.nexo.domain.interactor.base.DefaultMaybeObserver;


class PendingOrdersPresenter implements Presenter<PendingOrdersView> {

    private static final String TAG = "PendingOrdersPresenter";

    private final GetPendingOrdersListUseCase mGetPendingOrdersListUseCase;
    //force get data from network
    private final ReloadPendingOrdersListUseCase mReloadPendingOrdersListUseCase;
    private final AcceptPendingOrderUseCase mAcceptPendingOrderUseCase;
    private final RejectPendingOrderUseCase mRejectPendingOrderUseCase;

    private PendingOrdersView mPendingOrdersView;

    private boolean initializing = false;


    @Inject
    PendingOrdersPresenter(GetPendingOrdersListUseCase getPendingOrdersListUseCase,
                           ReloadPendingOrdersListUseCase reloadPendingOrdersListUseCase,
                           AcceptPendingOrderUseCase acceptPendingOrderUseCase,
                           RejectPendingOrderUseCase rejectPendingOrderUseCase) {
        mGetPendingOrdersListUseCase = getPendingOrdersListUseCase;
        mReloadPendingOrdersListUseCase = reloadPendingOrdersListUseCase;
        mAcceptPendingOrderUseCase = acceptPendingOrderUseCase;
        mRejectPendingOrderUseCase = rejectPendingOrderUseCase;
    }


    @Override
    public void resume() {
        Log.d(TAG," on resume pre - initializing: " + initializing);
        if(!initializing){
            Log.d(TAG," on resume post: " + initializing);
            initialize();
        }
    }

    @Override
    public void pause() {

    }



    @Override
    public void destroy() {
        mPendingOrdersView = null;
        mGetPendingOrdersListUseCase.dispose();
        //mUpdatePlayerCharacterUseCase.dispose();
    }

    @Override
    public void setView(PendingOrdersView view) {
        mPendingOrdersView = view;
    }

    @Override
    public void initialize() {
        Log.d(TAG, "initialize() called");
        initializing = true;
        loadPendingOrdersList();
    }

    public void reloadPendingOrdersList() {
        hideViewRetry();
        showViewLoading();
        mReloadPendingOrdersListUseCase.execute(new PendingOrderListObserver());
    }

    private void loadPendingOrdersList(){
        hideViewRetry();
        showViewLoading();
        getPendingOrdersList();
    }

    private void renderPendingOrders(List<PendingOrder> pendingOrdersList) {
        mPendingOrdersView.renderPendingOrders(pendingOrdersList);
    }

    public void acceptPendingOrder(int id) {
        Log.i(TAG,"acceptPendingOrder");
        showViewLoading();
        mAcceptPendingOrderUseCase.execute(new AcceptPendingOrderObserver(),id);
    }

    public void rejectPendingOrder(int id, String reason) {
        Log.i(TAG,"acceptPendingOrder");

        showViewLoading();

        RejectPendingOrderParam params = new RejectPendingOrderParam();

        params.setId(id);
        params.setReason(reason);

        mRejectPendingOrderUseCase.execute(new PendingOrdersPresenter.RejectPendingOrderObserver(), params);
    }

    private void showErrorGetPendingOrders(String text) {
        mPendingOrdersView.showErrorGetPendingOrders(text);
    }

    private void showPendingOrderAccepted(DuxWsResponse duxWsResponse) {
        mPendingOrdersView.showPendingOrderAccepted(duxWsResponse);
    }

    private void showPendingOrderRejected(DuxWsResponse duxWsResponse) {
        mPendingOrdersView.showPendingOrderRejected(duxWsResponse);
    }

    private void showErrorPendingOrderAccepted(String text) {
        mPendingOrdersView.showErrorPendingOrderAccepted(text);
    }

    private void showErrorPendingOrderRejected(String text) {
        mPendingOrdersView.showErrorPendingOrderRejected(text);
    }

    private void getPendingOrdersList() {
        mGetPendingOrdersListUseCase.execute(new PendingOrderListObserver());
    }

    private final class PendingOrderListObserver extends DefaultMaybeObserver<List<PendingOrder>> {

        @Override public void onComplete() {
            hideViewLoading();
            Log.d(TAG, "PendingOrderListObserver onComplete: ");
            renderPendingOrders(new LinkedList<>());
            initializing = false;
        }

        @Override public void onError(Throwable e) {
            hideViewLoading();
            showViewRetry();
            Log.d(TAG, "PendingOrderListObserver onError: ");
            renderPendingOrders(new LinkedList<>());
            showErrorGetPendingOrders(e.getMessage());
            initializing = false;
        }

        @Override public void onSuccess(List<PendingOrder> pendingOrdersList) {
            hideViewLoading();

            List<PendingOrder> uiList =new LinkedList<>();

            uiList.addAll(pendingOrdersList);

            Log.d(TAG, "PendingOrderListObserver onSuccess uiList count: " + uiList.size());

            initializing = false;

            renderPendingOrders(uiList);

        }
    }

    private final class AcceptPendingOrderObserver extends DefaultSingleObserver<DuxWsResponse> {

        @Override public void onError(Throwable e) {
            Log.d(TAG, "AcceptPendingOrderObserver onERROR: " + e.toString());
            hideViewLoading();
            showViewRetry();
            initializing = false;
            showErrorPendingOrderAccepted(e.getMessage());
        }

        @Override public void onSuccess(DuxWsResponse duxWsResponse) {
            hideViewLoading();
            //Log.d(TAG, "pendingOrder id: " + pendingOrder.getId());
            if (duxWsResponse.isOP_OK()){
                loadPendingOrdersList();
            }
            showPendingOrderAccepted(duxWsResponse);
            initializing = false;
        }
    }

    private final class RejectPendingOrderObserver extends DefaultSingleObserver<DuxWsResponse> {

        @Override public void onError(Throwable e) {
            Log.d(TAG, "RejectPendingOrderObserver onERROR: " + e.toString());
            hideViewLoading();
            showViewRetry();
            initializing = false;
            showErrorPendingOrderRejected(e.getMessage());
        }

        @Override public void onSuccess(DuxWsResponse duxWsResponse) {
            hideViewLoading();
            //Log.d(TAG, "pendingOrder id: " + pendingOrder.getId());
            if (duxWsResponse.isOP_OK()){
                loadPendingOrdersList();
            }

            showPendingOrderRejected(duxWsResponse);
            initializing = false;
        }
    }

    /*
    private final class GetPlayerCharacterObserver extends DefaultObserver<PlayerCharacter>{

        @Override
        public void onNext(PlayerCharacter playerCharacter) {
            Log.d(TAG, "onNext() called with: playerCharacter = [" + playerCharacter + "]");
            showPlayerCharacter(playerCharacter);
            initializing = false;
        }

        @Override
        public void onComplete() {
            hideViewLoading();
        }

        @Override
        public void onError(Throwable exception) {
            Log.d(TAG, "onError() called with: exception = [" + exception + "]");
            hideViewLoading();
            showViewRetry();
        }

    }
    */

    /*
    private final class UpdatePlayerCharacterObserver extends DefaultMaybeObserver<PlayerCharacter> {

        @Override
        public void onSuccess(PlayerCharacter playerCharacter) {
            Log.d(TAG, "onNext() called with: playerCharacter = [" + playerCharacter + "]");
            hideViewLoading();

            PlayerCharacter uiPC = new PlayerCharacter(playerCharacter);

            showPlayerCharacter(uiPC);

            initializing = false;
        }

        @Override
        public void onComplete() {
            hideViewLoading();
        }

        @Override
        public void onError(Throwable exception) {
            Log.d(TAG, "onError() called with: exception = [" + exception + "]");
            hideViewLoading();
            showViewRetry();
        }

    }
    */

    private void showViewLoading() {
        mPendingOrdersView.showLoading();
    }

    private void hideViewLoading() {
        mPendingOrdersView.hideLoading();
    }

    private void showViewRetry() {
        mPendingOrdersView.showRetry();
    }

    private void hideViewRetry() {
        mPendingOrdersView.hideRetry();
    }
}
