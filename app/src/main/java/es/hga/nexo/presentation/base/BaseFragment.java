package es.hga.nexo.presentation.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import es.hga.nexo.dagger.components.ApplicationComponent;
import es.hga.nexo.presentation.view.activity.MainActivity;

//import dnd.furkhail.bonuscalculator.dagger.components.ApplicationComponent;

public abstract class BaseFragment extends Fragment {
    private Context context;
    private static final String TAG = "BaseFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(getLayoutResource(), container, false);

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        if (view != null) {
            view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_UP) {
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            Log.d(TAG, "back captured on fragment");
                            onBackPressedFragment();
                            return true;
                        }
                    }


                    return false;
                }
            });
        }

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public Context getContext() {
        return context;
    }

    protected abstract int getLayoutResource();

    public abstract String getFragmentTitle();

    public void setActionBarTitle() {
        ((MainActivity)getActivity()).getSupportActionBar().setTitle(getFragmentTitle());
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public void showLoading() {
        if(getActivity() instanceof BaseActivity) {
            ((BaseActivity)getActivity()).showLoading();
        }
    }

    public void hideLoading() {
        if(getActivity() instanceof BaseActivity) {
            ((BaseActivity)getActivity()).hideLoading();
        }
    }

    protected abstract void injectComponent(ApplicationComponent component);

    protected void onBackPressedFragment(){

    };
}
