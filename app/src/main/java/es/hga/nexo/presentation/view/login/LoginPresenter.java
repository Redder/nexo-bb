package es.hga.nexo.presentation.view.login;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

import javax.inject.Inject;

import es.hga.nexo.MyApp;
import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.Login;
import es.hga.nexo.domain.interactor.base.DefaultMaybeObserver;
import es.hga.nexo.domain.interactor.base.DefaultSingleObserver;
import es.hga.nexo.domain.interactor.login.DoLoginUseCase;
import es.hga.nexo.domain.interactor.login.GetLoginInfoUseCase;
import es.hga.nexo.domain.interactor.login.SetPushTokenUseCase;
import es.hga.nexo.presentation.base.Presenter;


class LoginPresenter implements Presenter<LoginView> {

    private static final String TAG = "LoginPresenter";

    private LoginView mLoginView;

    private boolean initializing = false;

    private DoLoginUseCase mDoLoginUseCase;
    private GetLoginInfoUseCase mGetLoginInfoUseCase;
    private SetPushTokenUseCase mSetPushTokenUseCase;

    @Inject
    LoginPresenter(DoLoginUseCase doLoginUseCase,
                   GetLoginInfoUseCase getLoginInfoUseCase,
                   SetPushTokenUseCase setPushTokenUseCase) {
        mDoLoginUseCase = doLoginUseCase;
        mGetLoginInfoUseCase = getLoginInfoUseCase;
        mSetPushTokenUseCase = setPushTokenUseCase;
    }


    @Override
    public void resume() {
        if(!initializing){
            initialize();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        mLoginView = null;
        //mGetPlayerCharacterUseCase.dispose();
        //mUpdatePlayerCharacterUseCase.dispose();
    }

    @Override
    public void setView(LoginView view) {
        mLoginView = view;
    }

    @Override
    public void initialize() {
        Log.d(TAG, "initialize() called");
        initializing = true;
        loadLogin();
    }

    private void loadLogin(){
        hideViewRetry();
        showViewLoading();
        getLoginInfo();
    }

    private void showLoginResponse(DuxWsResponse duxWsResponse) {
        mLoginView.showLoginResponse(duxWsResponse);
    }

    private void showLoginInfo(Login login) {
        mLoginView.renderLogin(login);
    }

    private void loadDashboardFragment() {
        mLoginView.loadDashboardFragment();
    }

    private void configureMenus(Login login) {
        mLoginView.configureMenus(login);
    }

    private void getLoginInfo() {
        mGetLoginInfoUseCase.execute(new GetLoginInfoObserver());
    }

    public void doLogin(Login login){
        showViewLoading();
        mDoLoginUseCase.execute(new DoLoginObserver(), login);
    }

    private void registerFirebase(){
        /////////////
        //FIREBASE

        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG,"Token: " + token);
        if (token != null && token.length() > 0){
            mSetPushTokenUseCase.execute(new SetPushTokenObserver(), token);
        }

        /////////////
    }


    private final class DoLoginObserver extends DefaultMaybeObserver<Login> {

        @Override public void onError(Throwable e) {
            Log.d(TAG, "RejectPendingOrderObserver onERROR: " + e.toString());
            hideViewLoading();
            //showViewRetry();
            DuxWsResponse d = new DuxWsResponse();
            d.setOP_OK(false);
            d.setTexto(e.getMessage());

            showLoginResponse(d);
        }

        @Override public void onSuccess(Login login) {
            hideViewLoading();
            //Log.d(TAG, "pendingOrder id: " + pendingOrder.getId());

            if (login != null){

                mLoginView.setLoggedIn();

                registerFirebase();

                configureMenus(login);
                loadDashboardFragment();
            }else{
                DuxWsResponse d = new DuxWsResponse();
                d.setOP_OK(false);
                d.setTexto("Datos incorrectos.");

                showLoginResponse(d);
            }
        }

        @Override public void onComplete() {
            hideViewLoading();

            DuxWsResponse d = new DuxWsResponse();
            d.setOP_OK(false);
            d.setTexto("Datos incorrectos.");

            showLoginResponse(d);
        }

    }

    private final class GetLoginInfoObserver extends DefaultMaybeObserver<Login> {

        @Override public void onError(Throwable e) {
            Log.d(TAG, "GetLoginInfoObserver onERROR: " + e.toString());
            hideViewLoading();
            showViewRetry();
        }

        @Override public void onSuccess(Login login) {
            hideViewLoading();
            //Log.d(TAG, "pendingOrder id: " + pendingOrder.getId());

            showLoginInfo(login);
            doLogin(login);
        }

        @Override public void onComplete() {
            hideViewLoading();
            //Log.d(TAG, "pendingOrder id: " + pendingOrder.getId());

            showLoginInfo(new Login());
        }

    }

    private final class SetPushTokenObserver extends DefaultSingleObserver<DuxWsResponse> {

        @Override public void onError(Throwable e) {
            Log.d(TAG, "SetPushTokenObserver onERROR: " + e.toString());
        }

        @Override public void onSuccess(DuxWsResponse duxWsResponse) {

        }

    }


    /*
    private final class UpdatePlayerCharacterObserver extends DefaultMaybeObserver<PlayerCharacter> {

        @Override
        public void onSuccess(PlayerCharacter playerCharacter) {
            Log.d(TAG, "onNext() called with: playerCharacter = [" + playerCharacter + "]");
            hideViewLoading();

            PlayerCharacter uiPC = new PlayerCharacter(playerCharacter);

            showPlayerCharacter(uiPC);

            initializing = false;
        }

        @Override
        public void onComplete() {
            hideViewLoading();
        }

        @Override
        public void onError(Throwable exception) {
            Log.d(TAG, "onError() called with: exception = [" + exception + "]");
            hideViewLoading();
            showViewRetry();
        }

    }
    */

    private void showViewLoading() {
        mLoginView.showLoading();
    }

    private void hideViewLoading() {
        mLoginView.hideLoading();
    }

    private void showViewRetry() {
        mLoginView.showRetry();
    }

    private void hideViewRetry() {
        mLoginView.hideRetry();
    }
}
