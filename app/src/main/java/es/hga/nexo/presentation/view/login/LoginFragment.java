package es.hga.nexo.presentation.view.login;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.hga.nexo.MyApp;
import es.hga.nexo.R;
import es.hga.nexo.dagger.components.ApplicationComponent;
import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.Login;
import es.hga.nexo.domain.business.PendingOrder;
import es.hga.nexo.presentation.base.BaseFragment;
import es.hga.nexo.presentation.view.activity.MainActivity;
import es.hga.nexo.presentation.view.dashboard.DashboardFragment;
import es.hga.nexo.presentation.view.pendingorderdetails.PendingOrderDetailsFragment;
import es.hga.nexo.presentation.view.pendingorders.PendingOrdersFragment;


public class LoginFragment extends BaseFragment implements LoginView {

    private static final String TAG = "LoginFragment";
    private static final String mFragmentTitle = "Login";

    @BindView(R.id.et_login_username)
    EditText mUsername;
    @BindView(R.id.et_login_password)
    EditText mPassword;
    @BindView(R.id.btn_login_login)
    Button mBtnLogin;
    @BindView(R.id.pb_login_login)
    ProgressBar mPbLoading;

    @Inject
    LoginPresenter mPresenter;

    Login mLogin;

    public LoginFragment() {
    }

    public static LoginFragment newInstance() {
        Bundle args = new Bundle();

        LoginFragment fragment = new LoginFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectComponent(MyApp.get(getActivity()).getApplicationComponent());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view != null) {
            ButterKnife.bind(this, view);
            Log.i(TAG, "onCreateView");
            //mAbilityScoresRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
            //mStatScoresRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        }
        return view;
    }

    @Override
    public String getFragmentTitle() {
        return mFragmentTitle;
    }

    @Override
    public void setLoggedIn() {
        ((MainActivity)getActivity()).openSession();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_login;
    }

    @Override
    protected void injectComponent(ApplicationComponent component) {
        component.inject(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((MainActivity)getActivity()).setDrawerEnabled(false);
        ((MainActivity)getActivity()).configFab(false,0);

        mPresenter.setView(this);
        if (savedInstanceState == null) {
//            Log.i(TAG, "onViewCreated: no saveinstanceState");
            mPresenter.initialize();
        }



    }

    @Override
    public void renderLogin(@NonNull Login login) {
        Log.i(TAG, "render login");

        setActionBarTitle();

        mUsername.setText(login.getUsername());
        mPassword.setText(login.getPassword());

        mBtnLogin.setOnClickListener(v -> {
            Log.i(TAG,"btn login click");

            if (mUsername.getText().length() > 0 && mPassword.getText().length() > 0){
                mLogin = new Login();
                mLogin.setUsername(mUsername.getText() + "");
                mLogin.setPassword(mPassword.getText() + "");
                mPresenter.doLogin(mLogin);
            }

        });
    }


    @Override
    public void showLoginResponse(@NonNull DuxWsResponse duxWsResponse) {
        Log.i(TAG, "showLoginResponse");

        ((MainActivity)getActivity()).closeSession();

        Snackbar.make(getView(), duxWsResponse.getTexto(), Snackbar.LENGTH_LONG)
                .setAction("Acción", null).show();
    }

    @Override
    public void configureMenus(Login login){
        ((MainActivity)getActivity()).configureMenus(login);
    }


    @Override
    public void loadDashboardFragment() {
        //BaseFragment fragment = DashboardFragment.newInstance();

        BaseFragment fragment = PendingOrdersFragment.newInstance();
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.mainContainer, fragment)
                .addToBackStack(null)
                .commit();

        ((MainActivity)getActivity()).getSupportActionBar().setTitle(fragment.getFragmentTitle());
    }


    /*
    private void setStatAdapter() {
        if (mStatAdapter == null) {
            mStatAdapter = new StatAdapter(mPlayerCharacter.getStats());
            mStatScoresRecycler.setAdapter(mStatAdapter);
            mStatAdapter.getPositionClicks()
                    .subscribe(this::showStatAmountDialog, Throwable::printStackTrace);
        } else {
            mStatAdapter.setStats(mPlayerCharacter.getStats());
        }
    }

    private void setAbilityAdapter() {
        if (mAbilityAdapter == null) {
            mAbilityAdapter = new AbilityAdapter(mPlayerCharacter.getAbilities());
            mAbilityScoresRecycler.setAdapter(mAbilityAdapter);
            mAbilityAdapter.getPositionClicks()
                    .subscribe(this::showChangeAbilityDialog, Throwable::printStackTrace);
        } else {
            mAbilityAdapter.setAbilitiesScores(mPlayerCharacter.getAbilities());
        }
    }

    */


    //private void setNamePlayerCharacter(){
    //    mNameTextView.setText(mPlayerCharacter.getName());
    //}

    /*
    @OnClick(R.id.dashboard_text)
    public void clickDashboardText(){
        new MaterialDialog.Builder(getContext())
                .title(R.string.pc_name)
                .cancelable(true)
                .autoDismiss(false)
                .input(getString(R.string.pc_name), mPlayerCharacter.getName(), false, (dialog, input) -> {
                    if (input.length() > 0) {
                        mPlayerCharacter.setName(input.toString());
                        mPresenter.reloadPlayerCharacter(mPlayerCharacter);
                        dialog.dismiss();
                    }
                }).show();
    }
    */

    //@OnClick(R.id.fab)
    //public void clickFab(){

    //  showNewStatDialog()
    //};




    @Override
    public void showLoading() {

        mPbLoading.setVisibility(View.VISIBLE);
        mBtnLogin.setVisibility(View.GONE);

        mUsername.setEnabled(false);
        mPassword.setEnabled(false);

    }

    @Override
    public void hideLoading() {

        mPbLoading.setVisibility(View.GONE);
        mBtnLogin.setVisibility(View.VISIBLE);

        mUsername.setEnabled(true);
        mPassword.setEnabled(true);

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public Context context() {
        return getContext();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume() called");
        mPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause() called");
        mPresenter.pause();
    }

    @Override
    public void onDestroy() {
        mPresenter.destroy();
        super.onDestroy();
        Log.d(TAG, "onDestroy() called");
    }
}
