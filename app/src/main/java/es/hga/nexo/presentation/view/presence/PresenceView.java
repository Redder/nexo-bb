package es.hga.nexo.presentation.view.presence;


import android.location.Location;

import es.hga.nexo.domain.business.Dashboard;
import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.Presence;
import es.hga.nexo.presentation.view.LoadDataView;

public interface PresenceView extends LoadDataView {

    void renderPresence();
    void toggleIconGPS(boolean enable);
    void updatePosition(Location location);
    void showSavePresence(Presence presence);
    void showErrorSavePresence(String error);

}
