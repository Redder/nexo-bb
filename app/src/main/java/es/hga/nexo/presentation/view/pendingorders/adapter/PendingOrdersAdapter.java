package es.hga.nexo.presentation.view.pendingorders.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.hga.nexo.R;
import es.hga.nexo.domain.business.PendingOrder;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by alex on 10/05/2017.
 */

public class PendingOrdersAdapter extends RecyclerView.Adapter<PendingOrdersAdapter.ViewHolder>{

    private static final String TAG = "PendingOrdersAdapter";

    private List<PendingOrder> mPendingOrderList;

    final PublishSubject<Integer> detailsClick = PublishSubject.create();
    final PublishSubject<Integer> acceptClick = PublishSubject.create();
    final PublishSubject<Integer> rejectClick = PublishSubject.create();
    final PublishSubject<PendingOrder> removeStatusClick = PublishSubject.create();
    final PublishSubject<PendingOrder> updateStatusClick = PublishSubject.create();

    public PendingOrdersAdapter(List<PendingOrder> pendingOrderList) {
        mPendingOrderList = pendingOrderList;
        Log.i(TAG, "constructor");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pendingorders_rv_item, null);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pendingorders_rv_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PendingOrder pendingOrder = mPendingOrderList.get(position);
        //holder.poProveedor.setText(pendingOrder.getNumero() + "");
        holder.poProveedor.setText(pendingOrder.getProveedor());
        holder.poDocumento.setText(pendingOrder.getDocumento());
        holder.poImporteTotal.setText(pendingOrder.getTotalCosteUnidadPrincipal() + "");
        holder.poNumLineas.setText(pendingOrder.getLineas().size() + "");
        holder.poCentrosCoste.setText(pendingOrder.getCentrosCosteString());
        holder.poCreador.setText(pendingOrder.getCreador());
        holder.poFecha.setText(pendingOrder.getFecha());

        holder.mBtnAcceptPendingOrder.setOnClickListener(v -> {
            //Log.i(TAG,"btn accept click");
            //mPresenter.acceptPendingOrder(mIdPendingOrder);
            acceptClick.onNext(pendingOrder.getId());
        });

        holder.mBtnRejectPendingOrder.setOnClickListener(v -> {
            //Log.i(TAG,"btn accept click");
            rejectClick.onNext(pendingOrder.getId());
        });

        holder.pendingOrderContainer.setOnClickListener(v -> {
            detailsClick.onNext(pendingOrder.getId());
        });

        /*
        holder.poCardView.setOnClickListener(v -> {
            detailsClick.onNext(pendingOrder.getId());
        });
        */



    }

    /*
    private void removeAt(int position) {
        mStatusList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mStatusList.size());
    }
    */

    @Override
    public int getItemCount() {
        return mPendingOrderList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tv_pendingorders_rv_item_proveedor)
        protected TextView poProveedor;
        @BindView(R.id.tv_pendingorders_rv_item_documento)
        protected TextView poDocumento;
        @BindView(R.id.tv_pendingorders_rv_item_importetotal)
        protected TextView poImporteTotal;
        @BindView(R.id.tv_pendingorders_rv_item_numlineas)
        protected TextView poNumLineas;
        @BindView(R.id.tv_pendingorders_rv_item_centroscoste)
        protected TextView poCentrosCoste;
        @BindView(R.id.btn_pendingorders_rv_item_accept)
        protected Button mBtnAcceptPendingOrder;
        @BindView(R.id.btn_pendingorders_rv_item_reject)
        protected Button mBtnRejectPendingOrder;
        @BindView(R.id.tv_pendingorders_rv_item_creador)
        protected TextView poCreador;
        @BindView(R.id.tv_pendingorders_rv_item_fecha)
        protected TextView poFecha;


        @BindView(R.id.ll_pendingorders_rv_item_container)
        protected View pendingOrderContainer;
        @BindView(R.id.cv_pendingorders_rv_item_cardview)
        protected CardView poCardView;

        ViewHolder(View view){
            super(view);

            ButterKnife.bind(this, view);

        }

    }

    public Observable<Integer> getDetailsClick(){
        return detailsClick;
    }

    public Observable<Integer> getAcceptClick(){
        return acceptClick;
    }

    public Observable<Integer> getRejectClick(){
        return rejectClick;
    }

    public Observable<PendingOrder> getRemoveStatusClick(){
        return removeStatusClick;
    }

    public Observable<PendingOrder> getUpdateStatusClick(){
        return updateStatusClick;
    }


    public void setPendingOrdersList(List<PendingOrder> pendingOrdersList) {
        //mPendingOrderList = pendingOrdersList;
        Log.d(TAG,"setPendingOrdersList");
        mPendingOrderList.clear();
        mPendingOrderList.addAll(pendingOrdersList);
        notifyDataSetChanged();
    }
}

