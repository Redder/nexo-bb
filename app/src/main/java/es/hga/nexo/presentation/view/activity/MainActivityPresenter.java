package es.hga.nexo.presentation.view.activity;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

import javax.inject.Inject;

import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.interactor.base.DefaultCompletableObserver;
import es.hga.nexo.domain.interactor.base.DefaultSingleObserver;
import es.hga.nexo.domain.interactor.login.CloseSessionUseCase;
import es.hga.nexo.domain.interactor.settings.CheckTokenWsUseCase;
import es.hga.nexo.presentation.base.Presenter;

class MainActivityPresenter implements Presenter<MainActivityView> {

    private static final String TAG = "MainActivityPresenter";

    private MainActivityView mMainActivityView;

    private CheckTokenWsUseCase mCheckTokenWsUseCase;
    private CloseSessionUseCase mCloseSessionUseCase;

    private boolean initializing = false;


    @Inject
    MainActivityPresenter(CheckTokenWsUseCase checkTokenWsUseCase, CloseSessionUseCase closeSessionUseCase) {
        mCheckTokenWsUseCase = checkTokenWsUseCase;
        mCloseSessionUseCase = closeSessionUseCase;
    }


    @Override
    public void resume() {
        if (!initializing) {
            initialize();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        mMainActivityView = null;
        //mGetPlayerCharacterUseCase.dispose();
        //mUpdatePlayerCharacterUseCase.dispose();
    }

    @Override
    public void setView(MainActivityView view) {
        mMainActivityView = view;
    }

    @Override
    public void initialize() {
        Log.d(TAG, "initialize() called");
        initializing = true;
        //loadSettings();
    }

    public void closeSession(){

        mCloseSessionUseCase.execute(new CloseSessionObserver());

    }

    public void checkTokenWS(){

        mCheckTokenWsUseCase.execute(new CheckTokenWsObserver());

    }

    private void showErrorCheckTokenWS(){
        mMainActivityView.showErrorCheckTokenWS();
    }


    private final class CloseSessionObserver extends DefaultCompletableObserver {

        @Override public void onError(Throwable e) {
            Log.d(TAG, "CloseSessionObserver onERROR: " + e.toString());
        }

        @Override public void onComplete() {

        }

    }




    private final class CheckTokenWsObserver extends DefaultSingleObserver<DuxWsResponse> {

        @Override
        public void onError(Throwable e) {
            Log.d(TAG, "CheckTokenWsObserver onERROR: " + e.toString());

            showErrorCheckTokenWS();

            initializing = false;
        }

        @Override
        public void onSuccess(DuxWsResponse duxWsResponse) {
            Log.d(TAG, "CheckTokenWsObserver onSuccess: ");

            if (!duxWsResponse.isOP_OK()){
                showErrorCheckTokenWS();
            }

            initializing = false;
        }

    }

}


