package es.hga.nexo.presentation.view.presence;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.hga.nexo.MyApp;
import es.hga.nexo.R;
import es.hga.nexo.dagger.components.ApplicationComponent;
import es.hga.nexo.domain.business.Dashboard;
import es.hga.nexo.domain.business.Presence;
import es.hga.nexo.presentation.base.BaseFragment;
import es.hga.nexo.presentation.view.activity.MainActivity;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class PresenceFragment extends BaseFragment implements PresenceView, OnMapReadyCallback {

    private static final String TAG = "PresenceFragment";
    private static final String mFragmentTitle = "Presencia";

    @BindView(R.id.rl_presence_form)
    RelativeLayout mRlForm;

    @BindView(R.id.et_presence_card)
    EditText mEtCard;

    @BindView(R.id.btn_presence_scan)
    Button mBtnScan;

    @BindView(R.id.btn_presence_save)
    Button mBtnSave;

    @BindView(R.id.iv_presence_gps_status_icon)
    ImageView mGpsStatusIcon;

    @BindView(R.id.rl_presence_data)
    RelativeLayout mRlData;

    @BindView(R.id.tv_presence_name)
    TextView mTvName;

    @BindView(R.id.tv_presence_action)
    TextView mTvAction;

    @BindView(R.id.tv_presence_entry)
    TextView mTvEntry;

    @BindView(R.id.tv_presence_text)
    TextView mTvText;

    @BindView(R.id.iv_presence_action_icon)
    ImageView mActionIcon;

    @BindView(R.id.map)
    MapView mMapView;

    @Inject
    PresencePresenter mPresenter;

    private boolean resumeFromBarcode = false;

    //Dashboard mDashboard;

    private GoogleMap mGoogleMap;


    public PresenceFragment() {

    }

    public static PresenceFragment newInstance() {
        Bundle args = new Bundle();

        PresenceFragment fragment = new PresenceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectComponent(MyApp.get(getActivity()).getApplicationComponent());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        ((MainActivity)getActivity()).setDrawerEnabled(true);
        ((MainActivity)getActivity()).configFab(false,0);

        if (view != null) {
            ButterKnife.bind(this, view);
            Log.i(TAG, "onCreateView");
            ((MainActivity)getActivity()).configFab(false,0);
            //mAbilityScoresRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
            //mStatScoresRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        }

        mBtnScan.setOnClickListener(v -> {
            //Log.i(TAG,"btn accept click");

            mTvName.setText("");
            mTvAction.setText("");
            mTvEntry.setText("");
            mTvText.setText("");
            mRlData.setVisibility(View.GONE);

            scanFromFragment();
        });

        mBtnSave.setOnClickListener(v -> {
            savePresence(mEtCard.getText() + "");
        });

        /////////////////////////////////

        mGpsStatusIcon.setBackgroundResource(R.drawable.gps_off);
        //mActionIcon.setBackgroundResource(R.drawable.pause);
        mRlData.setVisibility(View.GONE);

        //////////////////////////////

        return view;
    }

    public void toggleIconGPS(boolean enabled){

        if (enabled){
            mGpsStatusIcon.setBackgroundResource(R.drawable.gps_on);
        }else{
            mGpsStatusIcon.setBackgroundResource(R.drawable.gps_off);
        }

    }


    public void updatePosition(Location location){

        if (location.getAccuracy() < 10){
            mGpsStatusIcon.setBackgroundResource(R.drawable.gps_fixed);
        }else{
            mGpsStatusIcon.setBackgroundResource(R.drawable.gps_on);
        }

        if (mGoogleMap != null){

            mGoogleMap.clear();

            mGoogleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(location.getLatitude(), location.getLongitude()))
                    .title("Tu posición").snippet("Esta es tu posición estimada.")
            );

            CameraPosition myPos = CameraPosition.builder()
                    .target(new LatLng(location.getLatitude(), location.getLongitude()))
                    .zoom(16).bearing(0).tilt(0).build();

            mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(myPos));

            mGoogleMap.getUiSettings().setAllGesturesEnabled(false);
            mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
            mGoogleMap.getUiSettings().setZoomControlsEnabled(false);

        }else{
            Log.d(TAG, "updatePosition: mGoogleMap is NULL");
        }

    }


    public void scanFromFragment() {
        IntentIntegrator.forSupportFragment(this).initiateScan();

        /*
                IntentIntegrator integrator = new IntentIntegrator(this);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
                integrator.setPrompt("Scan a barcode");
                integrator.setCameraId(0);  // Use a specific camera of the device
                integrator.setBeepEnabled(false);
                integrator.setBarcodeImageEnabled(true);
                integrator.initiateScan();
                 */
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Toast.makeText(getActivity(), "Lectura cancelada", Toast.LENGTH_LONG).show();
            } else {
                mEtCard.setText(result.getContents());
                resumeFromBarcode = true;

                //Snackbar.make(getView(), result.getContents(), Snackbar.LENGTH_LONG)
                //        //.setAction("Reintentar", null)
                //        .show();


                //mBtnSave.performClick();
                //mPresenter.savePresence(result.getContents() + "");

            }

        }
    }


    @Override
    public String getFragmentTitle() {
        return mFragmentTitle;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_presence;
    }

    @Override
    protected void injectComponent(ApplicationComponent component) {
        component.inject(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mPresenter.setView(this);
        if (savedInstanceState == null) {
//            Log.i(TAG, "onViewCreated: no saveinstanceState");
            mPresenter.initialize();
        }

        if (mMapView != null){
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        MapsInitializer.initialize(getContext());

        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

    }

    private void savePresence(String card) {
        mRlData.setVisibility(View.GONE);
        mPresenter.savePresence(card);
    }

    @Override
    public void showErrorSavePresence(@NonNull String text) {
        Log.i(TAG, "showErrorSavePresence");

        mTvName.setText(text);
        mTvAction.setText("");
        mTvEntry.setText("");
        mTvText.setText("");

        mActionIcon.setBackgroundResource(R.drawable.error_red);

        mRlData.setVisibility(View.VISIBLE);


        //Snackbar.make(getView(), text, Snackbar.LENGTH_LONG)
        //        //.setAction("Reintentar", null)
        //        .show();
    }

    @Override
    public void showSavePresence(@NonNull Presence presence) {
        Log.i(TAG, "showSavePresence");

        if (presence.getTodoOk()){

            mTvName.setText(presence.getNombre() + "");
            mTvAction.setText(presence.getAccion() + "");
            if (presence.getEntrada() != null){
                mTvEntry.setText(presence.getEntrada() + "");
            }else{
                mTvEntry.setText("");
            }
            if (presence.getTexto() != null){
                mTvText.setText(presence.getTexto() + "");
            }else{
                mTvText.setText("");
            }

            switch (presence.getAccion()){

                case "Entrada":
                    mActionIcon.setBackgroundResource(R.drawable.arrow_right_green);
                    break;

                case "Salida":
                    mActionIcon.setBackgroundResource(R.drawable.arrow_left_red);
                    break;

                case "Ya está dentro.":
                    mActionIcon.setBackgroundResource(R.drawable.pause_blue);
                    break;

                case "Ya está fuera.":
                    mActionIcon.setBackgroundResource(R.drawable.pause_blue);
                    break;

            }//swicth

        }else{

            mTvName.setText( presence.getTexto() + "");
            mTvAction.setText("");
            mTvEntry.setText("");
            mTvText.setText("");

            mActionIcon.setBackgroundResource(R.drawable.warning_yellow);

        }

        //mEtCard.setText("");
        mRlData.setVisibility(View.VISIBLE);

        //Snackbar.make(getView(), presence.getNombre(), Snackbar.LENGTH_LONG)
        //        //.setAction("Acción", null)
        //        .show();

    }

    @Override
    public void renderPresence() {
        //mDashboard = dashboard;
        setActionBarTitle();
        //setNamePlayerCharacter();
        //setAbilityAdapter();
        //setStatAdapter();
    }

   


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public Context context() {
        return getContext();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume() called");
        grantPermissions();
        mPresenter.resume();
        if (resumeFromBarcode){
            resumeFromBarcode = false;

            savePresence(mEtCard.getText() + "");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause() called");

        mPresenter.pause();
    }

    @Override
    public void onDestroy() {
        mPresenter.destroy();
        super.onDestroy();
        Log.d(TAG, "onDestroy() called");
    }

    private void grantPermissions(){

        //  PEDIR PERMISOS, NECESARIO EN API > 23
        if (ActivityCompat.checkSelfPermission(getContext(),
                android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(),
                        android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(),
                        android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{
                            android.Manifest.permission.CAMERA,
                            android.Manifest.permission.ACCESS_COARSE_LOCATION,
                            android.Manifest.permission.ACCESS_FINE_LOCATION
                    },1); //ESTE 1 NO SE PARA QUE ES
            //mLocationManager.requestLocationUpdates(provider, 400, 1, mLocationListenerGPS);
        } else {
            Log.e(TAG, "PERMISSIONS GRANTED");
            //mLocationManager.requestLocationUpdates(provider, 400, 1, mLocationListenerGPS);
        }
        /*
        //  PEDIR PERMISOS, NECESARIO EN API > 23
        if (ActivityCompat.checkSelfPermission(getContext(),
                android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{
                            android.Manifest.permission.CAMERA
                    },1); //ESTE 1 NO SE PARA QUE ES
        } else {
            Log.e(TAG, "PERMISSIONS GRANTED");
        }
        */
    }


}
