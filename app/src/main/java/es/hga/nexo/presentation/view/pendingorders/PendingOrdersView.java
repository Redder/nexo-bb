package es.hga.nexo.presentation.view.pendingorders;

//import es.hga.nexo.domain.business.PlayerCharacter;
import java.util.List;

import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.PendingOrder;
import es.hga.nexo.presentation.view.LoadDataView;

public interface PendingOrdersView extends LoadDataView {

    void renderPendingOrders(List<PendingOrder> pendingOrdersList);
    void showPendingOrderAccepted(DuxWsResponse duxWsResponse);
    void showPendingOrderRejected(DuxWsResponse duxWsResponse);
    void showErrorPendingOrderAccepted(String text);
    void showErrorPendingOrderRejected(String text);
    void showErrorGetPendingOrders(String text);

//    void showChangeAbilityDialog(Ability ability);

//    void showChangeStatDialog(Stat stat);

}
