package es.hga.nexo.presentation.view.settings;

import android.util.Log;

import javax.inject.Inject;

import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.Login;
import es.hga.nexo.domain.business.Settings;
import es.hga.nexo.domain.interactor.base.DefaultCompletableObserver;
import es.hga.nexo.domain.interactor.base.DefaultMaybeObserver;
import es.hga.nexo.domain.interactor.base.DefaultSingleObserver;
import es.hga.nexo.domain.interactor.login.DoLoginUseCase;
import es.hga.nexo.domain.interactor.settings.LoadSettingsUseCase;
import es.hga.nexo.domain.interactor.settings.SaveSettingsUseCase;
import es.hga.nexo.presentation.base.Presenter;
import es.hga.nexo.presentation.view.login.LoginView;

//import es.hga.nexo.domain.business.PlayerCharacter;
//import es.hga.nexo.domain.interactor.base.DefaultMaybeObserver;
//import es.hga.nexo.domain.interactor.base.DefaultObserver;
//import es.hga.nexo.domain.interactor.playercharacter.GetPlayerCharacterUseCase;
//import es.hga.nexo.domain.interactor.playercharacter.UpdatePlayerCharacterUseCase;

class SettingsPresenter implements Presenter<SettingsView> {

    private static final String TAG = "LoginPresenter";

    private SettingsView mSettingsView;

    private SaveSettingsUseCase mSaveSettingsUseCase;
    private LoadSettingsUseCase mLoadSettingsUseCase;
    //private UpdatePlayerCharacterUseCase mUpdatePlayerCharacterUseCase;

    private boolean initializing = false;


    @Inject
    SettingsPresenter(SaveSettingsUseCase saveSettingsUseCase,
                      LoadSettingsUseCase loadSettingsUseCase) {
        mSaveSettingsUseCase = saveSettingsUseCase;
        mLoadSettingsUseCase = loadSettingsUseCase;
    }


    @Override
    public void resume() {
        if(!initializing){
            initialize();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        mSettingsView = null;
        //mGetPlayerCharacterUseCase.dispose();
        //mUpdatePlayerCharacterUseCase.dispose();
    }

    @Override
    public void setView(SettingsView view) {
        mSettingsView = view;
    }

    @Override
    public void initialize() {
        Log.d(TAG, "initialize() called");
        initializing = true;
        loadSettings();
    }

    private void loadSettings(){
        hideViewRetry();
        showViewLoading();
        getSettingsInfo();
    }

    private void renderPendingOrders(Settings settings) {
        mSettingsView.renderSettings(settings);
    }

    private void showSavedSettings() {
        mSettingsView.showSavedSettings();
    }

    private void getSettingsInfo() {
        mLoadSettingsUseCase.execute(new LoadSettingsObserver());
    }

    public void saveSettings(Settings settings){
        mSaveSettingsUseCase.execute(new SaveSettingsObserver(), settings);
    }


    private final class SaveSettingsObserver extends DefaultCompletableObserver {

        @Override public void onError(Throwable e) {
            Log.d(TAG, "RejectPendingOrderObserver onERROR: " + e.toString());
            hideViewLoading();
            showViewRetry();
            initializing = false;
        }

        @Override public void onComplete() {
            hideViewLoading();
            //Log.d(TAG, "pendingOrder id: " + pendingOrder.getId());

            showSavedSettings();

            initializing = false;
        }

    }

    private final class LoadSettingsObserver extends DefaultMaybeObserver<Settings> {

        @Override public void onError(Throwable e) {
            Log.d(TAG, "RejectPendingOrderObserver onERROR: " + e.toString());
            hideViewLoading();
            showViewRetry();
            initializing = false;
        }

        @Override public void onSuccess(Settings settings){
            hideViewLoading();
            //Log.d(TAG, "pendingOrder id: " + pendingOrder.getId());

            renderPendingOrders(settings);

            initializing = false;
        }

        @Override public void onComplete() {
            hideViewLoading();
            //Log.d(TAG, "pendingOrder id: " + pendingOrder.getId());

            renderPendingOrders(new Settings());

            initializing = false;
        }

    }


    /*
    private final class UpdatePlayerCharacterObserver extends DefaultMaybeObserver<PlayerCharacter> {

        @Override
        public void onSuccess(PlayerCharacter playerCharacter) {
            Log.d(TAG, "onNext() called with: playerCharacter = [" + playerCharacter + "]");
            hideViewLoading();

            PlayerCharacter uiPC = new PlayerCharacter(playerCharacter);

            showPlayerCharacter(uiPC);

            initializing = false;
        }

        @Override
        public void onComplete() {
            hideViewLoading();
        }

        @Override
        public void onError(Throwable exception) {
            Log.d(TAG, "onError() called with: exception = [" + exception + "]");
            hideViewLoading();
            showViewRetry();
        }

    }
    */

    private void showViewLoading() {
        mSettingsView.showLoading();
    }

    private void hideViewLoading() {
        mSettingsView.hideLoading();
    }

    private void showViewRetry() {
        mSettingsView.showRetry();
    }

    private void hideViewRetry() {
        mSettingsView.hideRetry();
    }
}
