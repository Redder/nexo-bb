package es.hga.nexo.presentation.base;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import es.hga.nexo.R;

public abstract class BaseActivity extends AppCompatActivity {

    private LinearLayout ll_loading;
    private FrameLayout mainContainer;

    protected void addFragment(int containerViewId, Fragment fragment) {
        final FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(containerViewId, fragment);
        fragmentTransaction.commit();
    }

    public void showLoading() {
        ll_loading = (LinearLayout)this.findViewById(R.id.ll_loading);
        mainContainer = (FrameLayout)this.findViewById(R.id.mainContainer);

        ll_loading.setVisibility(LinearLayout.VISIBLE);
        mainContainer.setVisibility(LinearLayout.GONE);

    }

    public void hideLoading() {

        ll_loading = (LinearLayout)this.findViewById(R.id.ll_loading);
        mainContainer = (FrameLayout)this.findViewById(R.id.mainContainer);

        ll_loading.setVisibility(LinearLayout.GONE);
        mainContainer.setVisibility(LinearLayout.VISIBLE);

    }

}
