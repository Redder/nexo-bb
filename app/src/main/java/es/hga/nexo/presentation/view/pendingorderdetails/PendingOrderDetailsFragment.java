package es.hga.nexo.presentation.view.pendingorderdetails;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.hga.nexo.MyApp;
import es.hga.nexo.R;
import es.hga.nexo.dagger.components.ApplicationComponent;
import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.PendingOrder;
import es.hga.nexo.presentation.base.BaseFragment;
import es.hga.nexo.presentation.view.activity.MainActivity;
import es.hga.nexo.presentation.view.pendingorderdetails.adapter.PendingOrderDetailsAdapter;
import es.hga.nexo.presentation.view.pendingorders.PendingOrdersFragment;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;


public class PendingOrderDetailsFragment extends BaseFragment implements PendingOrderDetailsView {

    private static final String TAG = "PendOrderDetaFragm";
    public static final String ID_PENDINGORDER = "ID_PENDINGORDER";
    private static final String mFragmentTitle = "Detalles del pedido";

    @Inject
    PendingOrderDetailsPresenter mPresenter;


    @BindView(R.id.rv_pendingorderlines_list)
    RecyclerView mPendingOrderLinesRV;

    PendingOrderDetailsAdapter mPendingOrderDetailsAdapter;


    @BindView(R.id.tv_pendingorderdetails_proveedor)
    TextView mPendingorderdetails_proveedor;

    @BindView(R.id.tv_pendingorderdetails_documento)
    TextView mPendingorderdetails_documento;

    @BindView(R.id.tv_pendingorderdetails_importe)
    TextView mPendingorderdetails_importe;

    @BindView(R.id.btn_pendingorderdetails_accept)
    Button mBtnAcceptPendingOrder;

    @BindView(R.id.btn_pendingorderdetails_reject)
    Button mBtnRejectPendingOrder;

    @BindView(R.id.ll_pendingorderdetails_notfound)
    LinearLayout llNotFound;

    @BindView(R.id.rl_pendingorderdetails_details)
    RelativeLayout rlDetails;


    int mIdPendingOrder;

    public PendingOrderDetailsFragment() {
    }

    public static PendingOrderDetailsFragment newInstance(int idPendingOrder) {
        Bundle args = new Bundle();
        args.putInt(ID_PENDINGORDER,idPendingOrder);

        PendingOrderDetailsFragment fragment = new PendingOrderDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectComponent(MyApp.get(getActivity()).getApplicationComponent());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mIdPendingOrder = getArguments().getInt(ID_PENDINGORDER);
        ((MainActivity)getActivity()).setDrawerEnabled(true);
        ((MainActivity)getActivity()).configFab(false,0);
        if (view != null) {
            ButterKnife.bind(this, view);
            Log.i(TAG, "onCreateView");

            mPendingOrderLinesRV.setLayoutManager(new LinearLayoutManager(getContext()));
            mPendingOrderLinesRV.setNestedScrollingEnabled(false);

        }
        return view;
    }

    @Override
    public String getFragmentTitle() {
        return mFragmentTitle;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragemnt_pendingorderdetails;
    }

    @Override
    protected void injectComponent(ApplicationComponent component) {
        component.inject(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mPresenter.setView(this);
        if (savedInstanceState == null) {
//            Log.i(TAG, "onViewCreated: no saveinstanceState");
            mPresenter.initialize();
            mPresenter.loadPendingOrder(mIdPendingOrder);

        }
    }

    @Override
    public void renderPendingOrderDetails(@NonNull PendingOrder pendingOrder) {
        Log.i(TAG, "renderPendingOrderDetails");

        setActionBarTitle();

        llNotFound.setVisibility(View.GONE);
        rlDetails.setVisibility(View.VISIBLE);

        //mPendingorderdetails_proveedor.setText(Integer.toString(pendingOrder.getNumero()));
        mPendingorderdetails_proveedor.setText(pendingOrder.getProveedor());
        mPendingorderdetails_documento.setText(pendingOrder.getDocumento());
        mPendingorderdetails_importe.setText(pendingOrder.getTotalCosteUnidadPrincipal() + "");

        mBtnAcceptPendingOrder.setOnClickListener(v -> {
            //Log.i(TAG,"btn accept click");
            acceptPendingOrder(mIdPendingOrder);
        });

        mBtnRejectPendingOrder.setOnClickListener(v -> {
            //Log.i(TAG,"btn accept click");
            rejectPendingOrder(mIdPendingOrder);
        });

        Log.i(TAG,"pendingOrder.getLineas().size(): " + pendingOrder.getLineas().size());

        if (pendingOrder.getLineas().size() > 0){
            Log.i(TAG,"concepto: " + pendingOrder.getLineas().get(0).getConcepto());
        }

        setPendingOrderDetailsAdapter(pendingOrder.getLineas());


        //setNamePlayerCharacter();
        //setAbilityAdapter();
        //setStatAdapter();
    }

    @Override
    public void renderNotFoundPendingOrderDetails() {

        setActionBarTitle();

        llNotFound.setVisibility(View.VISIBLE);
        rlDetails.setVisibility(View.GONE);

    }

    private void acceptPendingOrder(int id) {

        new MaterialDialog.Builder(getContext())
                .title("Aceptar pedido")
                .content("¿Desea aceptar el pedido?")
                .positiveText("Ok")
                .negativeText("Cancelar")
                .cancelable(true)
                .autoDismiss(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        mPresenter.acceptPendingOrder(id);
                        dialog.dismiss();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();

        //mPresenter.acceptPendingOrder(id);
    }

    private void rejectPendingOrder(Integer id) {

        new MaterialDialog.Builder(getContext())
                .title("Rechazar pedido")
                .cancelable(true)
                .autoDismiss(false)
                .input("Motivo","", true, (dialog, input) -> {

                    mPresenter.rejectPendingOrder(id,input.toString());
                    dialog.dismiss();

                }).show();
    }

    @Override
    public void showPendingOrderAccepted(@NonNull DuxWsResponse duxWsResponse) {
        Log.i(TAG, "showPendingOrderAccepted");

        Snackbar.make(getView(), duxWsResponse.getTexto(), Snackbar.LENGTH_LONG)
                .setAction("Acción", null).show();
    }

    @Override
    public void showPendingOrderRejected(@NonNull DuxWsResponse duxWsResponse) {
        Log.i(TAG, "showPendingOrderRejected");

        Snackbar.make(getView(), duxWsResponse.getTexto(), Snackbar.LENGTH_LONG)
                .setAction("Acción", null).show();
    }

    private void setPendingOrderDetailsAdapter(List<PendingOrder.PendingOrderLine> pendingOrderLines) {
        if (mPendingOrderDetailsAdapter == null) {
            mPendingOrderDetailsAdapter = new PendingOrderDetailsAdapter(pendingOrderLines);
            mPendingOrderLinesRV.setAdapter(mPendingOrderDetailsAdapter);

            Log.i(TAG, "setPendingOrderDetailsAdapter: null");

        } else {
            Log.i(TAG, "setPendingOrderDetailsAdapter: not null");
            mPendingOrderDetailsAdapter.setPendingOrderLines(pendingOrderLines);
        }
    }

    @Override
    public void showErrorGetPendingOrderDetails(@NonNull String text) {
        Log.i(TAG, "showErrorGetPendingOrderDetails");

        llNotFound.setVisibility(View.VISIBLE);
        rlDetails.setVisibility(View.GONE);

        Snackbar.make(getView(), text, Snackbar.LENGTH_LONG)
                .setAction("Reintentar", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Log.i("Snackbar", "Pulsada acción snackbar!");
                        mPresenter.initialize();
                        mPresenter.loadPendingOrder(mIdPendingOrder);
                    }
                }).show();
    }

    @Override
    public void showErrorPendingOrderDetailsRejected(@NonNull String text) {
        Log.i(TAG, "showErrorPendingOrderDetailsRejected");

        Snackbar.make(getView(), text, Snackbar.LENGTH_LONG)
                .setAction("Reintentar", null)
                .show();
    }

    @Override
    public void showErrorPendingOrderDetailsAccepted(@NonNull String text) {
        Log.i(TAG, "showErrorPendingOrderDetailsAccepted");

        Snackbar.make(getView(), text, Snackbar.LENGTH_LONG)
                .setAction("Reintentar", null)
                .show();
    }

    public void loadPendingOrdersListFragment(){
        BaseFragment fragment = PendingOrdersFragment.newInstance();
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.mainContainer, fragment)
                //.addToBackStack(null)
                .commit();
    }


    @Override
    public void showLoading() {
        super.showLoading();
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public Context context() {
        return getContext();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume() called");
        mPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause() called");
        mPresenter.pause();
    }

    @Override
    public void onDestroy() {
        mPresenter.destroy();
        super.onDestroy();
        Log.d(TAG, "onDestroy() called");
    }

    @Override
    public void onBackPressedFragment() {
        Log.d(TAG,"onBackPressed");

        BaseFragment fragment = PendingOrdersFragment.newInstance();
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.mainContainer, fragment)
                //.addToBackStack(null)
                .commit();
    }
}
