package es.hga.nexo.presentation.view.dashboard;

//import es.hga.nexo.domain.business.PlayerCharacter;
import android.location.Location;

import es.hga.nexo.domain.business.Dashboard;
import es.hga.nexo.presentation.view.LoadDataView;

public interface DashboardView extends LoadDataView {

    void renderDashboard(Dashboard dashboard);
    void showLocation(Location location);

//    void showChangeAbilityDialog(Ability ability);

//    void showChangeStatDialog(Stat stat);

}
