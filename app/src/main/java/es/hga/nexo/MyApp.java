package es.hga.nexo;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import es.hga.nexo.dagger.components.ApplicationComponent;
import es.hga.nexo.dagger.components.DaggerApplicationComponent;
import es.hga.nexo.dagger.modules.ApplicationModule;

public class MyApp extends Application {
    private static final String TAG = "MyApp";

    private ApplicationComponent applicationComponent;
    private static MyApp instance;

    public static MyApp get(Context context) {
        return (MyApp) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.initializeInjector();
        Log.i(TAG, "onCreate");
        instance = this;
    }


    public void initializeInjector() {
        this.applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }


    public ApplicationComponent getApplicationComponent() {
        return this.applicationComponent;
    }

    public static MyApp getContext(){
        return instance;
    }
}