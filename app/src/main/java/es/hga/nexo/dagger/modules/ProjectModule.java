package es.hga.nexo.dagger.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import es.hga.nexo.data.cache.Location.memory.LocationMemoryCache;
import es.hga.nexo.data.cache.Location.memory.LocationMemoryCacheImpl;
import es.hga.nexo.data.cache.login.disk.LoginDiskCache;
import es.hga.nexo.data.cache.login.disk.LoginDiskCacheImpl;
import es.hga.nexo.data.cache.login.memory.LoginMemoryCache;
import es.hga.nexo.data.cache.login.memory.LoginMemoryCacheImpl;
import es.hga.nexo.data.cache.login.network.LoginNetworkCache;
import es.hga.nexo.data.cache.login.network.LoginNetworkCacheImpl;
import es.hga.nexo.data.cache.pendingorders.disk.PendingOrderDiskCache;
import es.hga.nexo.data.cache.pendingorders.disk.PendingOrderDiskCacheImpl;
import es.hga.nexo.data.cache.pendingorders.memory.PendingOrderMemoryCache;
import es.hga.nexo.data.cache.pendingorders.memory.PendingOrderMemoryCacheImpl;
import es.hga.nexo.data.cache.pendingorders.network.PendingOrderNetworkCache;
import es.hga.nexo.data.cache.pendingorders.network.PendingOrderNetworkCacheImpl;
import es.hga.nexo.data.cache.presence.network.PresenceNetworkCache;
import es.hga.nexo.data.cache.presence.network.PresenceNetworkCacheImpl;
import es.hga.nexo.data.cache.production.network.ProductionNetworkCache;
import es.hga.nexo.data.cache.production.network.ProductionNetworkCacheImpl;
import es.hga.nexo.data.cache.settings.disk.SettingsDiskCache;
import es.hga.nexo.data.cache.settings.disk.SettingsDiskCacheImpl;
import es.hga.nexo.data.cache.settings.memory.SettingsMemoryCache;
import es.hga.nexo.data.cache.settings.memory.SettingsMemoryCacheImpl;
import es.hga.nexo.data.cache.settings.network.SettingsNetworkCache;
import es.hga.nexo.data.cache.settings.network.SettingsNetworkCacheImpl;
import es.hga.nexo.data.repository.LocationDataRepository;
import es.hga.nexo.data.repository.LoginDataRepository;
import es.hga.nexo.data.repository.PendingOrderDataRepository;
import es.hga.nexo.data.repository.PresenceDataRepository;
import es.hga.nexo.data.repository.ProductionDataRepository;
import es.hga.nexo.data.repository.SettingsDataRepository;
import es.hga.nexo.domain.repository.LocationRepository;
import es.hga.nexo.domain.repository.LoginRepository;
import es.hga.nexo.domain.repository.PendingOrderRepository;
import es.hga.nexo.domain.repository.PresenceRepository;
import es.hga.nexo.domain.repository.ProductionRepository;
import es.hga.nexo.domain.repository.SettingsRepository;

@Module
public class ProjectModule {


    @Provides
    @Singleton
    PendingOrderRepository providePendingOrdersRepository(PendingOrderDataRepository repository) {
        return repository;
    }

    @Provides
    @Singleton
    LoginRepository provideLoginRepository(LoginDataRepository repository) {
        return repository;
    }

    @Provides
    @Singleton
    SettingsRepository provideSettingsRepository(SettingsDataRepository repository) {
        return repository;
    }

    @Provides
    @Singleton
    PresenceRepository providePresenceRepository(PresenceDataRepository repository) {
        return repository;
    }

    @Provides
    @Singleton
    LocationRepository provideLocationRepository(LocationDataRepository repository) {
        return repository;
    }

    @Provides
    @Singleton
    ProductionRepository provideProductionRepository(ProductionDataRepository repository) {
        return repository;
    }

    //


    @Provides
    @Singleton
    LoginMemoryCache provideLoginMemoryCache(LoginMemoryCacheImpl cache){
        return cache;
    }

    @Provides
    @Singleton
    LoginDiskCache provideLoginDiskCache(LoginDiskCacheImpl cache){
        return cache;
    }

    @Provides
    @Singleton
    LoginNetworkCache provideLoginNetworkCache(LoginNetworkCacheImpl cache){
        return cache;
    }

    //

    @Provides
    @Singleton
    PendingOrderMemoryCache providePendingOrderMemoryCache(PendingOrderMemoryCacheImpl cache){
        return cache;
    }

    @Provides
    @Singleton
    PendingOrderDiskCache providePendingOrderDiskCache(PendingOrderDiskCacheImpl cache){
        return cache;
    }

    @Provides
    @Singleton
    PendingOrderNetworkCache providePendingOrderNetworkCache(PendingOrderNetworkCacheImpl cache){
        return cache;
    }

    //

    @Provides
    @Singleton
    SettingsMemoryCache provideSettingsMemoryCache(SettingsMemoryCacheImpl cache){
        return cache;
    }

    @Provides
    @Singleton
    SettingsDiskCache provideSettingsDiskCache(SettingsDiskCacheImpl cache){
        return cache;
    }


    @Provides
    @Singleton
    SettingsNetworkCache provideSettingsNetworkCache(SettingsNetworkCacheImpl cache){
        return cache;
    }

    //

    @Provides
    @Singleton
    PresenceNetworkCache providePresenceNetworkCache(PresenceNetworkCacheImpl cache){
        return cache;
    }

    //

    @Provides
    @Singleton
    LocationMemoryCache provideLocationMemoryCache(LocationMemoryCacheImpl cache){
        return cache;
    }

    //


    @Provides
    @Singleton
    ProductionNetworkCache provideProductionNetworkCache(ProductionNetworkCacheImpl cache){
        return cache;
    }

    /*
    @Provides
    @Singleton
    public APIServiceRetrofit provideAPIServiceRetrofit(SettingsRepository settingsRepository){
        return ApiServiceRetrofitImp.createApiRestService(settingsRepository.getSettings().getUrlServer(), settingsRepository.getSettings().getToken());
    }
    */

    /*
    @Provides
    @Singleton
    StatusCache provideStatusCache(PrefserDiskCache diskCache){
        return new StatusCacheImpl(diskCache);
    }

    @Provides
    @Singleton
    StatusRepository provideStatusRepository(StatusDataRepository repository){
        return repository;
    }

    @Provides
    @Singleton
    PlayerCharacterRepository providePlayerCharacterRepository(PlayerCharacterDataRepository repository){
        return repository;
    }

    @Provides
    @Singleton
    PlayerCharacterCache providePlayerCharacterCache(PlayerCharacterCacheImpl cache){
        return cache;
    }
    */

}
