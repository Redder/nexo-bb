package es.hga.nexo.dagger.components;

import javax.inject.Singleton;

import dagger.Component;
import es.hga.nexo.dagger.modules.ApplicationModule;
import es.hga.nexo.dagger.modules.ProjectModule;
import es.hga.nexo.data.cache.login.network.LoginNetworkCacheImpl;
import es.hga.nexo.data.helper.SettingsDataHelper;
import es.hga.nexo.data.services.MyFirebaseInstanceIDService;
import es.hga.nexo.domain.repository.LoginRepository;
import es.hga.nexo.presentation.helpers.LocationHelper;
import es.hga.nexo.presentation.view.activity.MainActivity;
import es.hga.nexo.presentation.view.dashboard.DashboardFragment;
import es.hga.nexo.presentation.view.login.LoginFragment;
import es.hga.nexo.presentation.view.pendingorderdetails.PendingOrderDetailsFragment;
import es.hga.nexo.presentation.view.pendingorders.PendingOrdersFragment;
import es.hga.nexo.presentation.view.presence.PresenceFragment;
import es.hga.nexo.presentation.view.production.ProductionFragment;
import es.hga.nexo.presentation.view.settings.SettingsFragment;

@Singleton
@Component(modules = {ApplicationModule.class, ProjectModule.class})
public interface ApplicationComponent {
    void inject(MainActivity baseActivity);

    void inject(LoginFragment loginFragment);
    void inject(DashboardFragment dashboardFragment);
    void inject(PresenceFragment presenceFragment);
    void inject(ProductionFragment poductionFragment);
    void inject(PendingOrdersFragment pendingOrdersFragment);
    void inject(PendingOrderDetailsFragment pendingOrderDetailsFragment);
    void inject(SettingsFragment settingsFragment);
    void inject(MyFirebaseInstanceIDService myFirebaseInstanceIDService);
    void inject(LocationHelper locationHelper);
}