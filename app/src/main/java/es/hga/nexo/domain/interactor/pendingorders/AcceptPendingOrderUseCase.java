package es.hga.nexo.domain.interactor.pendingorders;

import javax.inject.Inject;

import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.PendingOrder;
import es.hga.nexo.domain.executor.PostExecutionThread;
import es.hga.nexo.domain.executor.ThreadExecutor;
import es.hga.nexo.domain.interactor.base.UseCaseSingle;
import es.hga.nexo.domain.repository.PendingOrderRepository;
import io.reactivex.Single;

/**
 * Created by alex on 10/05/2017.
 */

public class AcceptPendingOrderUseCase extends UseCaseSingle<DuxWsResponse, Integer> {
    private static final String TAG = "AcceptPendingOrderUseCase";

    private final PendingOrderRepository pendingOrderRepository;

    @Inject
    AcceptPendingOrderUseCase(PendingOrderRepository pendingOrderRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.pendingOrderRepository = pendingOrderRepository;
    }

    @Override
    public Single<DuxWsResponse> buildUseCaseSingle(Integer id) {
        return this.pendingOrderRepository.acceptPendingOrder(id);
    }
}
