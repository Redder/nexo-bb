package es.hga.nexo.domain.business;

import java.util.List;

public class Login {

    private String username;
    private String password;
    private List<Permission> permissions;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }

    ///////////////////////////////
    ///////////////////////////////

    public Permission getPermissionByFormCode(int formCode){

        if (permissions != null){
            for(Permission p : permissions) {
                if(p.getCodigoFormulario() == formCode) {
                    return p;
                }
            }
        }
        return null;
    }

    ///////////////////////////////
    ///////////////////////////////


    public class Permission{

        private int codigoFormulario;
        private boolean alta;
        private boolean baja;
        private boolean parcial;
        private boolean exportar;
        private boolean importar ;
        private boolean contabilizar;
        private boolean generar;
        private boolean verPropios;
        private boolean ver;
        private boolean enMenu;
        private boolean altaPropios;
        private boolean bajaPropios;

        public Permission(){

        }

        public int getCodigoFormulario() {
            return codigoFormulario;
        }

        public void setCodigoFormulario(int codigoFormulario) {
            this.codigoFormulario = codigoFormulario;
        }

        public boolean isAlta() {
            return alta;
        }

        public void setAlta(boolean alta) {
            this.alta = alta;
        }

        public boolean isBaja() {
            return baja;
        }

        public void setBaja(boolean baja) {
            this.baja = baja;
        }

        public boolean isParcial() {
            return parcial;
        }

        public void setParcial(boolean parcial) {
            this.parcial = parcial;
        }

        public boolean isExportar() {
            return exportar;
        }

        public void setExportar(boolean exportar) {
            this.exportar = exportar;
        }

        public boolean isImportar() {
            return importar;
        }

        public void setImportar(boolean importar) {
            this.importar = importar;
        }

        public boolean isContabilizar() {
            return contabilizar;
        }

        public void setContabilizar(boolean contabilizar) {
            this.contabilizar = contabilizar;
        }

        public boolean isGenerar() {
            return generar;
        }

        public void setGenerar(boolean generar) {
            this.generar = generar;
        }

        public boolean isVerPropios() {
            return verPropios;
        }

        public void setVerPropios(boolean verPropios) {
            this.verPropios = verPropios;
        }

        public boolean isVer() {
            return ver;
        }

        public void setVer(boolean ver) {
            this.ver = ver;
        }

        public boolean isEnMenu() {
            return enMenu;
        }

        public void setEnMenu(boolean enMenu) {
            this.enMenu = enMenu;
        }

        public boolean isAltaPropios() {
            return altaPropios;
        }

        public void setAltaPropios(boolean altaPropios) {
            this.altaPropios = altaPropios;
        }

        public boolean isBajaPropios() {
            return bajaPropios;
        }

        public void setBajaPropios(boolean bajaPropios) {
            this.bajaPropios = bajaPropios;
        }
    }



}
