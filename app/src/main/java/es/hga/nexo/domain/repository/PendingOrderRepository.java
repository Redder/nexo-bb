package es.hga.nexo.domain.repository;

import java.util.List;
import java.util.Map;

import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.PendingOrder;
import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * Created by alex on 10/05/2017.
 */

public interface PendingOrderRepository {

    //Maybe<List<PendingOrder>> addStatus(PendingOrder status);

    //Completable removeStatus(PendingOrder status);

    //Completable updateStatus(PendingOrder status);

    Maybe<List<PendingOrder>> getPendingOrdersList();

    Maybe<List<PendingOrder>> reloadPendingOrderListFromNetwork();

    Maybe<PendingOrder> getPendingOrderById(int id);

    Single<DuxWsResponse> acceptPendingOrder(int id);

    Single<DuxWsResponse> rejectPendingOrder(int id, String reason);

}
