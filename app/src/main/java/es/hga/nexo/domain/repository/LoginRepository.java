package es.hga.nexo.domain.repository;

import java.util.List;

import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.Login;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * Created by alex on 10/05/2017.
 */

public interface LoginRepository {

    Single<DuxWsResponse> doLogin(String company, String username, String password);
    Maybe<Login> loadLoginInfo();
    Login getLogin();
    Completable closeSession();
    Single<DuxWsResponse> setPushToken(String pushToken);

    Single<List<Login.Permission>> getPermissionsDux(String company, String username);

}
