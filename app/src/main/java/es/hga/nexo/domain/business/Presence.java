package es.hga.nexo.domain.business;

/**
 * Created by alex on 08/05/2017.
 */

public class Presence {

    private String Nombre;

    private String Apellido1;

    private String Apellido2;

    private String FechaNacimiento;

    private String Accion;

    private String Entrada;

    private String Hora;

    private String Texto;

    private Boolean TodoOk;

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getApellido1() {
        return Apellido1;
    }

    public void setApellido1(String apellido1) {
        Apellido1 = apellido1;
    }

    public String getApellido2() {
        return Apellido2;
    }

    public void setApellido2(String apellido2) {
        Apellido2 = apellido2;
    }

    public String getFechaNacimiento() {
        return FechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        FechaNacimiento = fechaNacimiento;
    }

    public String getAccion() {
        return Accion;
    }

    public void setAccion(String accion) {
        Accion = accion;
    }

    public String getEntrada() {
        return Entrada;
    }

    public void setEntrada(String entrada) {
        Entrada = entrada;
    }

    public String getHora() {
        return Hora;
    }

    public void setHora(String hora) {
        Hora = hora;
    }

    public String getTexto() {
        return Texto;
    }

    public void setTexto(String texto) {
        Texto = texto;
    }

    public Boolean getTodoOk() {
        return TodoOk;
    }

    public void setTodoOk(Boolean todoOk) {
        TodoOk = todoOk;
    }
}
