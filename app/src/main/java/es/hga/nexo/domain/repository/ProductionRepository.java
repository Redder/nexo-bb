package es.hga.nexo.domain.repository;


import es.hga.nexo.domain.business.Employee;
import io.reactivex.Single;

/**
 * Created by alex on 10/05/2017.
 */

public interface ProductionRepository {

    Single<Employee> checkCard(String card);

}
