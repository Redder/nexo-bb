package es.hga.nexo.domain.interactor.login;

import android.util.Log;

import javax.inject.Inject;

import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.Login;
import es.hga.nexo.domain.business.Settings;
import es.hga.nexo.domain.executor.PostExecutionThread;
import es.hga.nexo.domain.executor.ThreadExecutor;
import es.hga.nexo.domain.interactor.base.UseCaseMaybe;
import es.hga.nexo.domain.interactor.base.UseCaseSingle;
import es.hga.nexo.domain.repository.LoginRepository;
import es.hga.nexo.domain.repository.PendingOrderRepository;
import es.hga.nexo.domain.repository.SettingsRepository;
import io.reactivex.Maybe;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;

/**
 * Created by alex on 10/05/2017.
 */

public class DoLoginUseCase extends UseCaseMaybe<Login, Login> {
    private static final String TAG = "DoLoginUseCase";

    private final LoginRepository loginRepository;
    private final SettingsRepository settingsRepository;


    @Inject
    DoLoginUseCase(LoginRepository loginRepository, SettingsRepository settingsRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.loginRepository = loginRepository;
        this.settingsRepository = settingsRepository;

    }

    @Override
    public Maybe<Login> buildUseCaseMaybe(Login login) {

        return this.settingsRepository.loadSettings().flatMap(settings -> {
            //settings = settings;
            return this.loginRepository.doLogin(settings.getCompany() ,login.getUsername(), login.getPassword()).doOnSuccess(duxWsResponse -> {
                String t = duxWsResponse.getTexto();
                settingsRepository.saveSettings(settings.getUrlServer(), settings.getCompany(), t).subscribe();
                //loginRepository.getPermissionsDux("001","001").subscribe();
            }).flatMapMaybe(duxWsResponse -> {
                //return Maybe.empty();
                return this.loginRepository.loadLoginInfo();
            });
        });


        /*
        return this.settingsRepository.loadSettings().flatMapSingle(settings -> {
            //settings = settings;
            return this.loginRepository.doLogin(settings.getCompany() ,login.getUsername(), login.getPassword(), login.isRemember()).doOnSuccess(duxWsResponse -> {
                String t = duxWsResponse.getTexto();
                settingsRepository.saveSettings(settings.getUrlServer(), settings.getCompany(), t).subscribe();
                //loginRepository.getPermissionsDux("001","001").subscribe();
            }).flatMapMaybe(duxWsResponse -> {
                return Maybe.empty();
            });
        });
        */


        //return this.loginRepository.doLogin(login.getUsername(), login.getPassword(), login.isRemember());

    }
}
