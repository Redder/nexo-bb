package es.hga.nexo.domain.business;

/**
 * Created by alex on 08/05/2017.
 */

public class Settings {

    private String urlServer;
    private String company;
    private String token;

    public String getUrlServer() {
        return urlServer;
    }

    public void setUrlServer(String urlServer) {
        this.urlServer = urlServer;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
