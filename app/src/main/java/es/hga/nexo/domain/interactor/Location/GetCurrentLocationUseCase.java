package es.hga.nexo.domain.interactor.Location;

import javax.inject.Inject;

import es.hga.nexo.domain.business.NxLocation;
import es.hga.nexo.domain.executor.PostExecutionThread;
import es.hga.nexo.domain.executor.ThreadExecutor;
import es.hga.nexo.domain.interactor.base.UseCaseCompletable;
import es.hga.nexo.domain.interactor.base.UseCaseSingle;
import es.hga.nexo.domain.repository.LocationRepository;
import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Created by alex on 10/05/2017.
 */

public class GetCurrentLocationUseCase extends UseCaseSingle<NxLocation, Void> {
    private static final String TAG = "GetCurrentLcationUseCase";

    private final LocationRepository locationRepository;

    @Inject
    GetCurrentLocationUseCase(LocationRepository locationRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.locationRepository = locationRepository;
    }

    @Override
    public Single<NxLocation> buildUseCaseSingle(Void aVoid) {
        return this.locationRepository.getCurrentLocation();
    }
}
