package es.hga.nexo.domain.interactor.login;

import javax.inject.Inject;

import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.executor.PostExecutionThread;
import es.hga.nexo.domain.executor.ThreadExecutor;
import es.hga.nexo.domain.interactor.base.UseCaseSingle;
import es.hga.nexo.domain.repository.LoginRepository;
import io.reactivex.Single;



public class SetPushTokenUseCase extends UseCaseSingle<DuxWsResponse, String> {
    private static final String TAG = "SetPushTokenUseCase";

    private final LoginRepository loginRepository;

    @Inject
    SetPushTokenUseCase(LoginRepository loginRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.loginRepository = loginRepository;
    }

    @Override
    public Single<DuxWsResponse> buildUseCaseSingle(String pushToken) {

        return this.loginRepository.setPushToken(pushToken);

    }
}
