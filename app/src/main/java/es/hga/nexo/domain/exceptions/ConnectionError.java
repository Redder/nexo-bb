package es.hga.nexo.domain.exceptions;

public class ConnectionError extends Exception {

    public ConnectionError(){
        super("Error conectando con el servidor.");
    }

    public ConnectionError(String err){
        super(err);
    }

}
