package es.hga.nexo.domain.interactor.settings;

import javax.inject.Inject;

import es.hga.nexo.domain.business.Settings;
import es.hga.nexo.domain.executor.PostExecutionThread;
import es.hga.nexo.domain.executor.ThreadExecutor;
import es.hga.nexo.domain.interactor.base.UseCaseCompletable;
import es.hga.nexo.domain.interactor.base.UseCaseMaybe;
import es.hga.nexo.domain.repository.SettingsRepository;
import io.reactivex.Completable;
import io.reactivex.Maybe;


public class LoadSettingsUseCase extends UseCaseMaybe<Settings, Void> {


    private final SettingsRepository settingsRepository;

    @Inject
    LoadSettingsUseCase(SettingsRepository settingsRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.settingsRepository = settingsRepository;
    }

    @Override
    public Maybe<Settings> buildUseCaseMaybe(Void aVoid) {
        return this.settingsRepository.loadSettings();
    }


}
