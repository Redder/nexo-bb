package es.hga.nexo.domain.business;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alex on 08/05/2017.
 */

public class PendingOrder {

    private int id;
    private int numero;
    private int serie;
    private String estado;
    private String proveedor;
    private String documento;
    private float totalCosteUnidadPrincipal;
    private String creador;
    private String fecha;
    private long validTimestamp;
    private List<PendingOrderLine> lineas;

    public PendingOrder(){
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getSerie() {
        return serie;
    }

    public void setSerie(int serie) {
        this.serie = serie;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public float getTotalCosteUnidadPrincipal() {
        return totalCosteUnidadPrincipal;
    }

    public void setTotalCosteUnidadPrincipal(float totalCosteUnidadPrincipal) {
        this.totalCosteUnidadPrincipal = totalCosteUnidadPrincipal;
    }

    public String getCreador() {
        return creador;
    }

    public void setCreador(String creador) {
        this.creador = creador;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public List<PendingOrderLine> getLineas() {
        return lineas;
    }

    public long getValidTimestamp() {
        return validTimestamp;
    }

    public void setValidTimestamp(long validTimestamp) {
        this.validTimestamp = validTimestamp;
    }

    public void setLineas(List<PendingOrderLine> lineas) {
        this.lineas = lineas;
    }

    ///////////////////////////

    public String getCentrosCosteString(){

        ArrayList<String> ccList = new ArrayList<>();

        String toret = "";

        if (lineas != null){
            for (int i = 0; i < lineas.size(); i++){

                if (!ccList.contains(lineas.get(i).getCentrocoste())) {
                    toret += lineas.get(i).getCentrocoste() + " " ;
                    ccList.add(lineas.get(i).getCentrocoste());
                }//if

            }//for
        }//if


        return toret;
    }

    public boolean isListValid(){

        return false;

    }

    ///////////////////////////

    public class PendingOrderLine {

        private float totalLinea;
        private String concepto;
        private String estado;
        private String centrocoste;

        public PendingOrderLine(){

        }

        public float getTotalLinea() {
            return totalLinea;
        }

        public void setTotalLinea(float totalLinea) {
            this.totalLinea = totalLinea;
        }

        public String getConcepto() {
            return concepto;
        }

        public void setConcepto(String concepto) {
            this.concepto = concepto;
        }

        public String getEstado() {
            return estado;
        }

        public void setEstado(String estado) {
            this.estado = estado;
        }

        public String getCentrocoste() {
            return centrocoste;
        }

        public void setCentrocoste(String centrocoste) {
            this.centrocoste = centrocoste;
        }

        ////////////////////


    }
}
