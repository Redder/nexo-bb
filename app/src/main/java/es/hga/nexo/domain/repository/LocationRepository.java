package es.hga.nexo.domain.repository;

import es.hga.nexo.domain.business.NxLocation;
import es.hga.nexo.domain.business.Presence;
import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Created by alex on 10/05/2017.
 */

public interface LocationRepository {

    Completable setLocation(NxLocation nxLocation);
    Single<NxLocation> getCurrentLocation();

}
