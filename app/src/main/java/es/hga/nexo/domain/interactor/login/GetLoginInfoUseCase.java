package es.hga.nexo.domain.interactor.login;

import javax.inject.Inject;

import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.Login;
import es.hga.nexo.domain.executor.PostExecutionThread;
import es.hga.nexo.domain.executor.ThreadExecutor;
import es.hga.nexo.domain.interactor.base.UseCaseMaybe;
import es.hga.nexo.domain.interactor.base.UseCaseSingle;
import es.hga.nexo.domain.repository.LoginRepository;
import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * Created by alex on 10/05/2017.
 */

public class GetLoginInfoUseCase extends UseCaseMaybe<Login, Void> {


    private final LoginRepository loginRepository;


    @Inject
    GetLoginInfoUseCase(LoginRepository loginRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.loginRepository = loginRepository;
    }

    @Override
    public Maybe<Login> buildUseCaseMaybe(Void aVoid) {
        return this.loginRepository.loadLoginInfo();
    }
}
