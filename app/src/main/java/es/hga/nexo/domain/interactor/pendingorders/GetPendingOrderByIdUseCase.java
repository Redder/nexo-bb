package es.hga.nexo.domain.interactor.pendingorders;

import javax.inject.Inject;

import es.hga.nexo.domain.business.PendingOrder;
import es.hga.nexo.domain.executor.PostExecutionThread;
import es.hga.nexo.domain.executor.ThreadExecutor;
import es.hga.nexo.domain.interactor.base.UseCaseMaybe;
import es.hga.nexo.domain.interactor.base.UseCaseSingle;
import es.hga.nexo.domain.repository.PendingOrderRepository;
import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * Created by alex on 10/05/2017.
 */

public class GetPendingOrderByIdUseCase extends UseCaseMaybe<PendingOrder, Integer> {


    private final PendingOrderRepository pendingOrderRepository;


    @Inject
    GetPendingOrderByIdUseCase(PendingOrderRepository pendingOrderRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.pendingOrderRepository = pendingOrderRepository;
    }

    @Override
    public Maybe<PendingOrder> buildUseCaseMaybe(Integer id) {
        return this.pendingOrderRepository.getPendingOrderById(id);
    }
}
