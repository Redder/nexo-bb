package es.hga.nexo.domain.interactor.presence;

import javax.inject.Inject;

import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.Login;
import es.hga.nexo.domain.business.Presence;
import es.hga.nexo.domain.executor.PostExecutionThread;
import es.hga.nexo.domain.executor.ThreadExecutor;
import es.hga.nexo.domain.interactor.base.UseCaseMaybe;
import es.hga.nexo.domain.interactor.base.UseCaseSingle;
import es.hga.nexo.domain.interactor.model.PresenceLocationParam;
import es.hga.nexo.domain.repository.LoginRepository;
import es.hga.nexo.domain.repository.PresenceRepository;
import es.hga.nexo.domain.repository.SettingsRepository;
import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * Created by alex on 10/05/2017.
 */

public class SavePresenceUseCase extends UseCaseSingle<Presence, PresenceLocationParam> {
    private static final String TAG = "SavePresenceUseCase";

    private final PresenceRepository presenceRepository;

    @Inject
    SavePresenceUseCase(PresenceRepository presenceRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.presenceRepository = presenceRepository;
    }

    @Override
    public Single<Presence> buildUseCaseSingle(PresenceLocationParam presenceLocationParam) {
        return this.presenceRepository.savePresence(presenceLocationParam);
    }
}
