package es.hga.nexo.domain.business;

public class DuxWsResponse {

    //{"OP_OK":false,"Campo":"","NecesitaRespuesta":false,"Mens":42,"ListaMens":[],"ListaMensConMens":[],"Texto":"El estado del registro no permite ser tratado."}

    //Resultado de la operacion
    private boolean OP_OK = false;
    //Mensaje a mostrar
    private String Texto = "";

    ////no aplica//////
    private String Campo = "";
    private boolean NecesitaRespuesta = false;
    private int Mens = 0;
    //////////

    public boolean isOP_OK() {
        return OP_OK;
    }

    public void setOP_OK(boolean OP_OK) {
        this.OP_OK = OP_OK;
    }

    public String getCampo() {
        return Campo;
    }

    public void setCampo(String campo) {
        Campo = campo;
    }

    public boolean isNecesitaRespuesta() {
        return NecesitaRespuesta;
    }

    public void setNecesitaRespuesta(boolean necesitaRespuesta) {
        NecesitaRespuesta = necesitaRespuesta;
    }

    public int getMens() {
        return Mens;
    }

    public void setMens(int mens) {
        Mens = mens;
    }

    public String getTexto() {
        return Texto;
    }

    public void setTexto(String texto) {
        Texto = texto;
    }
}
