package es.hga.nexo.domain.interactor.model;

import es.hga.nexo.domain.business.NxLocation;

/**
 * Created by alex on 22/05/2017.
 */

public class PresenceLocationParam {

    private String card;
    private NxLocation nxLocation;

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public NxLocation getNxLocation() {
        return nxLocation;
    }

    public void setNxLocation(NxLocation nxLocation) {
        this.nxLocation = nxLocation;
    }
}
