package es.hga.nexo.domain.repository;

import java.util.List;

import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.Login;
import es.hga.nexo.domain.business.Presence;
import es.hga.nexo.domain.interactor.model.PresenceLocationParam;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * Created by alex on 10/05/2017.
 */

public interface PresenceRepository {

    Single<Presence> savePresence(PresenceLocationParam presenceLocationParam);

}
