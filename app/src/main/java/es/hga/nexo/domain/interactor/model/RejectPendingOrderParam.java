package es.hga.nexo.domain.interactor.model;

/**
 * Created by alex on 22/05/2017.
 */

public class RejectPendingOrderParam {

    private int id;
    private String reason;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
