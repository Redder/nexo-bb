package es.hga.nexo.domain.interactor.settings;

import javax.inject.Inject;

import es.hga.nexo.domain.business.Settings;
import es.hga.nexo.domain.executor.PostExecutionThread;
import es.hga.nexo.domain.executor.ThreadExecutor;
import es.hga.nexo.domain.interactor.base.UseCaseCompletable;
import es.hga.nexo.domain.repository.SettingsRepository;
import io.reactivex.Completable;


public class SaveSettingsUseCase extends UseCaseCompletable<Settings> {


    private final SettingsRepository settingsRepository;

    @Inject
    SaveSettingsUseCase(SettingsRepository settingsRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.settingsRepository = settingsRepository;
    }

    @Override
    public Completable buildUseCaseCompletable(Settings settings) {
        return this.settingsRepository.saveSettings(settings.getUrlServer(), settings.getCompany(), settings.getToken());
    }


}
