package es.hga.nexo.domain.business;

/**
 * Created by alex on 08/05/2017.
 */

public class Employee {

    private String Codigo;

    private String Tarjeta;

    private String Nombre;

    private String Apellido1;

    private String Apellido2;

    private int Maquina;

    private int Tipo;

    private int Turno;

    private int Horario;

    private String BDEmpleado;

    private int Orden;

    private int Nocturnidad;

    private int TipoNocturnidad;

    private String TrabajosAbiertos;

    private int Departamento;

    private String Resultado;


    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public String getTarjeta() {
        return Tarjeta;
    }

    public void setTarjeta(String tarjeta) {
        Tarjeta = tarjeta;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getApellido1() {
        return Apellido1;
    }

    public void setApellido1(String apellido1) {
        Apellido1 = apellido1;
    }

    public String getApellido2() {
        return Apellido2;
    }

    public void setApellido2(String apellido2) {
        Apellido2 = apellido2;
    }

    public int getMaquina() {
        return Maquina;
    }

    public void setMaquina(int maquina) {
        Maquina = maquina;
    }

    public int getTipo() {
        return Tipo;
    }

    public void setTipo(int tipo) {
        Tipo = tipo;
    }

    public int getTurno() {
        return Turno;
    }

    public void setTurno(int turno) {
        Turno = turno;
    }

    public int getHorario() {
        return Horario;
    }

    public void setHorario(int horario) {
        Horario = horario;
    }

    public String getBDEmpleado() {
        return BDEmpleado;
    }

    public void setBDEmpleado(String BDEmpleado) {
        this.BDEmpleado = BDEmpleado;
    }

    public int getOrden() {
        return Orden;
    }

    public void setOrden(int orden) {
        Orden = orden;
    }

    public int getNocturnidad() {
        return Nocturnidad;
    }

    public void setNocturnidad(int nocturnidad) {
        Nocturnidad = nocturnidad;
    }

    public int getTipoNocturnidad() {
        return TipoNocturnidad;
    }

    public void setTipoNocturnidad(int tipoNocturnidad) {
        TipoNocturnidad = tipoNocturnidad;
    }

    public String getTrabajosAbiertos() {
        return TrabajosAbiertos;
    }

    public void setTrabajosAbiertos(String trabajosAbiertos) {
        TrabajosAbiertos = trabajosAbiertos;
    }

    public int getDepartamento() {
        return Departamento;
    }

    public void setDepartamento(int departamento) {
        Departamento = departamento;
    }

    public String getResultado() {
        return Resultado;
    }

    public void setResultado(String resultado) {
        Resultado = resultado;
    }
}
