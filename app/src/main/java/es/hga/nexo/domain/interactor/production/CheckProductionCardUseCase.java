package es.hga.nexo.domain.interactor.production;

import javax.inject.Inject;

import es.hga.nexo.domain.business.Employee;
import es.hga.nexo.domain.business.Presence;
import es.hga.nexo.domain.executor.PostExecutionThread;
import es.hga.nexo.domain.executor.ThreadExecutor;
import es.hga.nexo.domain.interactor.base.UseCaseSingle;
import es.hga.nexo.domain.interactor.model.PresenceLocationParam;
import es.hga.nexo.domain.repository.PresenceRepository;
import es.hga.nexo.domain.repository.ProductionRepository;
import io.reactivex.Single;

/**
 * Created by alex on 10/05/2017.
 */

public class CheckProductionCardUseCase extends UseCaseSingle<Employee, String> {
    private static final String TAG = "CheckProductionCardUseCase";

    private final ProductionRepository productionRepository;

    @Inject
    CheckProductionCardUseCase(ProductionRepository productionRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.productionRepository = productionRepository;
    }

    @Override
    public Single<Employee> buildUseCaseSingle(String card) {
        return this.productionRepository.checkCard(card);
    }
}
