package es.hga.nexo.domain.interactor.login;

import javax.inject.Inject;

import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.Login;
import es.hga.nexo.domain.business.Settings;
import es.hga.nexo.domain.executor.PostExecutionThread;
import es.hga.nexo.domain.executor.ThreadExecutor;
import es.hga.nexo.domain.interactor.base.UseCaseCompletable;
import es.hga.nexo.domain.interactor.base.UseCaseSingle;
import es.hga.nexo.domain.repository.LoginRepository;
import es.hga.nexo.domain.repository.SettingsRepository;
import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Created by alex on 10/05/2017.
 */

public class CloseSessionUseCase extends UseCaseCompletable<Void> {
    private static final String TAG = "CloseSessionUseCase";

    private final LoginRepository loginRepository;
    private final SettingsRepository settingsRepository;


    @Inject
    CloseSessionUseCase(LoginRepository loginRepository, SettingsRepository settingsRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.loginRepository = loginRepository;
        this.settingsRepository = settingsRepository;

    }

    @Override
    public Completable buildUseCaseCompletable(Void aVoid) {

        return this.loginRepository.closeSession().doOnComplete(() -> {
            settingsRepository.loadSettings().doOnSuccess(settings -> {
                settingsRepository.saveSettings(settings.getUrlServer(),settings.getCompany(),"");
            });
        });

    }
}
