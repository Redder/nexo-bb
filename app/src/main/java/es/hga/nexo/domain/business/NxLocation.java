package es.hga.nexo.domain.business;

import android.location.Location;

import java.util.Date;

/**
 * Created by alex on 08/05/2017.
 */

public class NxLocation {

    private Location location;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
