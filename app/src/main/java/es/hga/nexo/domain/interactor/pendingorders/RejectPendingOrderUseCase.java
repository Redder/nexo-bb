package es.hga.nexo.domain.interactor.pendingorders;

import java.util.Map;

import javax.inject.Inject;

import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.executor.PostExecutionThread;
import es.hga.nexo.domain.executor.ThreadExecutor;
import es.hga.nexo.domain.interactor.base.UseCaseSingle;
import es.hga.nexo.domain.interactor.model.RejectPendingOrderParam;
import es.hga.nexo.domain.repository.PendingOrderRepository;
import io.reactivex.Single;

/**
 * Created by alex on 10/05/2017.
 */

public class RejectPendingOrderUseCase extends UseCaseSingle<DuxWsResponse, RejectPendingOrderParam> {


    private final PendingOrderRepository pendingOrderRepository;


    @Inject
    RejectPendingOrderUseCase(PendingOrderRepository pendingOrderRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.pendingOrderRepository = pendingOrderRepository;
    }

    @Override
    public Single<DuxWsResponse> buildUseCaseSingle(RejectPendingOrderParam params) {
        return this.pendingOrderRepository.rejectPendingOrder(params.getId(),params.getReason());
    }
}
