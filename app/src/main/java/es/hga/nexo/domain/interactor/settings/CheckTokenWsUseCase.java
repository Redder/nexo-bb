package es.hga.nexo.domain.interactor.settings;

import javax.inject.Inject;

import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.executor.PostExecutionThread;
import es.hga.nexo.domain.executor.ThreadExecutor;
import es.hga.nexo.domain.interactor.base.UseCaseSingle;
import es.hga.nexo.domain.repository.LoginRepository;
import es.hga.nexo.domain.repository.SettingsRepository;
import io.reactivex.Single;

/**
 * Created by alex on 10/05/2017.
 */

public class CheckTokenWsUseCase extends UseCaseSingle<DuxWsResponse, Void> {


    private final SettingsRepository settingsRepository;


    @Inject
    CheckTokenWsUseCase(SettingsRepository settingsRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.settingsRepository = settingsRepository;
    }

    @Override
    public Single<DuxWsResponse> buildUseCaseSingle(Void aVoid) {
        return this.settingsRepository.checkTokenWS();
    }
}
