package es.hga.nexo.domain.interactor.pendingorders;

import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import es.hga.nexo.domain.business.PendingOrder;
import es.hga.nexo.domain.executor.PostExecutionThread;
import es.hga.nexo.domain.executor.ThreadExecutor;
import es.hga.nexo.domain.interactor.base.UseCaseMaybe;
import es.hga.nexo.domain.repository.PendingOrderRepository;
import io.reactivex.Maybe;

/**
 * Created by alex on 10/05/2017.
 */

public class GetPendingOrdersListUseCase extends UseCaseMaybe<List<PendingOrder>, Void> {

    private static final String TAG = "GetPendOrdersLstUseCase";

    private final PendingOrderRepository pendingOrderRepository;


    @Inject
    GetPendingOrdersListUseCase(PendingOrderRepository pendingOrderRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.pendingOrderRepository = pendingOrderRepository;
    }


    @Override
    public Maybe<List<PendingOrder>> buildUseCaseMaybe(Void aVoid) {
        Log.d(TAG, "buildUseCaseMaybe");
        return pendingOrderRepository.getPendingOrdersList();
    }

}
