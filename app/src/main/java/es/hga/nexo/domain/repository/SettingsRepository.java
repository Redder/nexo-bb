package es.hga.nexo.domain.repository;

import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.Login;
import es.hga.nexo.domain.business.Settings;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * Created by alex on 10/05/2017.
 */

public interface SettingsRepository {

    Completable saveSettings(String urlServer, String company, String token);
    Maybe<Settings> loadSettings();

    Single<DuxWsResponse> checkTokenWS();

    Settings getSettings();

}
