package es.hga.nexo.domain.business;

/**
 * Created by alex on 08/05/2017.
 */

public class Dashboard {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
