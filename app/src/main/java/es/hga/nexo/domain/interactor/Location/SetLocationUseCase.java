package es.hga.nexo.domain.interactor.Location;

import javax.inject.Inject;

import es.hga.nexo.domain.business.NxLocation;
import es.hga.nexo.domain.business.Presence;
import es.hga.nexo.domain.executor.PostExecutionThread;
import es.hga.nexo.domain.executor.ThreadExecutor;
import es.hga.nexo.domain.interactor.base.UseCaseCompletable;
import es.hga.nexo.domain.interactor.base.UseCaseSingle;
import es.hga.nexo.domain.repository.LocationRepository;
import es.hga.nexo.domain.repository.PresenceRepository;
import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Created by alex on 10/05/2017.
 */

public class SetLocationUseCase extends UseCaseCompletable<NxLocation> {
    private static final String TAG = "SetLocationUseCase";

    private final LocationRepository locationRepository;

    @Inject
    SetLocationUseCase(LocationRepository locationRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.locationRepository = locationRepository;
    }

    @Override
    public Completable buildUseCaseCompletable(NxLocation nxLocation) {
        return this.locationRepository.setLocation(nxLocation);
    }
}
