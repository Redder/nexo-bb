package es.hga.nexo.data.repository;


import android.util.Log;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import es.hga.nexo.data.cache.login.disk.LoginDiskCache;
import es.hga.nexo.data.cache.login.memory.LoginMemoryCache;
import es.hga.nexo.data.cache.login.network.LoginNetworkCache;
import es.hga.nexo.data.cache.presence.network.PresenceNetworkCache;
import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.Login;
import es.hga.nexo.domain.business.Presence;
import es.hga.nexo.domain.interactor.model.PresenceLocationParam;
import es.hga.nexo.domain.repository.PresenceRepository;
import io.reactivex.Single;

@Singleton
public class PresenceDataRepository implements PresenceRepository {

    private static final String TAG = "PresenceDataRepository";
    private PresenceNetworkCache mPresenceNetworkCache;

    @Inject
    PresenceDataRepository(PresenceNetworkCache presenceNetworkCache) {
        this.mPresenceNetworkCache = presenceNetworkCache;
    }

    @Override
    public Single<Presence> savePresence(PresenceLocationParam presenceLocationParam) {

        return mPresenceNetworkCache.savePresence(presenceLocationParam);

    }

}

