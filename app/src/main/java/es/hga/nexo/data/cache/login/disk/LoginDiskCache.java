package es.hga.nexo.data.cache.login.disk;

import es.hga.nexo.data.cache.Cache;
import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.Login;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;


public interface LoginDiskCache extends Cache {

    Maybe<Login> loadLoginInfo();

    Login getLogin();

    Single<Boolean> setLoginInfo(Login login);

    Completable closeSession();

    //Single<PendingOrder> getPendingOrderById(int id);

    //Completable removePendingOrder(PendingOrder pendingOrder);

    //Completable updatePendingOrder(PendingOrder pendingOrder);

}
