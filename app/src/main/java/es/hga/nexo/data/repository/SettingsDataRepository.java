package es.hga.nexo.data.repository;


import android.util.Log;

import javax.inject.Inject;
import javax.inject.Singleton;

import es.hga.nexo.data.cache.login.disk.LoginDiskCache;
import es.hga.nexo.data.cache.login.memory.LoginMemoryCache;
import es.hga.nexo.data.cache.login.network.LoginNetworkCache;
import es.hga.nexo.data.cache.settings.disk.SettingsDiskCache;
import es.hga.nexo.data.cache.settings.memory.SettingsMemoryCache;
import es.hga.nexo.data.cache.settings.network.SettingsNetworkCache;
import es.hga.nexo.data.helper.SettingsDataHelper;
import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.Login;
import es.hga.nexo.domain.business.Settings;
import es.hga.nexo.domain.repository.LoginRepository;
import es.hga.nexo.domain.repository.SettingsRepository;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;

@Singleton
public class SettingsDataRepository implements SettingsRepository {

    private static final String TAG = "SettingsDataRepository";
    private SettingsMemoryCache mSettingsMemoryCache;
    private SettingsDiskCache mSettingsDiskCache;
    private SettingsNetworkCache mSettingsNetworkCache;


    @Inject
    SettingsDataRepository(SettingsMemoryCache settingsMemoryCache,
                           SettingsDiskCache settingsDiskCache,
                           SettingsNetworkCache settingsNetworkCache
    ) {
        this.mSettingsMemoryCache = settingsMemoryCache;
        this.mSettingsDiskCache = settingsDiskCache;
        this.mSettingsNetworkCache = settingsNetworkCache;
    }


    private Maybe<Settings> getSettingsInfoFromMemory(){
        return mSettingsMemoryCache.loadSettingsInfo();
    }

    private Maybe<Settings> getSettingsInfoFromDisk(){
        return mSettingsDiskCache.loadSettingsInfo().doOnSuccess(settings -> {
            Log.d(TAG,"settings: " + settings);
            mSettingsMemoryCache.setSettingsInfo(settings);
        });
    }

    @Override
    public Settings getSettings(){
        Settings settings = mSettingsMemoryCache.getSettings();

        if (settings == null){
            settings = mSettingsDiskCache.getSettings();
        }

        if (settings == null){
            settings = new Settings();
        }

        return settings;
    }

    @Override
    public Maybe<Settings> loadSettings() {

        Log.d(TAG,"loadSettings");

        return Maybe.concat(getSettingsInfoFromMemory(), getSettingsInfoFromDisk())
                .filter(settings -> settings != null)
                .firstElement();
    }

    @Override
    public Completable saveSettings(String urlServer, String company, String token) {

        Log.d(TAG,"saveSettings");

        Settings settings = new Settings();
        settings.setUrlServer(urlServer);
        settings.setCompany(company);
        settings.setToken(token);

        //save to memory
        //mSettingsMemoryCache.setSettingsInfo(settings).subscribe();
        //save to disk
        //mSettingsDiskCache.setSettingsInfo(settings).subscribe();

        /*
        return mSettingsDiskCache.setSettingsInfo(settings).doOnComplete(() -> {
            mSettingsMemoryCache.setSettingsInfo(settings);
        });
        */

        return mSettingsDiskCache.setSettingsInfo(settings).doOnComplete(() -> {
            mSettingsMemoryCache.setSettingsInfo(settings);
        });

    }

    @Override
    public Single<DuxWsResponse> checkTokenWS(){

        Settings settings =  getSettings();

        return mSettingsNetworkCache.checkTokenWS(settings.getUrlServer(),settings.getToken());
    }


    /*

    @Override
    public Maybe<List<PendingOrder>> getPendingOrdersList() {

        return Maybe.concat(pendingOrderCache.memory(), pendingOrderCache.disk(), pendingOrderCache.network())
                .filter(data -> data != null && !data.isEmpty())
                .firstElement();
    }

    @Override
    public Single<PendingOrder> getPendingOrderById(int id) {
        Log.d(TAG,"id: " + id);
        return pendingOrderCache.memory().flatMapSingle(pendingOrders ->

                Single.create(e -> {

                    boolean found = false;
                    for (PendingOrder p: pendingOrders) {
                        if (p.getId() == id){
                            e.onSuccess(p);
                            found = true;
                        }
                    }

                    if(!found){
                        e.onError(new IdNotFound());
                    }

                }));
    }

    @Override
    public Single<DuxWsResponse> acceptPendingOrder(int id) {

        return (pendingOrderCache.acceptPendingOrder(id));
    }

    @Override
    public Single<DuxWsResponse> rejectPendingOrder(int id, String reason) {

        return (pendingOrderCache.rejectPendingOrder(id, reason));
    }


    @Override
    public Maybe<List<PendingOrder>> addStatus(PendingOrder pendingOrder) {
        return PendingOrderMemoryCache.addPendingOrder(pendingOrder);
    }

    @Override
    public Completable removeStatus(PendingOrder pendingOrder) {
        return PendingOrderMemoryCache.removeStatus(pendingOrder);
    }

    @Override
    public Completable updateStatus(PendingOrder pendingOrder) {
        return PendingOrderMemoryCache.updateStatus(status);
    }
*/
}

