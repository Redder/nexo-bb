package es.hga.nexo.data.cache.production.network;

import android.util.Log;

import java.io.IOException;

import javax.inject.Inject;

import es.hga.nexo.data.helper.SettingsDataHelper;
import es.hga.nexo.data.services.APIServiceRetrofit;
import es.hga.nexo.data.services.ApiServiceRetrofitImp;
import es.hga.nexo.domain.business.Employee;
import es.hga.nexo.domain.exceptions.ConnectionError;
import io.reactivex.Single;

public class ProductionNetworkCacheImpl implements ProductionNetworkCache {
    private static final String TAG = "ProductionNetCacheImpl";
    private static final int NOT_FOUND = -1;

    SettingsDataHelper mSettingsDataHelper;

    @Inject
    public ProductionNetworkCacheImpl(SettingsDataHelper settingsDataHelper) {
        mSettingsDataHelper = settingsDataHelper;
        //mock();
    }

    private void mock() {

    }

    @Override
    public Single<Employee> checkCard(String card) {

        APIServiceRetrofit apiRestService = ApiServiceRetrofitImp.createApiRestService(mSettingsDataHelper.getSettings().getUrlServer(), mSettingsDataHelper.getSettings().getToken());

        return apiRestService.comprobarEmpleadoProduccionByToken(card, "Android: NexoApp")
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof IOException){
                        Log.d(TAG, "checkCard onErrorResumeNext 1: " + throwable.toString());
                        return Single.error(new ConnectionError());
                    }
                    Log.d(TAG, "checkCard onErrorResumeNext 2: " + throwable.toString());
                    return Single.error((throwable));
                })
                .flatMap(productionResponseResponse -> {
                    if (!productionResponseResponse.isSuccessful()) {
                        return Single.error(new Exception("Error de api."));
                    }

                    return Single.defer(() -> Single.just(productionResponseResponse.body()));
                });

    }

}//class

