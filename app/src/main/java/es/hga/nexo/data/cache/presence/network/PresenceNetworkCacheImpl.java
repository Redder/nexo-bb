package es.hga.nexo.data.cache.presence.network;

import android.util.Log;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import es.hga.nexo.data.cache.pendingorders.network.PendingOrderNetworkCache;
import es.hga.nexo.data.helper.SettingsDataHelper;
import es.hga.nexo.data.services.APIServiceRetrofit;
import es.hga.nexo.data.services.ApiServiceRetrofitImp;
import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.PendingOrder;
import es.hga.nexo.domain.business.Presence;
import es.hga.nexo.domain.exceptions.ConnectionError;
import es.hga.nexo.domain.interactor.model.PresenceLocationParam;
import io.reactivex.Single;

public class PresenceNetworkCacheImpl implements PresenceNetworkCache {
    private static final String TAG = "PresenceNetCacheImpl";
    private static final int NOT_FOUND = -1;

    SettingsDataHelper mSettingsDataHelper;

    @Inject
    public PresenceNetworkCacheImpl(SettingsDataHelper settingsDataHelper) {
        mSettingsDataHelper = settingsDataHelper;
        //mock();
    }

    private void mock() {

    }

    @Override
    public Single<Presence> savePresence(PresenceLocationParam presenceLocationParam) {

        APIServiceRetrofit apiRestService = ApiServiceRetrofitImp.createApiRestService(mSettingsDataHelper.getSettings().getUrlServer(), mSettingsDataHelper.getSettings().getToken());

        //Log.d(TAG, "savePresence getToken: " + mSettingsDataHelper.getSettings().getToken());
        //Log.d(TAG, "savePresence getUrlServer: " + mSettingsDataHelper.getSettings().getUrlServer());
        Log.d(TAG, "savePresence getLatitude: " + presenceLocationParam.getNxLocation().getLocation().getLatitude());
        Log.d(TAG, "savePresence getLongitude: " + presenceLocationParam.getNxLocation().getLocation().getLongitude());
        Log.d(TAG, "savePresence getAccuracy FLOAT: " + presenceLocationParam.getNxLocation().getLocation().getAccuracy());
        Log.d(TAG, "savePresence getAccuracy DOUBLE: " + Double.parseDouble(new Float(presenceLocationParam.getNxLocation().getLocation().getAccuracy()).toString()));
        Log.d(TAG, "savePresence getProvider: " + presenceLocationParam.getNxLocation().getLocation().getProvider());

        return apiRestService.grabarPresencia(presenceLocationParam.getCard(),
                "Android: NexoApp",
                presenceLocationParam.getNxLocation().getLocation().getLatitude(),
                presenceLocationParam.getNxLocation().getLocation().getLongitude(),
                Double.parseDouble(new Float(presenceLocationParam.getNxLocation().getLocation().getAccuracy()).toString()),
                presenceLocationParam.getNxLocation().getLocation().getProvider()
                )
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof IOException){
                        Log.d(TAG, "savePresence onErrorResumeNext 1: " + throwable.toString());
                        return Single.error(new ConnectionError());
                    }
                    Log.d(TAG, "savePresence onErrorResumeNext 2: " + throwable.toString());
                    return Single.error((throwable));
                })
                .flatMap(presenceResponseResponse -> {
                    if (!presenceResponseResponse.isSuccessful()) {
                        return Single.error(new Exception("Error de api."));
                    }

                    return Single.defer(() -> Single.just(presenceResponseResponse.body()));
                });

    }

}//class

