package es.hga.nexo.data.cache.login.memory;

import javax.inject.Inject;

import es.hga.nexo.domain.business.Login;

import io.reactivex.Maybe;
import io.reactivex.Single;

public class LoginMemoryCacheImpl implements LoginMemoryCache {
    private static final String TAG = "LoginMemoryCacheImpl";

    private Login mLogin;
    boolean mSuccess;

    @Inject
    public LoginMemoryCacheImpl() {

        //mock();
    }

    private void mock() {

    }

    @Override
    public void clearMemory() {
        mLogin = null;
    }

    @Override
    public Single<Boolean> setLoginInfo(Login login) {
        mSuccess = true;

        try{
            mLogin = login;
        }catch (Exception e){
            mSuccess = false;
        }

        return Single.fromCallable(() -> mSuccess);
    }

    @Override
    public Maybe<Login> loadLoginInfo() {
        //return Maybe.empty();
        //Log.i(TAG, "memory Login size: " + pendingOrderList.size());
        //return Maybe.fromCallable(() -> pendingOrderList).filter(orders -> orders!=null && !orders.isEmpty());
        return Maybe.fromCallable(() -> mLogin).filter(lo -> lo!=null);
    }

    @Override
    public Login getLogin() {

        return mLogin;

    }


}//class
