package es.hga.nexo.data.services;

import android.util.Log;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import es.hga.nexo.data.helper.SettingsDataHelper;
import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.Login;
import es.hga.nexo.domain.business.PendingOrder;
import es.hga.nexo.domain.business.Settings;
import es.hga.nexo.domain.interactor.base.DefaultSingleObserver;
import es.hga.nexo.domain.repository.SettingsRepository;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by alex on 11/05/2017.
 */

public final class ApiServiceRetrofitImp {

    private static final String TAG = "ApiServiceRetrofitImp";
    //private static final String baseUrl = "http://192.168.2.176:5050/duxservice.asmx/";
    //private static final String baseUrl = "https://jsonplaceholder.typicode.com";
    private static String baseUrl = "";
    private static String token = "";

    private SettingsRepository mSettingsRepository;

    @Inject
    ApiServiceRetrofitImp(){
    }

    public static APIServiceRetrofit createApiRestService(String urlServer, String tk){

        baseUrl = urlServer;
        token = tk;

        //OkHttpClient client = new OkHttpClient();

        /*
        client.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                HttpUrl url = request.url().newBuilder().addQueryParameter("token",token).build();
                request = request.newBuilder().url(url).build();
                return chain.proceed(request);
            }
        });
        */

        OkHttpClient.Builder httpClient =
                new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                HttpUrl originalHttpUrl = original.url();

                HttpUrl url = originalHttpUrl.newBuilder()
                        .addQueryParameter("token", token)
                        .build();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .url(url);

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });


        if ((baseUrl == null) || (baseUrl.length() == 0)){
            baseUrl = "http://127.0.0.1/";
        }

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build()
                .create(APIServiceRetrofit.class);
    }


}
