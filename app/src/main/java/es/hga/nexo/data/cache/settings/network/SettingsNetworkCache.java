package es.hga.nexo.data.cache.settings.network;

import java.util.List;

import es.hga.nexo.data.cache.Cache;
import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.Login;
import io.reactivex.Single;

/**
 * Created by alex on 10/05/2017.
 */

public interface SettingsNetworkCache extends Cache {

    Single<DuxWsResponse> checkTokenWS(String urlServer,String tokenWS);


}
