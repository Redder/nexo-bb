package es.hga.nexo.data.cache.settings.memory;

import es.hga.nexo.data.cache.Cache;
import es.hga.nexo.domain.business.Login;
import es.hga.nexo.domain.business.Settings;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;


public interface SettingsMemoryCache extends Cache {

    Maybe<Settings> loadSettingsInfo();

    Settings getSettings();

    void setSettingsInfo(Settings settings);

    void clearMemory();

    //Single<PendingOrder> getPendingOrderById(int id);

    //Completable removePendingOrder(PendingOrder pendingOrder);

    //Completable updatePendingOrder(PendingOrder pendingOrder);

}
