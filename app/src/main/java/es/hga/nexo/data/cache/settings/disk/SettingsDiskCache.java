package es.hga.nexo.data.cache.settings.disk;

import es.hga.nexo.data.cache.Cache;
import es.hga.nexo.domain.business.Login;
import es.hga.nexo.domain.business.Settings;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;


public interface SettingsDiskCache extends Cache {

    Maybe<Settings> loadSettingsInfo();

    Settings getSettings();

    Completable setSettingsInfo(Settings settings);

    //Single<PendingOrder> getPendingOrderById(int id);

    //Completable removePendingOrder(PendingOrder pendingOrder);

    //Completable updatePendingOrder(PendingOrder pendingOrder);

}
