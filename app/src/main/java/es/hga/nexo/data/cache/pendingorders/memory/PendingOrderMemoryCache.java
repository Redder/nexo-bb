package es.hga.nexo.data.cache.pendingorders.memory;

import java.util.List;

import es.hga.nexo.data.cache.Cache;
import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.Login;
import es.hga.nexo.domain.business.PendingOrder;
import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * Created by alex on 10/05/2017.
 */

public interface PendingOrderMemoryCache extends Cache {

    Maybe<List<PendingOrder>> getPendingOrderList();

    Single<Boolean> setPendingOrderList(List<PendingOrder> pendingOrderList);

    void clearMemory();

}
