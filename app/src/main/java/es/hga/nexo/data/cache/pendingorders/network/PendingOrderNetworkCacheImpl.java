package es.hga.nexo.data.cache.pendingorders.network;

import android.util.Log;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import es.hga.nexo.data.helper.SettingsDataHelper;
import es.hga.nexo.data.services.APIServiceRetrofit;
import es.hga.nexo.data.services.ApiServiceRetrofitImp;
import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.PendingOrder;
import es.hga.nexo.domain.exceptions.ConnectionError;
import io.reactivex.Maybe;
import io.reactivex.Single;

public class PendingOrderNetworkCacheImpl implements PendingOrderNetworkCache {
    private static final String TAG = "PendOrderNetCacheImpl";
    private static final int NOT_FOUND = -1;

    private List<PendingOrder> mPendingOrderList = new LinkedList<>();
    DuxWsResponse duxWsResponse = new DuxWsResponse();

    SettingsDataHelper mSettingsDataHelper;

    @Inject
    public PendingOrderNetworkCacheImpl(SettingsDataHelper settingsDataHelper) {
        mSettingsDataHelper = settingsDataHelper;
        //mock();
    }

    private void mock() {

    }

    private int findById (int id){
        for(int i = 0; i < mPendingOrderList.size(); i++) {
            if (mPendingOrderList.get(i).getId() == id){
                return i;
            }
        }
        return NOT_FOUND;
    }
    /*

    @Override
    public Single<PendingOrder> getPendingOrderById (int id){

        //return pendingOrderList.get(findById(id));

        PendingOrder pendingOrder = pendingOrderList.get(1);

        return Single.fromCallable(() -> pendingOrder).filter(p -> p!=null);


    }
    */

    @Override
    public Maybe<List<PendingOrder>> getPendingOrderList() {

        APIServiceRetrofit apiRestService = ApiServiceRetrofitImp.createApiRestService(mSettingsDataHelper.getSettings().getUrlServer(), mSettingsDataHelper.getSettings().getToken());

        return apiRestService.getPedidosProveedorPendientes()
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof IOException){
                        return Single.error(new ConnectionError());
                    }
                    return Single.error((throwable));
                })
                .flatMapMaybe(listResponse -> {
                    /*
                    switch (listResponse.code()){

                        case ApiServiceStatusCode.UNAUTHORIZED:
                            return Maybe.error(new Exception("Unauthorized"));

                    }//switch
                    */

                    if (!listResponse.isSuccessful()) {
                        return Maybe.error(new Exception("Error de api."));
                    }

                    return Maybe.defer(() -> Maybe.just(listResponse.body()));
                });
    }

    /*
    @Override
    public Maybe<List<PendingOrder>> getPendingOrderList() {

        //APIServiceRetrofit apiRestService =  ApiServiceRetrofitImp.createApiRestService();

        apiRestService.getPedidosProveedorPendientes("001","001").enqueue(new Callback<List<PendingOrder>>() {
            @Override
            public void onResponse(Call<List<PendingOrder>> call, Response<List<PendingOrder>> response) {

                if (response.isSuccessful()){

                }

                //PUT MEMORY
                pendingOrderList = response.body();

                if (pendingOrderList == null){
                    Log.i(TAG, "pendingOrderList: null");
                    pendingOrderList = new LinkedList<>();
                }else{
                    Log.i("pendingOrderList", "length: " + pendingOrderList.size());
                }//else

            }//onResponse

            @Override
            public void onFailure(Call<List<PendingOrder>> call, Throwable t) {
                Log.i(TAG,"FAILURE: " + t.toString() + " - " + t.getMessage());
            }
        });

        return Maybe.fromCallable(() -> pendingOrderList)
                .filter(list -> list!=null && !list.isEmpty());

        //return Maybe.empty();
    }

    */

    @Override
    public Single<DuxWsResponse> acceptPendingOrder(int serie, int numero) {

        APIServiceRetrofit apiRestService = ApiServiceRetrofitImp.createApiRestService(mSettingsDataHelper.getSettings().getUrlServer(), mSettingsDataHelper.getSettings().getToken());

        return apiRestService.aceptarPedidoProveedorPendiente(serie,numero)
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof IOException){
                        return Single.error(new ConnectionError());
                    }
                    return Single.error((throwable));
                })
                .flatMap(duxWsResponseResponse -> {
                    if (!duxWsResponseResponse.isSuccessful()) {
                        return Single.error(new Exception("Error de api."));
                    }

                    return Single.defer(() -> Single.just(duxWsResponseResponse.body()));
                });

    }

    @Override
    public Single<DuxWsResponse> rejectPendingOrder(int serie, int numero, String reason) {

        APIServiceRetrofit apiRestService = ApiServiceRetrofitImp.createApiRestService(mSettingsDataHelper.getSettings().getUrlServer(), mSettingsDataHelper.getSettings().getToken());

        return apiRestService.rechazarPedidoProveedorPendiente(serie, numero, reason)
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof IOException){
                        return Single.error(new ConnectionError());
                    }
                    return Single.error((throwable));
                })
                .flatMap(duxWsResponseResponse -> {
                    if (!duxWsResponseResponse.isSuccessful()) {
                        return Single.error(new Exception("Error de api."));
                    }

                    return Single.defer(() -> Single.just(duxWsResponseResponse.body()));
                });
    }

}//class

