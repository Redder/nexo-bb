package es.hga.nexo.data.cache.login.memory;

import es.hga.nexo.data.cache.Cache;
import es.hga.nexo.domain.business.Login;
import io.reactivex.Maybe;
import io.reactivex.Single;


public interface LoginMemoryCache extends Cache {

    Maybe<Login> loadLoginInfo();

    Login getLogin();

    Single<Boolean> setLoginInfo(Login login);

    void clearMemory();

    //Single<PendingOrder> getPendingOrderById(int id);

    //Completable removePendingOrder(PendingOrder pendingOrder);

    //Completable updatePendingOrder(PendingOrder pendingOrder);

}
