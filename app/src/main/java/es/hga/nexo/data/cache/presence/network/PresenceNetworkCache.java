package es.hga.nexo.data.cache.presence.network;

import java.util.List;

import es.hga.nexo.data.cache.Cache;
import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.PendingOrder;
import es.hga.nexo.domain.business.Presence;
import es.hga.nexo.domain.interactor.model.PresenceLocationParam;
import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * Created by alex on 10/05/2017.
 */

public interface PresenceNetworkCache extends Cache {

    Single<Presence> savePresence(PresenceLocationParam presenceLocationParam);

}
