package es.hga.nexo.data.repository;


import android.util.Log;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import es.hga.nexo.data.cache.pendingorders.disk.PendingOrderDiskCache;
import es.hga.nexo.data.cache.pendingorders.memory.PendingOrderMemoryCache;
import es.hga.nexo.data.cache.pendingorders.network.PendingOrderNetworkCache;
import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.PendingOrder;
import es.hga.nexo.domain.repository.PendingOrderRepository;
import io.reactivex.Maybe;
import io.reactivex.Single;

@Singleton
public class PendingOrderDataRepository implements PendingOrderRepository {

    private static final String TAG = "PendingOrderDataRepo";
    private static final int PENDINGORDERLIST_VALID_TIME = 600000;//10 mins (600000)
    private PendingOrderMemoryCache mPendingOrderMemoryCache;
    private PendingOrderDiskCache mPendingOrderDiskCache;
    private PendingOrderNetworkCache mPendingOrderNetworkCache;

    private long pendingOrderListTimestamp = 0;

    @Inject
    PendingOrderDataRepository(PendingOrderMemoryCache pendingOrderMemoryCache,
                               PendingOrderDiskCache pendingOrderDiskCache,
                               PendingOrderNetworkCache pendingOrderCache) {

        this.mPendingOrderMemoryCache = pendingOrderMemoryCache;
        this.mPendingOrderDiskCache = pendingOrderDiskCache;
        this.mPendingOrderNetworkCache = pendingOrderCache;
    }

    /////////////////////////////

    private Maybe<List<PendingOrder>> getPendingOrderListFromMemory(){

        if ((System.currentTimeMillis() - pendingOrderListTimestamp) > PENDINGORDERLIST_VALID_TIME){
            return Maybe.empty();
        }

        return mPendingOrderMemoryCache.getPendingOrderList();

    }

    private Maybe<List<PendingOrder>> getPendingOrderListFromDisk(){

        if ((System.currentTimeMillis() - pendingOrderListTimestamp) > PENDINGORDERLIST_VALID_TIME){
            return Maybe.empty();
        }

        return mPendingOrderDiskCache.getPendingOrderList().doOnSuccess(pendingOrders -> {
            mPendingOrderMemoryCache.setPendingOrderList(pendingOrders);
        });

    }

    private Maybe<List<PendingOrder>> getPendingOrderListFromNetwork(){

        return mPendingOrderNetworkCache.getPendingOrderList().doOnSuccess(pendingOrders -> {
            pendingOrderListTimestamp = System.currentTimeMillis();
            mPendingOrderDiskCache.setPendingOrderList(pendingOrders);
            mPendingOrderMemoryCache.setPendingOrderList(pendingOrders);
        });

    }

    /////////////////////////////

    @Override
    public Maybe<List<PendingOrder>> getPendingOrdersList() {

        Log.d(TAG, "getPendingOrdersList");

        return Maybe.concat(getPendingOrderListFromMemory(), getPendingOrderListFromDisk(), getPendingOrderListFromNetwork())
                .filter(pendingOrders -> pendingOrders != null && !pendingOrders.isEmpty())
                .firstElement();

    }

    @Override
    public Maybe<List<PendingOrder>> reloadPendingOrderListFromNetwork(){
        Log.d(TAG, "reloadPendingOrderListFromNetwork");
        return getPendingOrderListFromNetwork();

    }


    @Override
    public Maybe<PendingOrder> getPendingOrderById(int id) {
        Log.d(TAG,"id: " + id);

        //Listado de memoria
        return getPendingOrdersList().flatMap(pendingOrders -> {

            //Busco en memoria si esta la orden pendiente
            return Maybe.create(e -> {
                Log.d(TAG,"flatmap IN");
                boolean foundMemo = false;
                for (PendingOrder p: pendingOrders) {
                    if (p.getId() == id){
                        e.onSuccess(p);
                        foundMemo = true;
                    }
                }

                //Si no esta, pido a network todos los pedidos de nuevo
                if(!foundMemo){

                    getPendingOrderListFromNetwork().flatMap(pendingOrdersNet -> {

                        //Busco si está en el nuevo listado
                        return Maybe.create(n -> {
                            boolean foundNet = false;
                            for (PendingOrder pNet: pendingOrdersNet) {
                                if (pNet.getId() == id){
                                    e.onSuccess(pNet);
                                    foundNet = true;
                                }
                            }

                            if(!foundNet){
                                e.onComplete();
                            }
                        });

                    }).subscribe();
                }

            });
        });
    }

    @Override
    public Single<DuxWsResponse> acceptPendingOrder(int id) {

        return this.getPendingOrderById(id).flatMapSingle(pendingOrder -> {

            //Log.d(TAG, "ID: " + id + " - serie: " + pendingOrder.getSerie() + " - numero: " + pendingOrder.getNumero());

            return mPendingOrderNetworkCache.acceptPendingOrder(pendingOrder.getSerie(), pendingOrder.getNumero()).doOnSuccess(duxWsResponse -> {

                if (duxWsResponse.isOP_OK()){
                    getPendingOrdersList().doOnSuccess(pendingOrders -> {

                        List<PendingOrder> newList = new LinkedList<PendingOrder>();
                        newList.addAll(pendingOrders);

                        Boolean found = false;
                        for (PendingOrder p: pendingOrders) {
                            if (p.getId() == id){
                                newList.remove(p);
                                found = true;
                            }
                        }

                        if (found){
                            mPendingOrderDiskCache.setPendingOrderList(newList).subscribe();
                            mPendingOrderMemoryCache.setPendingOrderList(newList).subscribe();
                        }

                    }).subscribe();
                }
            });

        });

    }

    @Override
    public Single<DuxWsResponse> rejectPendingOrder(int id, String reason) {

        return this.getPendingOrderById(id).flatMapSingle(pendingOrder -> {

            return mPendingOrderNetworkCache.rejectPendingOrder(pendingOrder.getSerie(), pendingOrder.getNumero(), reason).doOnSuccess(duxWsResponse -> {

                if (duxWsResponse.isOP_OK()){
                    getPendingOrdersList().doOnSuccess(pendingOrders -> {

                        List<PendingOrder> newList = new LinkedList<PendingOrder>();
                        newList.addAll(pendingOrders);

                        Boolean found = false;
                        for (PendingOrder p: pendingOrders) {
                            if (p.getId() == id){
                                newList.remove(p);
                                found = true;
                            }
                        }

                        if (found){
                            Log.d(TAG,"FOUND");
                            mPendingOrderDiskCache.setPendingOrderList(newList).subscribe();
                            mPendingOrderMemoryCache.setPendingOrderList(newList).subscribe();
                        }

                    }).subscribe();
                }

            });

        });
    }

/*
    @Override
    public Maybe<List<PendingOrder>> addStatus(PendingOrder pendingOrder) {
        return PendingOrderMemoryCache.addPendingOrder(pendingOrder);
    }

    @Override
    public Completable removeStatus(PendingOrder pendingOrder) {
        return PendingOrderMemoryCache.removeStatus(pendingOrder);
    }

    @Override
    public Completable updateStatus(PendingOrder pendingOrder) {
        return PendingOrderMemoryCache.updateStatus(status);
    }
*/
}

