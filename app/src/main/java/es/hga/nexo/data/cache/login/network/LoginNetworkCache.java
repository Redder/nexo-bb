package es.hga.nexo.data.cache.login.network;

import java.util.List;

import es.hga.nexo.data.cache.Cache;
import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.Login;
import io.reactivex.Single;

/**
 * Created by alex on 10/05/2017.
 */

public interface LoginNetworkCache extends Cache {

    Single<DuxWsResponse> doLogin(String company, String username, String password);

    Single<DuxWsResponse> setPushToken(String pushToken);

    Single<List<Login.Permission>> getPermissionsDux(String company, String username);

    //Single<PendingOrder> getPendingOrderById(int id);

    //Completable removePendingOrder(PendingOrder pendingOrder);

    //Completable updatePendingOrder(PendingOrder pendingOrder);

}
