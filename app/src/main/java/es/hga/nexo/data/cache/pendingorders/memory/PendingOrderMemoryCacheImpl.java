package es.hga.nexo.data.cache.pendingorders.memory;

import android.util.Log;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import es.hga.nexo.data.cache.PrefserDiskCache;
import es.hga.nexo.data.services.APIServiceRetrofit;
import es.hga.nexo.data.services.ApiServiceRetrofitImp;
import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.PendingOrder;
import io.reactivex.Maybe;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by alex on 10/05/2017.
 */

public class PendingOrderMemoryCacheImpl implements PendingOrderMemoryCache {
    private static final String TAG = "PendOrderMemCacheImpl";
    private static final int NOT_FOUND = -1;

    private List<PendingOrder> mPendingOrderList = new LinkedList<>();

    boolean mSuccess;

    @Inject
    public PendingOrderMemoryCacheImpl(PrefserDiskCache prefserDiskCache) {

        //mock();
    }

    private void mock() {

    }

    private int findById (int id){
        for(int i = 0; i < mPendingOrderList.size(); i++) {
            if (mPendingOrderList.get(i).getId() == id){
                return i;
            }
        }
        return NOT_FOUND;
    }
    /*

    @Override
    public Single<PendingOrder> getPendingOrderById (int id){

        //return pendingOrderList.get(findById(id));

        PendingOrder pendingOrder = pendingOrderList.get(1);

        return Single.fromCallable(() -> pendingOrder).filter(p -> p!=null);


    }
    */

    @Override
    public void clearMemory() {
        mPendingOrderList.clear();
    }

    @Override
    public Maybe<List<PendingOrder>> getPendingOrderList() {
        //return Maybe.empty();
        Log.i(TAG, "memory pendingOrderList size: " + mPendingOrderList.size());
        return Maybe.fromCallable(() -> mPendingOrderList).filter(orders -> orders!=null && !orders.isEmpty());
    }

    @Override
    public Single<Boolean> setPendingOrderList(List<PendingOrder> pendingOrderList) {

        mSuccess = true;

        try{
            mPendingOrderList = pendingOrderList;
        }catch (Exception e){
            mSuccess = false;
        }

        return Single.fromCallable(() -> mSuccess);

    }


}//class

