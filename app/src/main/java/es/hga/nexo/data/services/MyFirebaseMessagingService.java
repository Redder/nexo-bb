package es.hga.nexo.data.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import es.hga.nexo.R;
import es.hga.nexo.presentation.view.activity.MainActivity;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d(TAG, "FROM: " + remoteMessage.getFrom());

        //Check if the message contains data
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data: " + remoteMessage.getData());
            //Log.d(TAG, "Message data ID: " + remoteMessage.getData().get("id"));
        }

        /*
        //Check if the message contains notification
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message body: " + remoteMessage.getNotification().getBody());
            //sendNotification(remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody(),remoteMessage.getData().get("id"));
            sendNotification(remoteMessage.getData().get("title"),remoteMessage.getData().get("body"),remoteMessage.getData().get("id"));
        }
        */
        sendNotification(remoteMessage.getData().get("title"),remoteMessage.getData().get("body"),remoteMessage.getData().get("id"));
    }

        //Display notification
        private void sendNotification(String title,String body, String id){
            Log.d(TAG,"sendNotification - title: " + title);

            Intent intent = new Intent(this, MainActivity.class);
            //intent.putExtra("openFragment", "pendingordersdetails");
            intent.putExtra("openFragment", "pendingorderlist");
            intent.putExtra("pendingOrderId", id);

            Log.d(TAG,"sendNotification");

            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this,0 /*Request code*/,intent,PendingIntent.FLAG_ONE_SHOT);
            //Set sound of notification
            Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notifiBuilder = new NotificationCompat.Builder(this)
                    //.setSmallIcon(R.mipmap.ic_launcher)
                    .setSmallIcon(R.drawable.logo_nexo)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setAutoCancel(true)
                    .setSound(notificationSound)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0 /* ID of notification */, notifiBuilder.build());

        }

}
