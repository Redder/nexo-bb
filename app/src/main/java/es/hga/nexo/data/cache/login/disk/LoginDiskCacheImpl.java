package es.hga.nexo.data.cache.login.disk;

import com.github.pwittchen.prefser.library.TypeToken;

import java.lang.reflect.Type;

import javax.inject.Inject;

import es.hga.nexo.data.cache.PrefserDiskCache;
import es.hga.nexo.domain.business.Login;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;


public class LoginDiskCacheImpl implements LoginDiskCache {
    private static final String TAG = "LoginDiskCacheImpl";
    private static final String LOGIN_LOGIN_KEY = "login_login_key";
    private static final int NOT_FOUND = -1;

    private boolean mSuccess;

    private PrefserDiskCache prefserDiskCache;

    @Inject
    public LoginDiskCacheImpl(PrefserDiskCache prefserDiskCache) {

        this.prefserDiskCache = prefserDiskCache;

        //mock();
    }

    private void mock() {

    }

    @Override
    public Single<Boolean> setLoginInfo(Login login) {
        mSuccess = true;

        try{

            prefserDiskCache.put(LOGIN_LOGIN_KEY,login);

        }catch (Exception e){
            mSuccess = false;
        }

        return Single.fromCallable(() -> mSuccess);
    }

    @Override
    public Maybe<Login> loadLoginInfo() {
        //return Maybe.empty();

        Login login = getLogin();

        return Maybe.fromCallable(() -> login)
                .filter(lo -> lo != null);

    }

    @Override
    public Login getLogin() {
        //return Maybe.empty();

        Login login = prefserDiskCache.get(LOGIN_LOGIN_KEY, new TypeToken<Login>() {
            @Override
            public Type getType() {
                return super.getType();
            }
        }, null);

        return login;
    }

    @Override
    public Completable closeSession() {
        //return Maybe.empty();

        //prefserDiskCache.clear();
        //prefserDiskCache.put(LOGIN_LOGIN_KEY,new Login());
        prefserDiskCache.remove(LOGIN_LOGIN_KEY);

        return Completable.complete();
    }

}//class

