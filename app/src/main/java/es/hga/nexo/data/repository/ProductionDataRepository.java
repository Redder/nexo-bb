package es.hga.nexo.data.repository;


import javax.inject.Inject;
import javax.inject.Singleton;

import es.hga.nexo.data.cache.presence.network.PresenceNetworkCache;
import es.hga.nexo.data.cache.production.network.ProductionNetworkCache;
import es.hga.nexo.domain.business.Employee;
import es.hga.nexo.domain.business.Presence;
import es.hga.nexo.domain.interactor.model.PresenceLocationParam;
import es.hga.nexo.domain.repository.ProductionRepository;
import io.reactivex.Single;

@Singleton
public class ProductionDataRepository implements ProductionRepository {

    private static final String TAG = "ProductionDataRepository";

    private ProductionNetworkCache mProductionNetworkCache;

    @Inject
    ProductionDataRepository(ProductionNetworkCache productionNetworkCache) {
        mProductionNetworkCache = productionNetworkCache;
    }

    @Override
    public  Single<Employee> checkCard (String card){

        return mProductionNetworkCache.checkCard(card);

    }


}

