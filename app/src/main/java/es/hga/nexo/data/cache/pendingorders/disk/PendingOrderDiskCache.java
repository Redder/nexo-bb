package es.hga.nexo.data.cache.pendingorders.disk;

import java.util.List;

import es.hga.nexo.data.cache.Cache;
import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.PendingOrder;
import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * Created by alex on 10/05/2017.
 */

public interface PendingOrderDiskCache extends Cache {

    Maybe<List<PendingOrder>> getPendingOrderList();

    Single<Boolean> setPendingOrderList(List<PendingOrder> pendingOrderList);

}
