package es.hga.nexo.data.cache.pendingorders.disk;

import android.util.Log;

import com.github.pwittchen.prefser.library.TypeToken;

import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import es.hga.nexo.data.cache.PrefserDiskCache;
import es.hga.nexo.data.services.APIServiceRetrofit;
import es.hga.nexo.data.services.ApiServiceRetrofitImp;
import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.Login;
import es.hga.nexo.domain.business.PendingOrder;
import io.reactivex.Maybe;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by alex on 10/05/2017.
 */

public class PendingOrderDiskCacheImpl implements PendingOrderDiskCache {
    private static final String TAG = "PendingOrderDiskCacheImpl";
    private static final String PENDDINGORDER_LIST_KEY = "pendingorder_list_key";
    private static final int NOT_FOUND = -1;

    private List<PendingOrder> pendingOrderList = new LinkedList<>();

    private PrefserDiskCache prefserDiskCache;

    private boolean mSuccess;

    @Inject
    public PendingOrderDiskCacheImpl(PrefserDiskCache prefserDiskCache) {

        this.prefserDiskCache = prefserDiskCache;

        //mock();
    }

    private void mock() {

    }

    @Override
    public Single<Boolean> setPendingOrderList(List<PendingOrder> pendingOrderList) {
        mSuccess = true;

        try{

            prefserDiskCache.put(PENDDINGORDER_LIST_KEY,pendingOrderList);

        }catch (Exception e){
            mSuccess = false;
        }

        return Single.fromCallable(() -> mSuccess);
    }

    @Override
    public Maybe<List<PendingOrder>> getPendingOrderList() {
        //return Maybe.empty();

        List<PendingOrder> list = prefserDiskCache.get(PENDDINGORDER_LIST_KEY, new TypeToken<List<PendingOrder>>() {
            @Override
            public Type getType() {
                return super.getType();
            }
        }, null);

        return Maybe.fromCallable(() -> list)
                .filter(lo -> lo != null);

    }


}//class

