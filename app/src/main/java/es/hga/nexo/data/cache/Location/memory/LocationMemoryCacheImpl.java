package es.hga.nexo.data.cache.Location.memory;

import android.util.Log;

import java.io.IOException;

import javax.inject.Inject;

import es.hga.nexo.data.cache.presence.network.PresenceNetworkCache;
import es.hga.nexo.data.helper.SettingsDataHelper;
import es.hga.nexo.data.services.APIServiceRetrofit;
import es.hga.nexo.data.services.ApiServiceRetrofitImp;
import es.hga.nexo.domain.business.NxLocation;
import es.hga.nexo.domain.business.Presence;
import es.hga.nexo.domain.exceptions.ConnectionError;
import io.reactivex.Completable;
import io.reactivex.Single;

public class LocationMemoryCacheImpl implements LocationMemoryCache {
    private static final String TAG = "LocationMemoryCacheImpl";
    private static final int NOT_FOUND = -1;

    SettingsDataHelper mSettingsDataHelper;

    private NxLocation mCurrentLocation = null;

    @Inject
    public LocationMemoryCacheImpl(SettingsDataHelper settingsDataHelper) {
        mSettingsDataHelper = settingsDataHelper;
        //mock();
    }

    private void mock() {

    }

    @Override
    public void clearMemory() {
        mCurrentLocation = null;
    }

    @Override
    public Single<NxLocation> getCurrentLocation() {

        return Single.defer(() -> Single.just(mCurrentLocation));

    }

    @Override
    public Completable setLocation(NxLocation location) {
        try{
            mCurrentLocation = location;
        }catch (Exception e){
            return Completable.error(e);
        }

        return Completable.complete();
    }

}//class


