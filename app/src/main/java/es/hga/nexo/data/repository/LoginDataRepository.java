package es.hga.nexo.data.repository;


import android.graphics.LightingColorFilter;
import android.util.Log;

import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.inject.Inject;
import javax.inject.Singleton;

import es.hga.nexo.data.cache.login.disk.LoginDiskCache;
import es.hga.nexo.data.cache.login.memory.LoginMemoryCache;
import es.hga.nexo.data.cache.login.network.LoginNetworkCache;
import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.Login;
import es.hga.nexo.domain.repository.LoginRepository;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;
import io.reactivex.functions.Consumer;

@Singleton
public class LoginDataRepository implements LoginRepository {

    private static final String TAG = "LoginDataRepository";
    private LoginMemoryCache mLoginMemoryCache;
    private LoginDiskCache mLoginDiskCache;
    private LoginNetworkCache mLoginNetworkCache;

    @Inject
    LoginDataRepository(LoginMemoryCache loginMemoryCache,
                        LoginDiskCache loginDiskCache,
                        LoginNetworkCache loginNetworkCache
    ) {
        this.mLoginMemoryCache = loginMemoryCache;
        this.mLoginDiskCache = loginDiskCache;
        this.mLoginNetworkCache = loginNetworkCache;
    }

    @Override
    public Single<List<Login.Permission>> getPermissionsDux(String company, String username) {
        return mLoginNetworkCache.getPermissionsDux(company, username);
    }

    @Override
    public Single<DuxWsResponse> doLogin(String company, String username, String password) {

        //return mLoginNetworkCache.doLogin(username,password);

        Log.d(TAG,"username: " + username + " - password: " + password);

        return mLoginNetworkCache.doLogin(company, username, password).doOnSuccess(duxWsResponse -> {

            try {
                if (duxWsResponse.isOP_OK()) {

                    Login login = new Login();
                    login.setUsername(username);
                    login.setPassword(password);

                    mLoginNetworkCache.getPermissionsDux(company, username).doOnSuccess(permissions -> {
                        login.setPermissions(permissions);
                    }).subscribe();

                    //login.setToken(duxWsResponse.getTexto());

                    //save to memory
                    mLoginMemoryCache.setLoginInfo(login).subscribe();
                    //save to disk
                    mLoginDiskCache.setLoginInfo(login).subscribe();

                }else{
                    closeSession().subscribe();
                }
            }catch (Exception e){
                throw e;
            }

        });
    }

    private Maybe<Login> getLoginInfoFromMemory(){
        return mLoginMemoryCache.loadLoginInfo();
    }

    private Maybe<Login> getLoginInfoFromDisk(){
        return mLoginDiskCache.loadLoginInfo().doOnSuccess(login -> {
            mLoginMemoryCache.setLoginInfo(login);
        });
    }

    @Override
    public Maybe<Login> loadLoginInfo() {

        Log.d(TAG,"getLoginInfo");

        return Maybe.concat(getLoginInfoFromMemory(), getLoginInfoFromDisk())
                .firstElement();
    }

    @Override
    public Single<DuxWsResponse> setPushToken(String pushToken){
        return mLoginNetworkCache.setPushToken(pushToken);
    }

    @Override
    public Login getLogin(){
        Login login = mLoginMemoryCache.getLogin();

        if (login == null){
            login = mLoginDiskCache.getLogin();
        }

        if (login == null){
            login = new Login();
        }

        return login;
    }

    @Override
    public Completable closeSession() {

        Log.d(TAG,"closeSession");

        return mLoginDiskCache.closeSession().doOnComplete(() -> {
            mLoginMemoryCache.clearMemory();
        });

    }


    /*

    @Override
    public Maybe<List<PendingOrder>> getPendingOrdersList() {

        return Maybe.concat(pendingOrderCache.memory(), pendingOrderCache.disk(), pendingOrderCache.network())
                .filter(data -> data != null && !data.isEmpty())
                .firstElement();
    }

    @Override
    public Single<PendingOrder> getPendingOrderById(int id) {
        Log.d(TAG,"id: " + id);
        return pendingOrderCache.memory().flatMapSingle(pendingOrders ->

                Single.create(e -> {

                    boolean found = false;
                    for (PendingOrder p: pendingOrders) {
                        if (p.getId() == id){
                            e.onSuccess(p);
                            found = true;
                        }
                    }

                    if(!found){
                        e.onError(new IdNotFound());
                    }

                }));
    }

    @Override
    public Single<DuxWsResponse> acceptPendingOrder(int id) {

        return (pendingOrderCache.acceptPendingOrder(id));
    }

    @Override
    public Single<DuxWsResponse> rejectPendingOrder(int id, String reason) {

        return (pendingOrderCache.rejectPendingOrder(id, reason));
    }


    @Override
    public Maybe<List<PendingOrder>> addStatus(PendingOrder pendingOrder) {
        return PendingOrderMemoryCache.addPendingOrder(pendingOrder);
    }

    @Override
    public Completable removeStatus(PendingOrder pendingOrder) {
        return PendingOrderMemoryCache.removeStatus(pendingOrder);
    }

    @Override
    public Completable updateStatus(PendingOrder pendingOrder) {
        return PendingOrderMemoryCache.updateStatus(status);
    }
*/
}

