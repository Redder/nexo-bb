package es.hga.nexo.data.cache.settings.network;

import android.util.Log;

import java.io.IOException;

import javax.inject.Inject;

import es.hga.nexo.data.services.APIServiceRetrofit;
import es.hga.nexo.data.services.ApiServiceRetrofitImp;
import es.hga.nexo.domain.business.DuxWsResponse;

import es.hga.nexo.domain.exceptions.ConnectionError;
import io.reactivex.Single;

public class SettingsNetworkCacheImpl implements SettingsNetworkCache {

    private static final String TAG = "SettNetworkCacheImpl";

    DuxWsResponse mDuxWsResponse = new DuxWsResponse();

    @Inject
    public SettingsNetworkCacheImpl(){
        //mock();
    }

    private void mock() {

    }

    @Override
    public Single<DuxWsResponse> checkTokenWS(String urlServer, String tokenWS) {

        Log.d(TAG,"checkTokenWS - token: " + tokenWS);

        APIServiceRetrofit apiRestService = ApiServiceRetrofitImp.createApiRestService(urlServer, tokenWS);

        try {

            return apiRestService.checkTokenWS()
                    .onErrorResumeNext(throwable -> {
                        if (throwable instanceof IOException) {
                            return Single.error(new ConnectionError());
                        }
                        return Single.error(throwable);
                    })
                    .flatMap(duxWsResponseResponse -> {
                        if (!duxWsResponseResponse.isSuccessful()) {
                            return Single.error(new Exception("Error de api."));
                        }
                        return Single.defer(() -> Single.just(duxWsResponseResponse.body()));
                    });
        }catch (Exception e){
            throw  e;
        }

    }

}//class

