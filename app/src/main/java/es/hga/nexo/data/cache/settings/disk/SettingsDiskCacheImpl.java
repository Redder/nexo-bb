package es.hga.nexo.data.cache.settings.disk;

import com.github.pwittchen.prefser.library.TypeToken;

import java.lang.reflect.Type;

import javax.inject.Inject;

import es.hga.nexo.data.cache.PrefserDiskCache;
import es.hga.nexo.domain.business.Settings;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;


public class SettingsDiskCacheImpl implements SettingsDiskCache {
    private static final String TAG = "SettingsDiskCacheImpl";
    private static final String SETTINGS_SETTINGS_KEY = "settings_settings_key";
    private static final int NOT_FOUND = -1;


    private PrefserDiskCache prefserDiskCache;

    @Inject
    public SettingsDiskCacheImpl(PrefserDiskCache prefserDiskCache) {

        this.prefserDiskCache = prefserDiskCache;

        //mock();
    }

    private void mock() {

    }

    @Override
    public Completable setSettingsInfo(Settings settings) {

        prefserDiskCache.put(SETTINGS_SETTINGS_KEY,settings);


        return Completable.complete();
    }

    @Override
    public Maybe<Settings> loadSettingsInfo() {
        //return Maybe.empty();

        Settings settings = getSettings();

        return Maybe.fromCallable(() -> settings)
                .filter(s -> s != null);

    }

    @Override
    public Settings getSettings() {
        //return Maybe.empty();

        Settings settings = prefserDiskCache.get(SETTINGS_SETTINGS_KEY, new TypeToken<Settings>() {
            @Override
            public Type getType() {
                return super.getType();
            }
        }, null);

        return settings;

    }

}//class

