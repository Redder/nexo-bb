package es.hga.nexo.data.cache.production.network;

import es.hga.nexo.data.cache.Cache;
import es.hga.nexo.domain.business.Employee;
import es.hga.nexo.domain.business.Presence;
import io.reactivex.Single;

/**
 * Created by alex on 10/05/2017.
 */

public interface ProductionNetworkCache extends Cache {

    Single<Employee> checkCard(String card);

}
