package es.hga.nexo.data.helper;


import android.util.Log;

import javax.inject.Inject;
import javax.inject.Singleton;

import es.hga.nexo.domain.business.Settings;
import es.hga.nexo.domain.repository.SettingsRepository;

public final class SettingsDataHelper {

    @Inject
    SettingsRepository settingsRepository;

    private static final String TAG = "SettingsDataHelper";


    @Inject
    public SettingsDataHelper(){

    }

    public Settings getSettings(){

        return settingsRepository.getSettings();

    }

}

