package es.hga.nexo.data.cache.login.network;

import android.util.Log;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import es.hga.nexo.data.helper.SettingsDataHelper;
import es.hga.nexo.data.services.APIServiceRetrofit;
import es.hga.nexo.data.services.ApiServiceRetrofitImp;
import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.Login;
import es.hga.nexo.domain.exceptions.ConnectionError;
import io.reactivex.Single;

public class LoginNetworkCacheImpl implements LoginNetworkCache {

    private static final String TAG = "LoginNetworkCacheImpl";

    DuxWsResponse mDuxWsResponse = new DuxWsResponse();
    Login mLogin = new Login();

    SettingsDataHelper mSettingsDataHelper;

    @Inject
    public LoginNetworkCacheImpl(SettingsDataHelper settingsDataHelper) {
        mSettingsDataHelper = settingsDataHelper;
        //mock();
    }


    private void mock() {

    }

    @Override
    public Single<DuxWsResponse> doLogin(String company, String username, String password) {

        APIServiceRetrofit apiRestService = ApiServiceRetrofitImp.createApiRestService(mSettingsDataHelper.getSettings().getUrlServer(), "");

        try {

            return apiRestService.doLoginWS(company, username, password)
                    .onErrorResumeNext(throwable -> {
                        if (throwable instanceof IOException) {
                            return Single.error(new ConnectionError());
                        }
                        return Single.error(throwable);
                    })
                    .flatMap(duxWsResponseResponse -> {
                        if (!duxWsResponseResponse.isSuccessful()) {
                            return Single.error(new Exception("Error de api."));
                        }
                        return Single.defer(() -> Single.just(duxWsResponseResponse.body()));
                    });
        }catch (Exception e){
            throw  e;
        }
    }

    @Override
    public Single<DuxWsResponse> setPushToken(String pushToken) {
        APIServiceRetrofit apiRestService = ApiServiceRetrofitImp.createApiRestService(mSettingsDataHelper.getSettings().getUrlServer(), mSettingsDataHelper.getSettings().getToken());

        return apiRestService.setPushToken(pushToken)
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof IOException){
                        return Single.error(new ConnectionError());
                    }
                    return Single.error((throwable));
                })
                .flatMap(duxWsResponseResponse -> {
                    if (!duxWsResponseResponse.isSuccessful()) {
                        return Single.error(new Exception("Error de api."));
                    }
                    return Single.defer(() -> Single.just(duxWsResponseResponse.body()));
                });
    }

    @Override
    public Single<List<Login.Permission>> getPermissionsDux(String company, String username) {

        APIServiceRetrofit apiRestService = ApiServiceRetrofitImp.createApiRestService(mSettingsDataHelper.getSettings().getUrlServer(), "");

        return apiRestService.getPermisosDux(company, username)
                .doOnError(throwable -> {
                    Log.d(TAG,"getPermisosDux - doOnError: " + throwable.toString());
                })
                .onErrorResumeNext(throwable -> {
                    Log.d(TAG,"getPermisosDux - onErrorResumeNext: " + throwable.toString());
                    if (throwable instanceof IOException){
                        return  Single.error(new Exception("Error de api"));
                    }
                    return Single.error((throwable));
                })
                .flatMap(permissionsResponse -> {
                    if (!permissionsResponse.isSuccessful()) {
                        return Single.error(new Exception("Error de api."));
                    }
                    return Single.defer(() -> Single.just(permissionsResponse.body()));
                });
    }

    /*
    @Override
    public Single<DuxWsResponse> doLogin(String username, String password) {

        APIServiceRetrofit apiRestService =  ApiServiceRetrofitImp.createApiRestService();

        apiRestService.doLoginWS("001",username,password).enqueue(new Callback<DuxWsResponse>() {
            @Override
            public void onResponse(Call<DuxWsResponse> call, Response<DuxWsResponse> response) {

                mDuxWsResponse = response.body();

                if (mDuxWsResponse == null){
                    mDuxWsResponse = new DuxWsResponse();
                    mDuxWsResponse.setOP_OK(false);
                    mDuxWsResponse.setTexto("ERROR: duxWsResponse is null. (0x5495722211)");
                }

            }//onResponse

            @Override
            public void onFailure(Call<DuxWsResponse> call, Throwable t) {
                Log.i(TAG,"FAILURE: " + t.toString() + " - " + t.getMessage());
                mDuxWsResponse = new DuxWsResponse();
                mDuxWsResponse.setOP_OK(false);
                mDuxWsResponse.setTexto("FAILURE: " + t.toString() + " - " + t.getMessage() + " (0x87332434): ");
            }
        });

        return Single.fromCallable(() -> mDuxWsResponse);

        //return Maybe.empty();
    }
    */



}//class

