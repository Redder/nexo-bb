package es.hga.nexo.data.cache.Location.memory;

import es.hga.nexo.data.cache.Cache;
import es.hga.nexo.domain.business.NxLocation;
import es.hga.nexo.domain.business.Presence;
import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Created by alex on 10/05/2017.
 */

public interface LocationMemoryCache extends Cache {

    Completable setLocation(NxLocation location);
    Single<NxLocation> getCurrentLocation();
    void clearMemory();

}
