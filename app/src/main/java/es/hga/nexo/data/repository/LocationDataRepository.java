package es.hga.nexo.data.repository;

import android.util.Log;


import javax.inject.Inject;
import javax.inject.Singleton;

import es.hga.nexo.data.cache.Location.memory.LocationMemoryCache;
import es.hga.nexo.domain.business.NxLocation;
import es.hga.nexo.domain.repository.LocationRepository;
import io.reactivex.Completable;
import io.reactivex.Single;

@Singleton
public class LocationDataRepository implements LocationRepository {

    private static final String TAG = "LocationDataRepository";
    private LocationMemoryCache mLocationMemoryCache;

    @Inject
    LocationDataRepository(LocationMemoryCache locationMemoryCache) {
        this.mLocationMemoryCache = locationMemoryCache;
    }

    @Override
    public Completable setLocation(NxLocation nxLocation) {

        return mLocationMemoryCache.setLocation(nxLocation);

    }

    @Override
    public Single<NxLocation> getCurrentLocation() {

        return mLocationMemoryCache.getCurrentLocation();

    }


}

