package es.hga.nexo.data.services;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.util.Dictionary;
import java.util.Set;

import javax.inject.Inject;

import es.hga.nexo.MyApp;
import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.interactor.base.DefaultSingleObserver;
import es.hga.nexo.domain.interactor.login.SetPushTokenUseCase;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    public static final String TAG = "MyFirebaseInsIDService";

    @Inject
    SetPushTokenUseCase mSetPushTokenUseCase;

    /*
    @Inject
    public MyFirebaseInstanceIDService(SetPushTokenUseCase setPushTokenUseCase){
        mSetPushTokenUseCase = setPushTokenUseCase;
    }
    */

    @Override
    public void onCreate() {
        //MyApp.get(this).getcomponent.inject(this);
        ((MyApp)getApplication()).getApplicationComponent().inject(this);
    }

    @Override
    public void onTokenRefresh() {
        //Get updated token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG,"New Token: " + refreshedToken);

        //You can save the token into third party server to do anything you want
        if (refreshedToken != null && refreshedToken.length() > 0){

            //DiContainer diContainer = new DiContainer();
            mSetPushTokenUseCase.execute(new SetPushTokenObserver(), refreshedToken);

        }

    }

    private final class SetPushTokenObserver extends DefaultSingleObserver<DuxWsResponse> {

        @Override public void onError(Throwable e) {
            Log.d(TAG, "SetPushTokenObserver onERROR: " + e.toString());
        }

        @Override public void onSuccess(DuxWsResponse duxWsResponse) {
            Log.d(TAG, "SetPushTokenObserver onSuccess: " + duxWsResponse);
        }

    }
}
