package es.hga.nexo.data.cache.settings.memory;

import javax.inject.Inject;

import es.hga.nexo.data.cache.login.memory.LoginMemoryCache;
import es.hga.nexo.domain.business.Login;
import es.hga.nexo.domain.business.Settings;
import es.hga.nexo.domain.repository.SettingsRepository;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;

public class SettingsMemoryCacheImpl implements SettingsMemoryCache {
    private static final String TAG = "SettingsMemoryCacheImpl";

    private Settings mSettings;


    @Inject
    public SettingsMemoryCacheImpl() {

        //mock();
    }

    private void mock() {

    }

    @Override
    public void clearMemory() {
        mSettings = null;
    }

    @Override
    public void setSettingsInfo(Settings settings) {

        mSettings = settings;

    }

    @Override
    public Maybe<Settings> loadSettingsInfo() {
        //return Maybe.empty();
        //Log.i(TAG, "memory Login size: " + pendingOrderList.size());
        //return Maybe.fromCallable(() -> pendingOrderList).filter(orders -> orders!=null && !orders.isEmpty());
        return Maybe.fromCallable(() -> mSettings).filter(s -> s!=null);
    }

    public Settings getSettings() {
        return mSettings;
    }


}//class
