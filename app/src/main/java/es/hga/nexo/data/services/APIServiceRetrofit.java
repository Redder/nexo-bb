package es.hga.nexo.data.services;

import java.util.List;

import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.Employee;
import es.hga.nexo.domain.business.Login;
import es.hga.nexo.domain.business.PendingOrder;

import es.hga.nexo.domain.business.Presence;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface APIServiceRetrofit {

    //@POST("login")
    //Call<User> doLogin(@Body User user);

    @GET("setPushToken")
    Single<Response<DuxWsResponse>> setPushToken(@Query("pushtoken") String pushToken);

    @GET("getPedidosProveedorPendientes")
    Single<Response<List<PendingOrder>>> getPedidosProveedorPendientes();

    @GET("aceptarPedidoProveedorPendiente")
    Single<Response<DuxWsResponse>> aceptarPedidoProveedorPendiente(@Query("serie") Integer serie,
                                                                    @Query("numero") Integer numero);

    @GET("rechazarPedidoProveedorPendiente")
    Single<Response<DuxWsResponse>> rechazarPedidoProveedorPendiente(@Query("serie") Integer serie,
                                                                     @Query("numero") Integer numero,
                                                                     @Query("motivo") String motivo);

    @GET("doLoginWS")
    Single<Response<DuxWsResponse>> doLoginWS(@Query("empresa") String empresa,
                                              @Query("usuario") String usuario,
                                              @Query("password") String password);

    @GET("getPermisosDux")
    Single<Response<List<Login.Permission>>> getPermisosDux(@Query("empresa") String empresa,
                                                            @Query("usuario") String usuario);

    @GET("checkTokenWS")
    Single<Response<DuxWsResponse>> checkTokenWS();

    @GET("grabarPresenciaByToken")
    Single<Response<Presence>> grabarPresencia(@Query("tarjeta") String tarjeta,
                                               @Query("equipo") String equipo,
                                               @Query("latitud") Double latitud,
                                               @Query("longitud") Double longitud,
                                               @Query("precision") Double precision,
                                               @Query("provider") String provider);

    @GET("comprobarEmpleadoProduccionByToken")
    Single<Response<Employee>> comprobarEmpleadoProduccionByToken(@Query("tarjeta") String tarjeta,
                                                                  @Query("equipo") String equipo);
}