package es.hga.nexo.data.cache.pendingorders.network;

import java.util.List;

import es.hga.nexo.data.cache.Cache;
import es.hga.nexo.domain.business.DuxWsResponse;
import es.hga.nexo.domain.business.PendingOrder;
import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * Created by alex on 10/05/2017.
 */

public interface PendingOrderNetworkCache extends Cache {

    Maybe<List<PendingOrder>> getPendingOrderList();

    Single<DuxWsResponse> acceptPendingOrder(int serie, int numero);

    Single<DuxWsResponse> rejectPendingOrder(int serie, int numero, String reason);

}
